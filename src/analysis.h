#pragma once
#include "model\simulation.h"
#include <mutex>
#include <condition_variable>

class analysis
{
public:
	analysis(simulation* sim);
	~analysis();

	void operator()();

	float testTime; //czas w minutach
	void stopAnalisis();

	void loadSettings();
	void saveResults();
private:
	void prepareCycle(analysisInput inp);

	int cyclesNumber;

	void showMessagebox();
	void setBaseTravelTime();
	float calculateBaseTravelTime(car* c);
	

	simulation* ptr_simulation;

	

};

