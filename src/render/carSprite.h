#pragma once

#include "oxygine-framework.h"
#include "../model/car.h"

class car;

class carSprite: public oxygine::Sprite
{
public:
	carSprite(car* c, ox::Resources& res);
	~carSprite();
	void update(const ox::UpdateState& parentUS);
	car* m_car;
private:
	
};


typedef oxygine::intrusive_ptr<carSprite> spCarSprite;


