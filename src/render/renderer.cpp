#include "renderer.h"

#include "roadSprite.h"
#include "junctionSprite.h"
#include "RbSprite.h"



renderer::renderer()
{
	
}


renderer::~renderer()
{
}

void renderer::init(spCamera parent, ox::Resources& res)
{
	m_camera = parent;
	m_res = &res;
	for (infElement* ele : storageObj.elements)
	{
		if (ele->type == infElType::road)
		{
			parent->addChildToCamera(new roadSprite(static_cast<road*>(ele),res));
		}
		else if (ele->type == infElType::junction)
		{
			parent->addChildToCamera(new junctionSprite(static_cast<junction*>(ele),res));
		}
		else if (ele->type == infElType::roundabout)
		{
			parent->addChildToCamera(new RbSprite(static_cast<Roundabout*>(ele), res));
		}
	}
}

void renderer::addCar(car * c)
{
	storageObj.threadSyncro.rendering_mtx.lock();
	c->calculateAbsPos();
	spCarSprite newCar = new carSprite(c, *m_res);
	carSprites[c] = newCar.get();
	m_camera->addChildToCamera(newCar);
	storageObj.threadSyncro.rendering_mtx.unlock();
}

void renderer::removeCar(car * c)
{
	storageObj.threadSyncro.rendering_mtx.lock();
	spCarSprite oldCar = carSprites[c];
	oldCar->detach();
	carSprites.erase(c);
	storageObj.threadSyncro.rendering_mtx.unlock();

}

void renderer::removeAllCars()
{
	storageObj.threadSyncro.rendering_mtx.lock();
	for (auto const& cs: carSprites)
	{
		cs.second->detach();
	}
	carSprites.clear();
	storageObj.threadSyncro.rendering_mtx.unlock();
}
