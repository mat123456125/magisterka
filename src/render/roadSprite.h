#pragma once

#include "oxygine-framework.h"
#include "../model/road.h"

class roadSprite :public oxygine::ColorRectSprite
{
public:
	
	roadSprite(road* r, ox::Resources& res);
	~roadSprite();



	road* ptr_road;

private:
	void addLines();
	void addArrows(ox::Resources& res);
	void checkRb(ox::Resources& res);
	void addAngles();
};

typedef oxygine::intrusive_ptr<roadSprite> spRoadSprite;
