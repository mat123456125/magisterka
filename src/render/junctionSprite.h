#pragma once

#include "oxygine-framework.h"
#include "../model/junction/simpleJunctionLights4.h"
#include <vector>

class junctionSprite :public ox::Polygon
{
public:
	junctionSprite(junction* j, ox::Resources& res);
	~junctionSprite();

	static ox::vertexPCT2 initVertex(const ox::Vector2& pos, unsigned int color);
	ox::vertexPCT2* createVertices(std::vector<ox::Vector2>&  points);
	void update(const ox::UpdateState& parentUS);

	std::vector<ox::spSprite> lights;

private:
	junction * junc;
};

