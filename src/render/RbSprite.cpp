#include "RbSprite.h"



RbSprite::RbSprite(Roundabout *rb, ox::Resources & res)
{
	
	setAnchor(0.5, 0.5);

	if (rb->roundaboutType == RoundaboutType::oneLine)
	{
		setResAnim(res.getResAnim("rondo_zw20m"));
		setSize(20, 20);
	}
	else if (rb->roundaboutType == RoundaboutType::turbo)
	{
		setResAnim(res.getResAnim("rondo_turb28m"));
		setSize(28, 28);

	}
	else /*if (rb->roundaboutType == RoundaboutType::twoLine)*/
	{
		setResAnim(res.getResAnim("rondo_zw28m"));
		setSize(28, 28);
	}

	setPosition(rb->center.x,-rb->center.y);
}


RbSprite::~RbSprite()
{
}
