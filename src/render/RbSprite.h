#pragma once

#include "oxygine-framework.h"
#include "../model/roundabout/Roundabout.h"


class RbSprite : public oxygine::Sprite
{
public:
	RbSprite(Roundabout *rb, ox::Resources & res);
	~RbSprite();
};

