#include "junctionSprite.h"

#include <vector>


using namespace oxygine;


junctionSprite::junctionSprite(junction * j, ox::Resources& res)
{
	junc = j;
	Vector2 center = Vector2(j->center.x, -(j->center.y));
	setPosition(center);
	std::vector<Vector2> points;

	points.push_back(Vector2(0, 0));
	for (int i = 0; i < j->endsNumber*2; i++)
	{
		Vector2 d = Vector2(j->dim[i].x, (j->dim[i].y)) - center;
		if (points[i] != d)
		{
			points.push_back(d);
		}
	}
	

	vertexPCT2* vertices = createVertices(points);
	setVertices(vertices, sizeof(vertexPCT2) * (points.size()-1) * 4, vertexPCT2::FORMAT, true);

	if (junc->junctionType != JunctionType::noLights)
	{
		junctionLights* jl = dynamic_cast<junctionLights*>(junc);
		if (jl->lightsType == LightsType::sides)
		{
			lights.resize(j->endsNumber);
			for (int i = 0; i < j->endsNumber; i++)
			{

				Vector2 d = Vector2(j->dim[2 * i].x, (j->dim[2 * i].y)) - center;
				lights[i] = new Sprite();
				lights[i]->setResAnim(res.getResAnim("light"));
				lights[i]->setAnchor(0.5, 0.5);
				lights[i]->setSize(2, 2);
				lights[i]->setPosition(d);


				addChild(lights[i]);
			}
		}
		else
		{
			int index = 0;
			for (int i = 0; i < j->endsNumber; i++)
			{
				for (int k = 0; k < j->ends[i].inputs.size(); k++)
				{					
					lights.push_back(new Sprite());
					lights[index]->setResAnim(res.getResAnim("light"));
					lights[index]->setAnchor(0.5, 0.5);
					lights[index]->setSize(2, 2);

					vector2 temp = j->ends[i].inputs[k].position;
					lights[index]->setPosition(Vector2(temp.x,temp.y));
					lights[index]->setPriority(5);

					addChild(lights[index]);
					index++;
				}				
			}

		}
	}
}

junctionSprite::~junctionSprite()
{
}

vertexPCT2 junctionSprite::initVertex(const Vector2 & pos, unsigned int color)
{
	vertexPCT2 v;
	v.color = color;
	v.x = pos.x;
	v.y = pos.y;
	v.z = 0;
	v.u = v.x / 128;
	v.v = v.y / 128;

	return v;
}

ox::vertexPCT2 * junctionSprite::createVertices(std::vector<Vector2>&  points)
{
	int verticesCount = (points.size()-1) * 4;

	vertexPCT2* vertices = new vertexPCT2[verticesCount];
	int color = Color(30, 50, 60, 255).rgba();

	vertexPCT2* p = vertices;
	for (int n = 1; n < points.size()-1; ++n)
	{
		//add centered vertex
		*p = initVertex(points[0], color);
		++p;

		*p = initVertex(points[n], color);
		++p;

		*p = initVertex(points[n+1], color);
		++p;

		//Oxygine uses "triangles strip" rendering mode
		//dublicate last vertex (degenerate triangles)
		*p = initVertex(points[n+1], color);
		++p;
	}

	//add centered vertex
	*p = initVertex(points[0], color);
	++p;

	*p = initVertex(points[points.size() - 1], color);
	++p;

	*p = initVertex(points[1], color);
	++p;

	//Oxygine uses "triangles strip" rendering mode
	//dublicate last vertex (degenerate triangles)
	*p = initVertex(points[1], color);
	++p;

	return vertices;
}

void junctionSprite::update(const UpdateState& parentUS)
{
	if (junc->junctionType != JunctionType::noLights)
	{
		junctionLights* j = dynamic_cast<junctionLights*>(junc);

		if (j->lightsType == LightsType::sides)
		{
			for (int i = 0; i < j->endsNumber; i++)
			{
				if (j->trafficLights[i] == trafficLightsColors::green)
					lights[i]->setColor(Color::Lime);
				else if (j->trafficLights[i] == trafficLightsColors::yellow)
					lights[i]->setColor(Color::Yellow);
				else
					lights[i]->setColor(Color::Red);
			}
		}
		else
		{
			int index = 0;
			for (int i = 0; i < j->endsNumber; i++)
			{
				for (int k = 0; k < j->ends[i].inputs.size(); k++)
				{
					if (j->ends[i].inputs[k].lightColor == trafficLightsColors::green)
						lights[index]->setColor(Color::Lime);
					else if (j->ends[i].inputs[k].lightColor == trafficLightsColors::yellow)
						lights[index]->setColor(Color::Yellow);
					else
						lights[index]->setColor(Color::Red);

					index++;
				}
			}

		}
	}
	
	Polygon::update(parentUS);
}


