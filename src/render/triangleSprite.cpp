#include "triangleSprite.h"




using namespace oxygine;

triangleSprite::triangleSprite(vector2 p1, vector2 p2, vector2 p3)
{
	std::vector<Vector2> points;

	points.push_back(Vector2(p1.x, p1.y));
	points.push_back(Vector2(p2.x, p2.y));
	points.push_back(Vector2(p3.x, p3.y));
	


	vertexPCT2* vertices = new vertexPCT2[4];
	int color = Color(30, 50, 60, 255).rgba();
	vertexPCT2* p = vertices;
	//add centered vertex
	*p = initVertex(points[0], color);
	++p;

	*p = initVertex(points[1], color);
	++p;

	*p = initVertex(points[2], color);
	++p;

	//Oxygine uses "triangles strip" rendering mode
	//dublicate last vertex (degenerate triangles)
	*p = initVertex(points[2], color);
	++p;




	
	setVertices(vertices, sizeof(vertexPCT2)  * 4, vertexPCT2::FORMAT, true);



}

triangleSprite::~triangleSprite()
{
}

ox::vertexPCT2 triangleSprite::initVertex(const ox::Vector2 & pos, unsigned int color)
{
	vertexPCT2 v;
	v.color = color;
	v.x = pos.x;
	v.y = pos.y;
	v.z = 0;
	v.u = v.x / 128;
	v.v = v.y / 128;

	return v;
}
