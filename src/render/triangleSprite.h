#pragma once

#include "oxygine-framework.h"
#include "../util/vector2.h"

class triangleSprite :public ox::Polygon
{
public:
	triangleSprite(vector2 p1, vector2 p2, vector2 p3);
	~triangleSprite();

	static ox::vertexPCT2 initVertex(const ox::Vector2& pos, unsigned int color);
};

