#pragma once

#include "oxygine-framework.h"
#include "../model/storage.h"

#include "../camera.h"
#include "carSprite.h"
#include <map>


class carSprite;
typedef ox::intrusive_ptr<carSprite> spCarSprite;

class renderer
{
public:
	renderer();
	~renderer();

	void init(spCamera parent, ox::Resources& res);

	void addCar(car* c);
	void removeCar(car* c);
	void removeAllCars();

	spCamera m_camera;
	ox::Resources* m_res;
	std::map<car*, spCarSprite > carSprites;

};

