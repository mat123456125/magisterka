#include "carSprite.h"

#include "../model/storage.h"


using namespace oxygine;

carSprite::carSprite(car * c, ox::Resources & res)
{
	m_car = c;
	setResAnim(res.getResAnim("car"));
	int color = storageObj.m_randomHelper.randi(0, 4);
	switch (color)
	{
		case 0:
			setColor(Color::Red);
			break;
		case 1:
			setColor(Color::Blue);
			break;
		case 2:
			setColor(Color::Yellow);
			break;
		case 3:
			setColor(Color::LightGray);
			break;
		case 4:
			setColor(Color::Green);
			break;
	}


	setSize(4.5, 1.9);
	setAnchor(1, 0.5);
	setPosition(c->absPositon.x,-c->absPositon.y);
	setRotation(c->angle);
	


}

carSprite::~carSprite()
{
}

void carSprite::update(const ox::UpdateState & parentUS)
{
	setPosition(m_car->absPositon.x, m_car->absPositon.y);
	setRotation(m_car->angle);

	
	Sprite::update(parentUS);
}
