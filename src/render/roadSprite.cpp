#include "roadSprite.h"

#include "triangleSprite.h"

#include <math.h>

using namespace oxygine;

roadSprite::roadSprite(road * r, ox::Resources& res)
{
	ptr_road = r;
	setAnchor(0, (r->numLinesBack/(float)(r->numLinesForward + r->numLinesBack)));
	vector2 o = r->getRoadOrientation();
	Vector2 orientation = Vector2(o.x, o.y);

	float len = r->length - (r->startReduction + r->endReduction);
	setSize(len, 3.0*(r->numLinesForward + r->numLinesBack));
	setRotation(atan2( o.y,o.x));

	vector2 pos = r->start->position;
	if (r->startReduction != 0)
	{
		pos = r->startArcPos;		
	}
	addAngles();
	
	setPosition(pos.x, pos.y);

	setColor(Color(30,50,60,255));
	addLines();
	addArrows(res);
	checkRb(res);

}

roadSprite::~roadSprite()
{
}

void roadSprite::addLines()
{
	float lineWidth = 0.2f;
	spColorRectSprite line; 
	for (int i = 0; i < ptr_road->numLinesBack; i++)
	{
		line = new ColorRectSprite();
		
		
		line->setSize(getSize().x, lineWidth);
		if (i == 0)
		{
			if (ptr_road->isOneDirectional)
				continue;
			line->setPosition(0, i*(-3.0) - lineWidth + (getSize().y*getAnchor().y));
		}
		else
			line->setPosition(0, i*(-3.0) - lineWidth / 2 + (getSize().y*getAnchor().y));
		addChild(line);
	}

	for (int i = 0; i < ptr_road->numLinesForward; i++)
	{
		line = new ColorRectSprite();
		line->setSize(getSize().x, lineWidth);
		if (i == 0)
		{
			if (ptr_road->isOneDirectional)
				continue;
			line->setPosition(0, i*(3.0) + (lineWidth)+(getSize().y*getAnchor().y));
		}
		else
			line->setPosition(0, i*(3.0) - (lineWidth / 2) + (getSize().y*getAnchor().y));
		addChild(line);
	}

}

void roadSprite::addArrows(ox::Resources& res)
{
	if (ptr_road->nextToJunction)
	{
		if (ptr_road->getNextElement(true)->type == infElType::junction || ptr_road->getNextElement(true)->type == infElType::roundabout)
		{
			for (roadLine& rl : ptr_road->linesForward)
			{
				char dir = 0;
				for (posibleDirections d : rl.directions)
				{
					dir = dir | (char)d;
				}
				spSprite arrow = new Sprite();
				arrow->setAnchor(0.5, 0);
				arrow->setRotationDegrees(90);
								
				arrow->setPosition(getSize().x - 5, 1.5 + 3.0 *rl.number + (getSize().y*getAnchor().y));


				if (dir == 1)
					arrow->setResAnim(res.getResAnim("frd"));
				else if(dir == 2)
					arrow->setResAnim(res.getResAnim("right"));
				else if (dir == 3)
					arrow->setResAnim(res.getResAnim("frdright"));
				else if (dir == 4)
					arrow->setResAnim(res.getResAnim("left"));
				else if (dir == 5)
					arrow->setResAnim(res.getResAnim("frdleft"));
				else
					arrow->setResAnim(res.getResAnim("frdleftright"));

				arrow->setSize(1, 4);
				addChild(arrow);

			}

		}
		if(ptr_road->getNextElement(false)->type == infElType::junction || ptr_road->getNextElement(false)->type == infElType::roundabout)
		{
			for (roadLine& rl : ptr_road->linesBack)
			{
				char dir = 0;
				for (posibleDirections d : rl.directions)
				{
					dir = dir | (char)d;
				}
				spSprite arrow = new Sprite();
				arrow->setAnchor(0.5, 0);
				arrow->setRotationDegrees(-90);

				arrow->setPosition( 5, -1.5 - 3.0 *rl.number + (getSize().y*getAnchor().y));


				if (dir == 1)
					arrow->setResAnim(res.getResAnim("frd"));
				else if (dir == 2)
					arrow->setResAnim(res.getResAnim("right"));
				else if (dir == 3)
					arrow->setResAnim(res.getResAnim("frdright"));
				else if (dir == 4)
					arrow->setResAnim(res.getResAnim("left"));
				else if (dir == 5)
					arrow->setResAnim(res.getResAnim("frdleft"));
				else
					arrow->setResAnim(res.getResAnim("frdleftright"));

				arrow->setSize(1, 4);
				addChild(arrow);

			}

			
		}
	}
}

void roadSprite::checkRb(ox::Resources & res)
{
	if (ptr_road->end->getOther(ptr_road)->type == infElType::roundabout)
	{
		spColorRectSprite endExtention= new ColorRectSprite();
		endExtention->setColor(Color(30, 50, 60, 255));
		endExtention->setSize(2, getSize().y);
		endExtention->setPosition(getSize().x,0);
		addChild(endExtention);


	}
	if (ptr_road->start->getOther(ptr_road)->type == infElType::roundabout)
	{
		spColorRectSprite startExtention = new ColorRectSprite();

		startExtention->setColor(Color(30, 50, 60, 255));
		startExtention->setSize(2, getSize().y);
		startExtention->setPosition(-2, 0);
		addChild(startExtention);
	}
}

void roadSprite::addAngles()
{
	if (ptr_road->startReduction != 0)
	{
		vector2 p1 = vector2(0, 3.0*(ptr_road->numLinesForward));
		vector2 p2 = vector2(0, -3.0*(ptr_road->numLinesBack));
		vector2 p3;
		if (ptr_road->startArcVecRel.y < 0) //gora
		{
			p3 = p2 - (ptr_road->startArcVecRel)*ptr_road->startAddition;
		}
		else
		{
			p3 = p1 - ptr_road->startArcVecRel*ptr_road->startAddition;
		}

		triangleSprite* startTri = new triangleSprite(p1, p2, p3);
		startTri->setPosition(getAnchor()*getSize().y);
		addChild(startTri);

		
	}
	if (ptr_road->endReduction != 0)
	{
		float len = getSize().x;
		vector2 p1 = vector2(len, -3.0*(ptr_road->numLinesForward));
		vector2 p2 = vector2(len, 3.0*(ptr_road->numLinesBack));
		vector2 p3;
		if (ptr_road->endArcVecRel.y < 0) //gora
		{
			p3 = p2 + ptr_road->endArcVecRel*ptr_road->endAddition;
		}
		else
		{
			p3 = p1 + ptr_road->endArcVecRel*ptr_road->endAddition;
		}

		triangleSprite* endTri = new triangleSprite(p1, p2, p3);
		endTri->setPosition(getAnchor()*getSize().y);
		addChild(endTri);
		
	}
}
