#include "simulation.h"
#include "storage.h"

storage storageObj;



simulation::simulation(renderer* ren)
{
	storageObj.init(); //inicjalzacja mapy

	m_renderer = ren;
	deltaF = FRAME_TIME;
	div = 0;

	__int64 countsPerSec;

	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	mSecondsPerCount = 1.0 / (double)countsPerSec;

	for (infElement* ele : storageObj.elements)
	{
		if (ele->type == infElType::border)
		{
			static_cast<border*>(ele)->m_simulation = this;
		}
	}
	hidden = storageObj.hiddenSim;
}


simulation::~simulation()
{
}

void simulation::run()
{
	__int64 newTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&newTime);
	__int64 deltaTime = newTime - currTime;
	currTime = newTime;
	double deltaSecounds = deltaTime * mSecondsPerCount; //czas od ostatniej klatki w sek

	if (hidden)
	{
		div++;
		if (div > 2)
		{
			div = 0;
			runEle(FRAME_TIME * 3);
		}

		runCars(FRAME_TIME);

	}
	else
	{
		deltaF -= deltaSecounds;
		if (deltaF < 0)
		{
			deltaF = FRAME_TIME;



			div++;
			if (div > 2)
			{
				div = 0;
				runEle(FRAME_TIME * 3);
			}

			runCars(FRAME_TIME);
		}
	}


}

void simulation::addCar(car * c)
{
	m_renderer->addCar(c);
}

void simulation::removeAllCars()
{
	m_renderer->removeAllCars();
	storageObj.cars.clear();
	//std::list<car*>::iterator i = storageObj.cars.begin();
	//while (i != storageObj.cars.end())
	//{

	//	
	//		m_renderer->removeCar(*i);
	//		/*(*i)->m_roadLine->cars.remove(*i);
	//		if ((*i)->old_roadLine != nullptr)
	//			(*i)->old_roadLine->cars.remove(*i);
	//		if((*i)->m_junctionInput != nullptr && !(*i)->m_junctionInput->cars.empty())
	//			(*i)->m_junctionInput->cars.remove(*i);*/
	//		
	//		i = storageObj.cars.erase(i);
	//	
	//}

	for (infElement* ele : storageObj.elements)
	{
		
		ele->clear();
	}

}

void simulation::setRealtimeSim()
{
	hidden = false;
}

void simulation::setHiddenSim()
{
	hidden = true;
}

void simulation::runCars(float deltaT)
{
	std::list<car*>::iterator i = storageObj.cars.begin();
	while (i != storageObj.cars.end())
	{
		
		if ((*i)->run(deltaT))
		{
			m_renderer->removeCar(*i);
			(*i)->m_roadLine->cars.remove(*i);
			i = storageObj.cars.erase(i);  
		}
		else
		{			
			++i;
		}
	}
	
}

void simulation::runEle(float deltaT)
{
	if (hidden)
	{
		for (infElement* ele : storageObj.elements)
		{
			//if (ele->type != infElType::border )
				ele->run(deltaT);
		}
		//storageObj.borders.front()->run(deltaT);
	}
	else
	{
		for (infElement* ele : storageObj.elements)
		{
			ele->run(deltaT);
		}
	}
	/*storageObj.elements.back()->run(deltaT);*/
}
