#include "roadLine.h"





roadLine::roadLine(int num, bool rd, road* p)
{
	parent = p;
	roadDir = rd;
	number = num;
	startArcLen = 0;
	endArcLen = 0;
}

roadLine::~roadLine()
{
}

void roadLine::setForward()
{
	directions.clear();
	directions.push_back(posibleDirections::forward);
}

void roadLine::setRight()
{
	directions.clear();
	directions.push_back(posibleDirections::right);
}

void roadLine::setLeft()
{
	directions.clear();
	directions.push_back(posibleDirections::left);
}

void roadLine::setForwardRight()
{
	directions.clear();
	directions.push_back(posibleDirections::right);
	directions.push_back(posibleDirections::forward);
}

void roadLine::setForwardLeft()
{
	directions.clear();
	directions.push_back(posibleDirections::forward);
	directions.push_back(posibleDirections::left);
}

void roadLine::setForwardSides()
{
	directions.clear();
	directions.push_back(posibleDirections::right);
	directions.push_back(posibleDirections::forward);
	directions.push_back(posibleDirections::left);
}

carPosData roadLine::getCarPosData(float pos, float lcp)
{
	if (startArcLen == 0 && endArcLen == 0)
		return getCarPosDataNormal(pos, lcp);
	else if(startArcLen == 0)
		return getCarPosDataEndArc(pos, lcp);
	else if (endArcLen == 0)
		return getCarPosDataStartArc(pos, lcp);
	else
		return getCarPosDataBothArcs(pos, lcp);
}

roadLine * roadLine::getPreviousLine()
{
	infElement* prevEle = parent->getNextElement(!roadDir);
	if (prevEle->type != infElType::road)
		return nullptr;

	road* prevRoad = static_cast<road*>(prevEle);
	node* tempNode = parent->getForntNode(!roadDir);
	
	return prevRoad->getOutputLine(tempNode, number);
}

carPosData roadLine::getCarPosDataNormal(float pos, float lcp)
{
	carPosData res;
	vector2 orientation;
	orientation = parent->getRoadOrientation();
	if (roadDir)
	{
		res.absPositon = parent->start->position;
	}
	else
	{
		res.absPositon = parent->end->position;
		orientation = -orientation;
	}

	res.absPositon = res.absPositon + orientation * pos;
	res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
	res.angle = atan2(orientation.y, orientation.x);
	return res;
}

carPosData roadLine::getCarPosDataStartArc(float pos, float lcp)
{
	carPosData res;
	vector2 orientation;
	orientation = parent->getRoadOrientation();
	if (pos < startArcLen) //poczatek luku
	{
		float arcDistance = startArcLen - pos;
		orientation = parent->getRoadOrientation();
		if (roadDir)
		{
			res.absPositon = parent->startArcPos;
			res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
			orientation = parent->startArcVec;
			res.absPositon = res.absPositon - orientation * arcDistance;
			res.angle = atan2(orientation.y, orientation.x);

		}
		else
		{
			res.absPositon = parent->endArcPos;
			orientation = -orientation;
			res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
			orientation = -(parent->endArcVec);
			res.absPositon = res.absPositon - orientation * arcDistance;
			res.angle = atan2(orientation.y, orientation.x);
		}


	}
	else 
	{
		float distance;
		if (roadDir)
			distance = (pos - startArcLen) + parent->startReduction;
		else
			distance = (pos - startArcLen) + parent->endReduction;

		res = getCarPosDataNormal(distance, lcp);
	}
	
	return res;
}

carPosData roadLine::getCarPosDataEndArc(float pos, float lcp)
{
	carPosData res;
	vector2 orientation;

	
	if (pos < length - endArcLen)
	{
		res = getCarPosDataNormal(pos, lcp);
	}
	else
	{
		float arcDistance = length - endArcLen; 
		arcDistance = pos - arcDistance;
		orientation = parent->getRoadOrientation();
		if (roadDir)
		{
			res.absPositon = parent->endArcPos;
			res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
			orientation = parent->endArcVec;
			res.absPositon = res.absPositon + orientation * arcDistance;
			res.angle = atan2(orientation.y, orientation.x);

		}
		else
		{
			res.absPositon = parent->startArcPos;
			orientation = -orientation;
			res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
			orientation = -(parent->startArcVec);
			res.absPositon = res.absPositon + orientation * arcDistance;
			res.angle = atan2(orientation.y, orientation.x);
		}

	}
	return res;


}

carPosData roadLine::getCarPosDataBothArcs(float pos, float lcp)
{
	carPosData res;
	vector2 orientation;
	
	if (pos < startArcLen) //poczatek luku
	{
		float arcDistance = startArcLen - pos;
		orientation = parent->getRoadOrientation();
		if (roadDir)
		{
			res.absPositon = parent->startArcPos;
			res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
			orientation = parent->startArcVec;
			res.absPositon = res.absPositon - orientation * arcDistance;
			res.angle = atan2(orientation.y, orientation.x);

		}
		else
		{
			res.absPositon = parent->endArcPos;
			orientation = -orientation;
			res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
			orientation =  -(parent->endArcVec);
			res.absPositon = res.absPositon - orientation * arcDistance;
			res.angle = atan2(orientation.y, orientation.x);
		}


	}
	else if (pos < length - endArcLen)
	{
		float distance;
		if (roadDir)		
			distance = (pos - startArcLen) + parent->startReduction;		
		else		
			distance = (pos - startArcLen) + parent->endReduction;

		res = getCarPosDataNormal(distance, lcp);		
	}
	else
	{
		float arcDistance = length - endArcLen; 
		arcDistance = pos - arcDistance;
		orientation = parent->getRoadOrientation();
		if (roadDir)
		{
			res.absPositon = parent->endArcPos;
			res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
			orientation = parent->endArcVec;
			res.absPositon = res.absPositon + orientation * arcDistance;
			res.angle = atan2(orientation.y, orientation.x);

		}
		else
		{
			res.absPositon = parent->startArcPos;
			orientation = -orientation;
			res.absPositon = res.absPositon + orientation.perpendicular() *(3.0 *(number + 0.5 + lcp));
			orientation = -(parent->startArcVec);
			res.absPositon = res.absPositon + orientation * arcDistance;
			res.angle = atan2(orientation.y, orientation.x);
		}

	}
	return res;
}
