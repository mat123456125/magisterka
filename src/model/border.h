#pragma once
#include "infElement.h"
#include "node.h"

#include "../util/randomHelper.h"
#include "simulation.h"
#include <vector>

class simulation;

class border:public infElement
{
public:

	struct destProb  //prawdopodobienstwa cel�w
	{
		int id;   //id w tablicy border
		float probability;

		destProb() {}
		destProb(int i, float p):id(i),probability(p) {}
	};


	
	border();
	~border();
	border(node* p, float spc);

	node* m_node;
	int id; //id w tablicy border

	float secPerCar;
	float maxSpeed;

	std::vector<destProb>  destinationsProbability;

	simulation* m_simulation;


	void run(float deltaT);
	car* generateBlindCar(int dest);

	void clear();
private:
	randomHelper* ptr_randomHelper;
	float timeCounter;
	void generateCar();


};

