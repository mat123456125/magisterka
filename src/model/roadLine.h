#pragma once

#include <list>
#include <vector>
#include "car.h"
#include "BasicLine.h"

class car;
class road;
struct carPosData;

enum class posibleDirections
{
	forward = 1,
	right = 2,
	left = 4,
	reverse = 8

};



//szerokosc 3m
#define ROAD_LINE_WIDTH 3.0f

class roadLine :public BasicLine
{
public:
	roadLine(int num,bool rd, road* p);
	~roadLine();
	
	road* parent;
	bool roadDir;
	int number;		//indeks pasa w tablicy

	

	std::list<car*> cars;
	std::vector<posibleDirections> directions;

	//luki
	float startArcLen;  // dlugosc luku na poczatku pasa
	float endArcLen;  // dlugosc luku na poczatku pasa
	


	

	void setForward();
	void setRight();
	void setLeft();
	void setForwardRight();
	void setForwardLeft();
	void setForwardSides();

	carPosData getCarPosData(float pos,float lcp);
	roadLine* getPreviousLine(); //poprzedni pas jesli porzednio by�a droga
private:
	carPosData getCarPosDataNormal(float pos, float lcp);
	carPosData getCarPosDataStartArc(float pos, float lcp);
	carPosData getCarPosDataEndArc(float pos, float lcp);
	carPosData getCarPosDataBothArcs(float pos, float lcp);
};

