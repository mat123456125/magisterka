#include "road.h"
#include <math.h>




road::road()
{
	type = infElType::road;
}


road::~road()
{
}

road::road(node * s, node * e, int nf, int nb)
{
	nextToJunction = false;
	type = infElType::road;
	start = s;
	start->setElement(this);
	end = e;
	end->setElement(this);

	length = (end->position - start->position).length();

	numLinesForward = nf;
	for (int i = 0; i < nf; i++)
	{
		linesForward.push_back(roadLine(i,true,this));
		linesForward[i].length = length;
	}
	if (nb == 0 || nf == 0)	
		isOneDirectional = true;	
	else
		isOneDirectional = false;

	numLinesBack = nb;
	for (int i = 0; i < nb; i++)
	{
		linesBack.push_back(roadLine(i,false, this));
		linesBack[i].length = length;
	}

	startReduction = 0;
	endReduction = 0;


}

void road::fixLines()
{
		
	float roadAngle = getRoadAngle();
	infElement* sNeighbour = start->getOther(this);
	if (sNeighbour->type == infElType::road)
	{
		road* sNeighbourRoad = static_cast<road*>(sNeighbour);
		float sNeighbourAngle; //kat miedzy drogami
		if (checkNeighbourRoad(sNeighbourRoad, sNeighbourRoad->directionFrom(start),sNeighbourAngle))
		{
			changeLinesLength(sNeighbourAngle,start);
		}

		
		

	}
	else if (sNeighbour->type == infElType::junction)
	{
		//todo
	}

	infElement* eNeighbour = end->getOther(this);
	if (eNeighbour->type == infElType::road)
	{
		road* eNeighbourRoad = static_cast<road*>(eNeighbour);
		float eNeighbourAngle; //kat miedzy drogami
		if (checkNeighbourRoad(eNeighbourRoad, !(eNeighbourRoad->directionFrom(end)), eNeighbourAngle))
		{
			changeLinesLength(eNeighbourAngle,end);
		}

	}
	else if (eNeighbour->type == infElType::junction)
	{
		//todo
	}





}

std::vector<roadLine>& road::getInputLines(node * n)
{
	if (n == start)
		return linesForward;
	else
		return linesBack;
}

std::vector<roadLine>& road::getOutputLines(node * n)  //pasy ktore zjezdzaja z drogi w tym nodzie
{
	if (n == start)
		return linesBack;
	else
		return linesForward;
}

roadLine * road::getInputLine(node * n, int index)
{
	if (n == start)
		return &(linesForward[index]);
	else
		return &(linesBack[index]);
}

roadLine * road::getOutputLine(node * n, int index)
{
	try
	{
		if (n == start)
			return &(linesBack.at(index));
		else
			return &(linesForward.at(index));
	}
	catch (const std::out_of_range& oor)
	{
		return nullptr;
	}
}

bool road::directionFrom(node * n)
{
	return start == n;
}

node * road::getOther(node * caller)
{
	if (caller == start)
		return end;
	else
		return start;
}

infElement * road::getNextElement(bool dir)
{
	node* nextNode = getForntNode(dir);	
	return nextNode->getOther(this);
}

node * road::getForntNode(bool dir)
{
	if (dir)
		return end;
	else
		return start;
}

vector2 road::getRoadOrientation()
{
	return (end->position - start->position).normalize();
}

float road::getRoadAngle()
{
	vector2 roadVec = getRoadOrientation();

	return atan2(roadVec.y, roadVec.x);// TODO zmianic na normalny uklad wspolrzednych
}

bool road::isCarNextToJunction(bool carDirection)
{
	if(!nextToJunction)
		return false;
	if (getNextElement(carDirection)->type == infElType::junction || getNextElement(carDirection)->type == infElType::roundabout)
		return true;
	return false;
}

void road::clear()
{
	for (roadLine& l : linesForward)
	{
		l.cars.clear();
	}
	for (roadLine& l : linesBack)
	{
		l.cars.clear();
	}

}

bool road::checkNeighbourRoad(road * neighbour, bool direction, float& neighbourAngle)
{
	float roadAngle = getRoadAngle();
	neighbourAngle = neighbour->getRoadAngle();
	if (direction) //drogi sa skierowane do siebie
	{
		if (neighbourAngle > 0)
			neighbourAngle -= M_PI;
		else
			neighbourAngle += M_PI;
	}

	if (neighbourAngle > (roadAngle + 0.1f) || neighbourAngle < (roadAngle - 0.1f))
	{
		if (neighbourAngle >(roadAngle + M_PI / 2.0f) || neighbourAngle < (roadAngle - M_PI / 2.0f))
			throw std::logic_error("too large angle");

		neighbourAngle -= roadAngle; //kat wzgledny

		return true;
	}
	return false;
}

void road::changeLinesLength(float relAngle, node* side)
{
	float alpha = abs(relAngle / 2.0f);
	//TODO dodanie obslugi zmiany ilosci posow pomiedzy drogami

	float reduction;
	if (relAngle > 0) // kat od outputow w tym koncu
	{
		std::vector<roadLine>* lines = &(getOutputLines(side));
		int width = lines->size();
		float reduction = ROAD_LINE_WIDTH * width * tan(alpha);
		setReduction(side, reduction, relAngle / 2.0f);
		for (int i = 0; i < width; i++)
		{
			float a = ROAD_LINE_WIDTH * (width - (i + 0.5f));
			float addition = a * sin(alpha);
			(*lines)[i].endArcLen = addition;
			(*lines)[i].length = ((*lines)[i].length - reduction) + addition;
		}

		lines = &(getInputLines(side));
		for (int i = 0; i < lines->size(); i++)
		{
			float a = ROAD_LINE_WIDTH * (width + (i + 0.5f));
			float addition = a * sin(alpha);
			(*lines)[i].startArcLen = addition;
			(*lines)[i].length = ((*lines)[i].length - reduction) + addition;
		}


	}
	else    //kat od inputow w tym koncu
	{
		std::vector<roadLine>* lines = &(getInputLines(side));
		int width = lines->size();
		float reduction = ROAD_LINE_WIDTH * width * tan(alpha);
		setReduction(side, reduction, relAngle / 2.0f);
		for (int i = 0; i < width; i++)
		{
			float a = ROAD_LINE_WIDTH * (width - (i + 0.5f));
			float addition = a * sin(alpha);
			(*lines)[i].startArcLen = addition;
			(*lines)[i].length = ((*lines)[i].length - reduction) + addition;
		}

		lines = &(getOutputLines(side));
		for (int i = 0; i < lines->size(); i++)
		{
			float a = ROAD_LINE_WIDTH * (width + (i + 0.5f));
			float addition = a * sin(alpha);
			(*lines)[i].endArcLen = addition;
			(*lines)[i].length = ((*lines)[i].length - reduction) + addition;
		}

	}

}

void road::setReduction(node * side, float reduction, float alpha)
{
	if (side == start)
	{
		startReduction = reduction;
		vector2 roadOrientation = getRoadOrientation();
		startArcPos = start->position + (roadOrientation * reduction);
		float angle = getRoadAngle() + alpha;
		startArcVec = vector2(cosf(angle), sinf(angle)); // TODO zmianic na normalny uklad wspolrzednych

		startArcVecRel = vector2(cosf(alpha), sinf(alpha));
		startAddition = ROAD_LINE_WIDTH * (linesForward.size() + linesBack.size()) * sin(abs(alpha));

	}
	else
	{
		endReduction = reduction;
		vector2 roadOrientation = getRoadOrientation();
		endArcPos = end->position - (roadOrientation * reduction);
		float angle = getRoadAngle() + alpha;
		endArcVec = vector2(cosf(angle), sinf(angle)); // TODO zmianic na normalny uklad wspolrzednych
		endArcVecRel = vector2(cosf(alpha), sinf(alpha));
		endAddition = ROAD_LINE_WIDTH * (linesForward.size() + linesBack.size()) * sin(abs(alpha));
	}
		

}
