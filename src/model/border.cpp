#include "border.h"
#include "storage.h"

#define UNIFORM_RANGE_DIVISOR 1.0

using namespace std;
border::border()
{
}


border::~border()
{
}

border::border(node * p, float spc)
{
	m_node = p;
	m_node->setElement(this);
	secPerCar = 3;// spc;
	timeCounter = 0;
	ptr_randomHelper = &storageObj.m_randomHelper;
	type = infElType::border;
	maxSpeed = 15.0f;
}

//TODO do zmiany prawdopodobnie ustawianie na sta�y fps dla wydajnosci

void border::run(float deltaT)
{
	float a = deltaT + ptr_randomHelper->randf(-deltaT/ UNIFORM_RANGE_DIVISOR, deltaT /( 1+UNIFORM_RANGE_DIVISOR));
	timeCounter -= deltaT;
	if (timeCounter < 0)
	{
		timeCounter += ptr_randomHelper->randNormal(secPerCar, secPerCar / 2.0);//10.0
		generateCar();
	}


}

car * border::generateBlindCar(int dest)
{
	road* adjRoad = static_cast<road*>(m_node->getOther(this));
	roadLine* roadLine = adjRoad->getInputLine(m_node,0);

	car* tempCar = new car(adjRoad);
	tempCar->driver.midParameters();
	tempCar->direction = adjRoad->directionFrom(m_node);
	//tempCar->curMaxSpeed = maxSpeed;

	roadLine->cars.push_back(tempCar); 		
	tempCar->line = 0;
	tempCar->m_roadLine = roadLine;


	tempCar->destination = storageObj.borders[dest];
	tempCar->route = &(storageObj.mapInfo.routes[this].routes[storageObj.borders[dest]]);
	tempCar->speed = 10; //m/s
	tempCar->blind = true;

	tempCar->init();
	return tempCar;

}

void border::clear()
{
}

void border::generateCar()
{
	road* adjRoad = static_cast<road*>(m_node->getOther(this));
	vector<roadLine>* roadLines = &(adjRoad->getInputLines(m_node));
	if (roadLines->size() == 0) return; //TODO tu pewnie wyjatek albo cos

	

		
	int size = roadLines->size();
	vector<int> freeLines;
	for (int l = 0; l < size; l++)
	{
		for (car* c : roadLines->at(l).cars) //sprawdzanie czy jest miejsce
		{
			if (c->positionOnRoad < 20) 
			{
				goto fullLine;
			}
		}
		freeLines.push_back(l);
	fullLine:
		continue;

	}

	int i;
	if (freeLines.size() == 0)
		return;
	if (freeLines.size() == 1)
		i = freeLines[0];
	else
	{
		i = ptr_randomHelper->randi(0, freeLines.size()-1);
		i = freeLines[i];
	}

	int templine = i;


	

	//cel losowanie
	if (destinationsProbability.empty()) //brak zdefinioanych celow
	{
		/*i = ptr_randomHelper->randi(0, storageObj.borders.size() - 1);
		while (storageObj.borders[i] == this)
		{
			i = ptr_randomHelper->randi(0, storageObj.borders.size() - 1);
		}*/
		return;
	}
	else //cele zdefiniwane w config
	{
		float value = ptr_randomHelper->randf(); //od 0 do 1
		float curProbSum = 0; //aktualna suma prawdopodobienstw sprawdzonych celow
		bool lastDest = true;
		for (int j = 0; j < destinationsProbability.size() - 1; j++)
		{
			curProbSum += destinationsProbability[j].probability;
			if (value < curProbSum)
			{
				i = destinationsProbability[j].id;
				lastDest = false;
				break;
			}
		}
		if (lastDest)
			i = destinationsProbability.back().id;
	}

	car* tempCar = new car(adjRoad);
	storageObj.cars.push_back(tempCar);
	tempCar->driver.pickParameters();
	tempCar->direction = adjRoad->directionFrom(m_node);
	tempCar->curMaxSpeed = maxSpeed;


	roadLines->at(templine).cars.push_back(tempCar); //TODO do przemyslenia kolejnosc dodawania		
	tempCar->line = templine;
	tempCar->m_roadLine = &(roadLines->at(templine));


	tempCar->destination = storageObj.borders[i];
	tempCar->route = &(storageObj.mapInfo.routes[this].routes[storageObj.borders[i]]);
	//tempCar->calculatePath();
	tempCar->speed = 10; //m/s
	tempCar->init();

	m_simulation->addCar(tempCar);

	
}
