#pragma once

#define ROAD_WIDTH 3.0			// m
#define MAX_ACCELERATION 5.0	// m/s^2
#define MAX_SPEED 20.0
#define FRAME_TIME 0.03f
#define LINE_CHANGE_SPEED 0.1
#define ROAD_CHECK_TIME 1.0		//s - czas pomiedzy sprawdzeniami drogi przez inteligentne skrzyzowania
