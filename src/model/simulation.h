#pragma once


#include "../render/renderer.h"


#include <Windows.h>

class renderer;

class simulation
{
public:
	simulation(renderer* ren);
	~simulation();

	float deltaF; //czas kaltki symulacji w sekunach
	renderer* m_renderer;
	int div;
	bool hidden;

	void run();
	void addCar(car* c);
	void removeAllCars();

	void setRealtimeSim();
	void setHiddenSim();
private :
	__int64 currTime; //aktualny czas
	double mSecondsPerCount; //ilosc cykli na sekunde

	void runCars(float deltaT);
	void runEle(float deltaT);
};

