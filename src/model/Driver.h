#pragma once
class Driver
{
public:
	Driver();
	~Driver();

	float maxSpeed; //wynika z drogi //TODO ustawiac wartosc
	float safeTimeHeadway; //T
	float maxAcceleration; //a
	float maxDeceleration; //b
	float jamDistance; //s0
	float jamDistance1; //s1   raczej ignorowany

	float maxWaitForLineChange; //w sek
	float timeBufferForLineChange;
	float timeBufferForLineChangeRb;

	float maxWaitForPriority; //w sek
	float timeBufferJunction;
	float timeBufferForYellowIgnore;  //odleglosc czasowa w kotore ignoruje zolte swiatla

	void pickParameters();
	void midParameters();

private:
	float generateRandom(float min, float max);
	float generateRandom(float min, float max, float mean);
	float midParameter(float min, float max);
};


