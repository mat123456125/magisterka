#include "car.h"
#include "junction\simpleJunctionLights4.h"
#include "roundabout/Roundabout.h"



car::car()
{
}


car::~car()
{
}

car::car(infElement * element)
{
	lenght = 4.5;

	onElement = element;
	positionOnRoad = 0;
	
	needToChangeLine = false;
	needToChangeLineRb = false;
	needToGiveWay = false;
	changingLines = false;
	changingLinesRb = false;
	
	curMaxSpeed = 100;
	

	m_junctionInput = nullptr;
	old_roadLine = nullptr;
	aliveTime = 0;
	nextJunctionLine = nullptr;
	nextRoundabout = nullptr;
	junctionBlocked = false;
	junctionOnPriority = false;
	junctionNoSpace = false;

	ignorePriority = false;
	waitingForPriority = false;
	timeOnPriority = 0;
	timeOnChangeLine = 0;
	routeProgress = 0;

	lastLine = nullptr;
	blind = false;
}

int car::run(float deltaT)
{
	aliveTime++;

	
	distanceToJunction = -1;	
	nextTrafficLightsColor = trafficLightsColors::green;
	closestCar.p_car = nullptr;
	if (!blind)
	{
		if (onElement->type == infElType::road)
		{
			closestCar = getClosestCarOnRoad();
			road* curRoad = static_cast<road*>(onElement);
			if (curRoad->isCarNextToJunction(direction))//skrzyzowanie moze byc na obu koncach
			{

				infElement* nextEle = curRoad->getNextElement(direction);
				distanceToJunction = m_roadLine->length - positionOnRoad;
				if (nextEle->type == infElType::junction)
				{
					//swiatla		
					junction* nextJunc = dynamic_cast<junction*>(curRoad->getNextElement(direction));

					nextTrafficLightsColor = nextJunc->getLightsColorForCar(curRoad->getForntNode(direction), line);


					//brak swiatel na skrzyzowaniu ( skrzyzowania kolizyjne ze swiatlami)
					if (nextJunc->junctionType >= JunctionType::noLights)
					{
						checkJunctionForPriority(deltaT);
					}
				}
				else //rondo
				{
					checkRoundaboutForPriority(deltaT);
				}

			}
		}
		else if (onElement->type == infElType::junction)
		{
			closestCar = getClosestCarOnJunction();
		}
		else if (onElement->type == infElType::roundabout)
		{
			closestCar = getClosestCarOnRoundabout();
		}
	}

	//sprawdzenie pasa
	if (needToChangeLine)
	{
		if (checkLineForChange(futureRoadLine)) //start zmiany pasa
		{
			startLineChange();
		}
		else
		{
			if (speed < 0.001)
				timeOnChangeLine += deltaT;
			else
				timeOnChangeLine = 0;
		}
	}
	else if (needToChangeLineRb)
	{
		if (checkLineForChangeRb()) //start zmiany pasa
		{
			startLineChangeRb();			
		}
		else
		{
			if (speed == 0)
				timeOnChangeLine += deltaT;
			else
				timeOnChangeLine = 0;
		}
	}
	
	//zmiana predkosci i polozenia
	calculateMotion(deltaT);
	
	

	//zmiana predkosci zmiany pasa
	if (changingLines)
	{
		float deltaPos = 0;
		

		deltaPos = deltaT * speed;
		lineChangeProgres += deltaPos * LINE_CHANGE_SPEED;

		if (lineChangeProgres > 1)
		{
			lineChangeProgres = 0;
			changingLines = false;
			if (preferedLine != line)
			{
				needToChangeLine = true;
				setFutureLine();
			}
			else
				onPreferedLine();

			old_roadLine->cars.remove(this);
		}

	}

	else if (changingLinesRb)
	{
		float deltaPos = 0;

		deltaPos = deltaT * speed;
		lineChangeProgres += deltaPos * LINE_CHANGE_SPEED;

		if (lineChangeProgres > 1)
		{
			lineChangeProgres = 0;
			changingLinesRb = false;
			

			old_rbLine->cars.remove(this);
		}

	}

	

	//obliczenie przejscia na kolejny obiekt
	int newElement = checkElementChange();

	if (newElement == 1)
	{
		if (blind) return aliveTime;
		
		routeData& rData = storageObj.aResults.routesData[std::make_pair(route->startId, route->destId)];
		// samochod przed kasowaniem liczenie statystyk
		if (rData.noCarFinished)
		{
			rData.firstCarTime = storageObj.aResults.currentTime;
			rData.noCarFinished = false;
		}


		rData.sumTimeSpend += deltaT * aliveTime;
		rData.carNum++;
		rData.carsFinished++;
		
		return 1;

	}


	if (!blind)
	 calculateAbsPos();
	return 0;
}

void car::init()
{
	curMaxSpeed = std::min(driver.maxSpeed, curMaxSpeed);
	onNewRoad();
}

float car::getDistanceToJunction()
{
	return m_roadLine->length - positionOnRoad;
}



void car::calculateAbsPos()
{
	carPosData posData;
	if (onElement->type == infElType::road)
	{
		float lcp = 0; //line change progress and direction
		if (changingLines)
		{
			lcp = (line - oldLine) * lineChangeProgres; //TODO dodac zmiane pasa na luku
			posData = old_roadLine->getCarPosData(positionOnRoad, lcp);
		}
		else posData = m_roadLine->getCarPosData(positionOnRoad,lcp);
	}
	else if (onElement->type == infElType::junction)
	{
		posData = m_junctionLine->getCarPosData(positionOnRoad,0);
	}
	else if (onElement->type == infElType::roundabout)
	{
		if(changingLinesRb)
			posData = m_rbLine->getCarPosData(positionOnRoad, 1-lineChangeProgres);
		else
			posData = m_rbLine->getCarPosData(positionOnRoad,0);
	}

	absPositon = posData.absPositon;
	

	if (!changingLinesRb && positionOnRoad < lenght*0.5 && lastLine != nullptr)
	{
		posData = lastLine->getCarPosData(lastLine->length -(lenght*0.5 - positionOnRoad), 0);
	}
	else
	{
		if (onElement->type == infElType::road)
		{
			float lcp = 0; //line change progress and direction
			if (changingLines)
			{
				lcp = (line - oldLine) * lineChangeProgres; //TODO dodac zmiane pasa na luku
				posData = old_roadLine->getCarPosData(positionOnRoad - lenght*0.5, lcp);
			}
			else posData = m_roadLine->getCarPosData(positionOnRoad - lenght*0.5, lcp);
		}
		else if (onElement->type == infElType::junction)
		{
			posData = m_junctionLine->getCarPosData(positionOnRoad - lenght*0.5, 0);
		}
		else if (onElement->type == infElType::roundabout)
		{
			if (changingLinesRb)
			{
				//usrednianie kata poniedzy pasami
				float cd = tempCarCenterDist * (1 - lineChangeProgres) + lenght * 0.5f*lineChangeProgres;
				float newAngle = m_rbLine->getCarPosData(positionOnRoad - cd, 1 - lineChangeProgres).angle;
				//angle = oldAngle * (1 - lineChangeProgres) + newAngle * lineChangeProgres;
				angle = newAngle;
				return;

			}
			else
				posData = m_rbLine->getCarPosData(positionOnRoad - lenght*0.5, 0);
		}
	}

	

	
	angle = posData.angle;

}

car::ClosestCar car::checkRoad()
{
	ClosestCar result;

	float closestDistance = INFINITY;
	if (m_roadLine->cars.size() < 2)	//tylko ten samochod na pasie
	{
		result.p_car = nullptr;
		return result;
	}

	car* closestCar;
	for (car* c : m_roadLine->cars)
	{
		if (c != this)
		{
			float tempDistance = c->positionOnRoad - positionOnRoad;
			if (tempDistance > 0 && tempDistance < closestDistance)
			{
				closestDistance = tempDistance;
				closestCar = c;
			}

		}			
	}

	if (closestDistance == INFINITY)
	{
		result.p_car = nullptr;
		return result;
	}
	
	result.p_car = closestCar;
	result.distance = closestDistance;

	return result;
}

car::ClosestCar car::checkNextRoad(road * nextR, node * nextN, int l_number, float dist)
{
	roadLine* l;
	if (nextR->directionFrom(nextN))
		l = &(nextR->linesForward[l_number]);
	else
		l = &(nextR->linesBack[l_number]);

	ClosestCar result = checkLine(l);
	result.distance += dist;
	return result;
}

car::ClosestCar car::checkLine(roadLine * l)
{
	ClosestCar result;

	float closestDistance = INFINITY;
	if (l->cars.size() < 1)		//puste 
	{
		result.p_car = nullptr;
		return result;
	} 

	car* closestCar;
	for (car* c : l->cars)
	{
		float tempDistance =  c->positionOnRoad;
		if ( tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}
		
	}
	if (closestDistance == INFINITY) 
	{
		result.p_car = nullptr;
		return result;
	}

	result.p_car = closestCar;
	result.distance = closestDistance;

	return result;
}

car::ClosestCar car::checkNextJunction(junction * nextJ, node * nextN, float dist)
{
	ClosestCar result;

	int index = nextJ->nodeIndex(nextN);
	junctionInput& temp_junctionInput = nextJ->ends[index].inputs[line];
	
	float closestDistance = INFINITY;

	std::vector<junctionOutput>& temp_junctionOutputs = nextJ->ends[route->turns[routeProgress]].outputs;
	node* juncOutNode = nextJ->ends[route->turns[routeProgress]].roads;
	road* roadAfterJunc = static_cast<road*>(nextJ->ends[route->turns[routeProgress]].roads->getOther(nextJ));

	checkJunctionForSpace(nextJ); //sprawdzanie czy jest miejsce za skrzyzowaniem
	

	car* closestCar;

	if (temp_junctionInput.cars.size() < 1) //puste
	{
		result.p_car = nullptr;
		return result;
	}

	for (car* c : nextJ->ends[index].inputs[line].cars)
	{
		float tempDistance = c->positionOnRoad;
		if (tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}

	}
	if (closestDistance == INFINITY) 
	{
		result.p_car = nullptr;
		return result;
	}

	result.p_car = closestCar;
	result.distance = closestDistance + dist;

	return result;
}

car::ClosestCar car::checkJunction()
{
	ClosestCar result;

	float closestDistance = INFINITY;
	std::list<car*>* tempCars = &(m_junctionInput->cars);

	if (tempCars->size() < 2)  //tylko ten samochod na pasie
	{
		result.p_car = nullptr;
		return result;
	}

	car* closestCar;
	for (car* c : *tempCars)
	{
		if (c != this)
		{
			float tempDistance = c->positionOnRoad - positionOnRoad;
			if (tempDistance > 0 && tempDistance < closestDistance)
			{
				closestDistance = tempDistance;
				closestCar = c;
			}

		}
	}
	if (closestDistance == INFINITY) 
	{
		result.p_car = nullptr;
		return result;
	}

	result.p_car = closestCar;
	result.distance = closestDistance;

	return result;
}

car::ClosestCar car::checkNextRoundabout(Roundabout * nextR, node * nextN, float dist)
{
	ClosestCar result;

	int index = nextR->nodeIndex(nextN);
	RbInputLine& temp_RbInput = nextR->ends[index].inputLines[line];

	float closestDistance = INFINITY;
	
	

	car* closestCar;

	if (temp_RbInput.cars.size() < 1) //puste
	{
		result.p_car = nullptr;
		return result;
	}

	for (car* c : temp_RbInput.cars)
	{
		float tempDistance = c->positionOnRoad;
		if (tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}

	}
	if (closestDistance == INFINITY)
	{
		result.p_car = nullptr;
		return result;
	}

	result.p_car = closestCar;
	result.distance = closestDistance + dist;

	return result;
}



car::ClosestCar car::checkRbLine(RbLine * nextL, float dist)
{
	ClosestCar result;

	float closestDistance = INFINITY;


	

	car* closestCar;
	if (dist == 0)
	{
		for (car* c : nextL->cars)
		{
			if (c != this)
			{
				float tempDistance = c->positionOnRoad - positionOnRoad;
				if (tempDistance > 0 && tempDistance < closestDistance)
				{
					closestDistance = tempDistance;
					closestCar = c;
				}

			}
		}	

		if (nextL->lineType == RbLineType::circle_in)
		{
			
			int id = (static_cast<RbCircleLine*>(nextL))->line;
			if (id == nextL->parent->linesNumber - 1)
			{
				RbOutputLine* tempOutputLine;
				if ( nextL->parent->ends[nextL->inputNode].special)
					tempOutputLine = nextL->parent->ends[nextL->inputNode].outputLine2;
				else
					tempOutputLine = nextL->parent->ends[nextL->inputNode].outputLine;
				for (car* c : tempOutputLine->cars)
				{
					float tempDistance = c->positionOnRoad - positionOnRoad;
					if (tempDistance > 0 && tempDistance < closestDistance)
					{
						closestDistance = tempDistance;
						closestCar = c;
					}
				}
			}

			int i = 0;

			while (i < id + 1)
			{
				RbInputLine* tempInputLine = &(nextL->parent->ends[nextL->inputNode].inputLines[i]);
				float tempLLength = tempInputLine->length;
				for (car* c : tempInputLine->cars)
				{
					//float tempDistance = (nextL->length - positionOnRoad) - (tempLLength - c->positionOnRoad);
					float tempDistance = c->lenght + driver.jamDistance + nextL->length*0.4f - positionOnRoad;
					if (tempDistance > 0 && tempDistance < closestDistance)
					{
						closestDistance = tempDistance;
						closestCar = c;
					}
					break;
				}
				i++;
			}

		}

		else if (nextL->lineType == RbLineType::output )
		{
			int id = (static_cast<RbOutputLine*>(nextL))->startLine;
			RbCircleLine* tempLine;
			if(nextL->parent->ends[nextL->inputNode].special)
			{
				if (id == 1)
					tempLine = nullptr;
				else
					tempLine = &(nextL->parent->ends[nextL->inputNode].insideLines[1]);
			}
			else
			{
				tempLine = &(nextL->parent->ends[nextL->inputNode].insideLines[id]);
			}

			if(tempLine != nullptr)
				for (car* c : tempLine->cars)
				{
					float tempDistance = c->positionOnRoad - positionOnRoad;
					if ( c->positionOnRoad - c->lenght < 0.4f*tempLine->length && tempDistance > 0 && tempDistance < closestDistance)
					{
						closestDistance = tempDistance;
						closestCar = c;
					}
				}
			
		}
		else if (nextL->lineType == RbLineType::input)
		{
			int id = (static_cast<RbInputLine*>(nextL))->targetLine;
			RbCircleLine* tempLine = &(nextL->parent->ends[nextL->inputNode].insideLines[id]);
			float tempLLength = tempLine->length;
			for (car* c : tempLine->cars)
			{
				if (c->positionOnRoad < c->lenght + c->driver.jamDistance + tempLLength * 0.4f)
					continue;
				float tempDistance = (nextL->length - (positionOnRoad)) - (tempLLength - c->positionOnRoad);
				if (tempDistance > 0 && tempDistance < closestDistance)
				{
					closestDistance = tempDistance;
					closestCar = c;
				}
			}
			

		}

	}
	else
	{
		for (car* c : nextL->cars)
		{
			float tempDistance = c->positionOnRoad;
			if (tempDistance < closestDistance)
			{
				closestDistance = tempDistance;
				closestCar = c;
			}

		}

		if (nextL->lineType == RbLineType::circle_in)
		{
			int id = (static_cast<RbCircleLine*>(nextL))->line;
			if (id == nextL->parent->linesNumber - 1)
			{
				RbOutputLine* tempOutputLine;
				if( nextL->parent->ends[nextL->inputNode].special)
					tempOutputLine = nextL->parent->ends[nextL->inputNode].outputLine2;
				else
					tempOutputLine = nextL->parent->ends[nextL->inputNode].outputLine;
				for (car* c : tempOutputLine->cars)
				{
					float tempDistance = c->positionOnRoad;
					if (tempDistance < closestDistance)
					{
						closestDistance = tempDistance;
						closestCar = c;
					}
				}
			}

			int i = 0;

			while (i < id + 1)
			{
				RbInputLine* tempInputLine = &(nextL->parent->ends[nextL->inputNode].inputLines[id]);
				float tempLLength = tempInputLine->length;
				for (car* c : tempInputLine->cars)
				{
					float tempDistance = c->lenght + driver.jamDistance + nextL->length*0.4f ;
					if (tempDistance < closestDistance)
					{
						closestDistance = tempDistance;
						closestCar = c;
					}
					break;
				}
				i++;
			}

		}

		else if (nextL->lineType == RbLineType::output)
		{
			int id = (static_cast<RbOutputLine*>(nextL))->startLine;
			RbCircleLine* tempLine;
			if ( nextL->parent->ends[nextL->inputNode].special)
			{
				if (id == 1)
					tempLine = nullptr;
				else //id ==0
					tempLine = &(nextL->parent->ends[nextL->inputNode].insideLines[1]);
			}
			else
			{
				tempLine = &(nextL->parent->ends[nextL->inputNode].insideLines[id]);
			}

			if(tempLine != nullptr)
				for (car* c : tempLine->cars)
				{
					float tempDistance = c->positionOnRoad;
					if (tempDistance < closestDistance)
					{
						closestDistance = tempDistance;
						closestCar = c;
					}
				}

		}
		
	}

	

	if (closestDistance == INFINITY)
	{
		result.p_car = nullptr;
		return result;
	}

	result.p_car = closestCar;
	result.distance = closestDistance + dist;

	if (result.distance <= driver.jamDistance)
	{
		result.distance += 0.01;
	}

	return result;
}

car::ClosestCar car::getClosestCarOnRoad()
{
	ClosestCar closestCar = checkRoad();
	
	if (closestCar.p_car == nullptr) //pusta droga przed samochoden
	{
		road* curRoad = static_cast<road*>(onElement);
		

		node* nextNode = curRoad->getForntNode(direction);		
		infElement* nextEle = curRoad->getNextElement(direction);
		
		if (nextEle->type == infElType::road)
		{
			closestCar = checkNextRoad(static_cast<road*>(nextEle), nextNode, line, m_roadLine->length - positionOnRoad);
		}
		else if (nextEle->type == infElType::junction)
		{
			closestCar = checkNextJunction(static_cast<junction*>(nextEle), nextNode, m_roadLine->length - positionOnRoad);
		}
		else if (nextEle->type == infElType::roundabout)
		{
			closestCar = checkNextRoundabout(static_cast<Roundabout*>(nextEle), nextNode, m_roadLine->length - positionOnRoad);

		}
		else
		{
			// border nullptr
		}
	}
	//if(closestDistance > 4.5)
	//	closestDistance = closestDistance - lenght; // polozenie pojazdu oznacza srodek przodu
	return closestCar;
}

car::ClosestCar car::getClosestCarOnJunction()
{
	ClosestCar closestCar = checkJunction();
	
	if (closestCar.p_car == nullptr) //pusta droga przed samochoden
	{
		junction* curJunc = static_cast<junction*>(onElement);
		

		node* nextNode = curJunc->ends[m_junctionLine->outNode].roads;

		
		infElement* nextEle = nextNode->getOther(onElement);

		if(nextEle->type != infElType::road)
			throw std::logic_error("expected road");

		closestCar = checkNextRoad(static_cast<road*>(nextEle), nextNode, m_junctionLine->outLine, m_junctionLine->length - positionOnRoad);
		
	}
	
	
	return closestCar;
}

car::ClosestCar car::getClosestCarOnRoundabout()
{
	ClosestCar closestCar = checkRbLine(m_rbLine,0);

	Roundabout* curRb = static_cast<Roundabout*>(onElement);
	RbLine* tempRbLine = m_rbLine;
	float dist = m_rbLine->length - positionOnRoad;
	
	while(closestCar.p_car == nullptr) //pusta droga przed samochoden
	{
				
		if (tempRbLine->lineType == RbLineType::output )
		{
			node* nextNode = curRb->ends[route->turns[routeProgress]].roads;
			infElement* nextEle = nextNode->getOther(onElement);

			if (nextEle->type != infElType::road)
				throw std::logic_error("expected road");
			
			closestCar = checkNextRoad(static_cast<road*>(nextEle), nextNode, static_cast<RbOutputLine*>(tempRbLine)->targetLine, dist);
			return closestCar;
		}
		else
		{
			tempRbLine = tempRbLine->getNextLine(route->turns[routeProgress]);
			
			closestCar = checkRbLine(tempRbLine, dist);
			dist = dist + tempRbLine->length;
			
		}

	}


	return closestCar;
}

car * car::getClosestCarForPriority()
{
	car* result = nullptr;
	float distance = INFINITY;
	std::vector<junctionLine*>& lines = nextJunctionLine->priorityLines;

	for (junctionLine *line : lines)
	{		
		for(car* temp_car : line->wantToCross)
		{
			if (temp_car->getDistanceToJunction() < distance)
			{
				distance = temp_car->getDistanceToJunction();
				result = temp_car;
			}
			
		}
	}
	return result;
}

car * car::getClosestCarForPriorityRb()
{
	car* result = nullptr;
	if (nextRoundabout != nullptr)
	{
		float distance = INFINITY;


		std::vector<RbCircleLine>* tempLines = &(nextRoundabout->ends[nextRoundaboutEnd].insideLines);

		for (RbCircleLine& rb_line : *tempLines)
		{
			if(nextRoundabout->ends[nextRoundaboutEnd].special || this->line <= rb_line.line)
			for (car* temp_car : rb_line.cars)
			{
				//temp
				return temp_car;

			}
		}

		tempLines = &(nextRoundabout->ends[nextRoundaboutEnd].afterLines);

		for (RbCircleLine& rb_line : *tempLines)
		{
			if ((nextRoundabout->ends[nextRoundaboutEnd].special && rb_line.line == 0) 
				||( !nextRoundabout->ends[nextRoundaboutEnd].special && this->line <= rb_line.line))
			for (car* temp_car : rb_line.cars)
			{
				if(temp_car->route->turns[temp_car->routeProgress] != nextRoundaboutEnd && temp_car->speed > 0.5f)
				return temp_car;

			}
		}
	}
	return result;
}

void car::calculatePrefLine() //tylko jesli next to junction 
{
	road* curRoad = static_cast<road*>(onElement);

	std::vector<roadLine>* rl;
	
	if (direction)
		rl = &(curRoad->linesForward);
	else
		rl = &curRoad->linesBack;

	infElement* nextEle = curRoad->getNextElement(direction);
	posibleDirections path;
	if (nextEle->type == infElType::junction)
	{
		junction* nextJunc = static_cast<junction*>(nextEle);

		path = nextJunc->getTurnDirection(route->turns[routeProgress], curRoad->getForntNode(direction));
	}
	else
	{
		Roundabout* nextR = static_cast<Roundabout*>(nextEle);
		path = nextR->getTurnDirection(route->turns[routeProgress], curRoad->getForntNode(direction));
	}

	for (posibleDirections p : rl->at(line).directions)
	{
		if (p == path)
		{
			preferedLine = line;
			return;
		}
	}

	int i = 0;
	preferedLine = INT_MAX;
	for (roadLine& a : *rl)
	{
		for (posibleDirections p : a.directions)
		{
			if (p == path)
			{
				if(abs(preferedLine - line) > abs(i - line))
				preferedLine = i;				
			}
		}
		i++;
	}
}

bool car::checkLineForChange(roadLine * l)
{
	if (blind) return true;

	if(l->parent->getNextElement(!(l->roadDir))->type != infElType::road && positionOnRoad < 21.0f) //nie zmiena pasa zaraz za skrzyzowaniem
		return false;

	if (timeOnChangeLine > driver.maxWaitForLineChange && positionOnRoad > m_roadLine->length - 55.f )
		return true;
		
	
	ClosestCar forwardCar = checkForChangeForward(l);
	ClosestCar backCar = checkForChangeBack(l);
	if(forwardCar.p_car != nullptr)
		if (forwardCar.distance - driver.jamDistance < 0)
			return false;
		
			


	// rozwalanie lockow
	if (backCar.p_car != nullptr)
	{
		if (backCar.distance - driver.jamDistance < 0)
		{
			if (backCar.p_car->needToChangeLine && backCar.p_car->speed < 0.5)
				return true;
			else
				return false;
		}
			

		float timeToCollision = backCar.distance / (backCar.p_car->speed - speed);
		if (timeToCollision > 0 && timeToCollision < driver.timeBufferForLineChange)
			return false;
	}



	return true; 
}

car::ClosestCar car::checkForChangeForward(roadLine * l) //zwraca odleglosc tyl samochodu
{
	ClosestCar result;

	float closestDistance = INFINITY;
	if (l->cars.size() < 1)	//brak samochodow na pasie
	{
		result.p_car = nullptr;
		return result;
	}

	car* closestCar;
	for (car* c : l->cars)
	{	
		float tempDistance = c->positionOnRoad - positionOnRoad;
		if (tempDistance > 0 && tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}
				
	}

	if (closestDistance == INFINITY)
	{
		result.p_car = nullptr;
		return result;
	}

	result.p_car = closestCar;
	result.distance = closestDistance - closestCar->lenght;

	return result;
}

car::ClosestCar car::checkForChangeBack(roadLine * l) //zwraca odleglosc od tyl tego samochodu
{
	ClosestCar result;

	float closestDistance = INFINITY;
	if (l->cars.size() < 1)	//brak samochodow na pasie
	{
		result.p_car = nullptr;
		return result;
	}

	car* closestCar;
	for (car* c : l->cars)
	{
		float tempDistance = positionOnRoad - c->positionOnRoad;
		if (tempDistance > 0 && tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}

	}
	if (closestDistance == INFINITY)
	{
		//poprzednia droga
		roadLine* prevLine = l->getPreviousLine();
		if (prevLine != nullptr)
		{
			for (car* c : prevLine->cars)
			{
				float tempDistance = positionOnRoad + prevLine->length - c->positionOnRoad;
				if (tempDistance < closestDistance)
				{
					closestDistance = tempDistance;
					closestCar = c;
				}

			}
		}

		if (closestDistance == INFINITY)
		{
			result.p_car = nullptr;
			return result;
		}
	}

	

	result.p_car = closestCar;
	result.distance = closestDistance - lenght;

	return result;
}

bool car::checkLineForChangeRb()
{
	if (blind) return true;

	ClosestCar forwardCar = checkForChangeForwardRb();
	ClosestCar backCar = checkForChangeBackRb();
	if (forwardCar.p_car != nullptr)
		if (forwardCar.distance - driver.jamDistance < 0)
			return false;




	// rozwalanie lockow
	if (backCar.p_car != nullptr)
	{
		if (backCar.distance - driver.jamDistance < 0)
		{
			return false;
		}


		float timeToCollision = backCar.distance / (backCar.p_car->speed - speed);
		if (timeToCollision > 0 && timeToCollision < driver.timeBufferForLineChangeRb)
			return false;
	}



	return true;
	
}

car::ClosestCar car::checkForChangeForwardRb() //sprawddza tylko circle in line 
{
	ClosestCar result;

	RbCircleLine* curRbLine = static_cast<RbCircleLine*>(m_rbLine);
	int lineNumber = curRbLine->line;
	float newPos = curRbLine->getCarPosOnNewLine(positionOnRoad);

	RbCircleLine* tempLine = &(m_rbLine->parent->ends[m_rbLine->inputNode].insideLines[lineNumber + 1]);

	float closestDistance = INFINITY;
	

	car* closestCar;
	for (car* c : tempLine->cars)
	{
		float tempDistance = c->positionOnRoad - newPos;
		if (tempDistance > 0 && tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}

	}
	//output
	RbOutputLine* tempOutputLine = m_rbLine->parent->ends[m_rbLine->inputNode].outputLine;
	for (car* c : tempOutputLine->cars)
	{
		float tempDistance = c->positionOnRoad - newPos;
		if (tempDistance > 0 && tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}

	}


	if (closestDistance == INFINITY)
	{
		result.p_car = nullptr;
		return result;
	}
	

	result.p_car = closestCar;
	result.distance = closestDistance - closestCar->lenght;

	return result;
}

car::ClosestCar car::checkForChangeBackRb()
{
	ClosestCar result;

	RbCircleLine* curRbLine = static_cast<RbCircleLine*>(m_rbLine);
	int lineNumber = curRbLine->line;
	float newPos = curRbLine->getCarPosOnNewLine(positionOnRoad); //pozycja na nowym pasie
	RbCircleLine* tempLine = &(m_rbLine->parent->ends[m_rbLine->inputNode].insideLines[lineNumber + 1]);

	float closestDistance = INFINITY;
	

	car* closestCar;
	for (car* c : tempLine->cars)
	{
		float tempDistance = newPos - c->positionOnRoad;
		if (tempDistance > 0 && tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}

	}

	RbOutputLine* tempOutputLine = m_rbLine->parent->ends[m_rbLine->inputNode].outputLine;
	for (car* c : tempOutputLine->cars)
	{
		float tempDistance = newPos - c->positionOnRoad ;
		if (tempDistance > 0 && tempDistance < closestDistance)
		{
			closestDistance = tempDistance;
			closestCar = c;
		}

	}

	if (closestDistance == INFINITY)
	{
		RbLine* prevLine = tempLine->getPreviousLine();
		if (prevLine != nullptr)
		{
			for (car* c : prevLine->cars)
			{
				float tempDistance = newPos + prevLine->length - c->positionOnRoad;
				if (tempDistance < closestDistance)
				{
					closestDistance = tempDistance;
					closestCar = c;
				}

			}
		}

		if (closestDistance == INFINITY)
		{
			result.p_car = nullptr;
			return result;
		}
	}



	result.p_car = closestCar;
	result.distance = closestDistance - lenght;

	return result;
}

int car::checkElementChange()
{
	float eleLength;
	if (onElement->type == infElType::road)
	{
		road* onRoad = static_cast<road*> (onElement);
		eleLength = m_roadLine->length;
		if (positionOnRoad > eleLength)
		{
			node* nextNode = onRoad->getForntNode(direction);
			
			infElement* nextEle = nextNode->getOther(onElement);

			if (nextEle->type == infElType::road)
			{
				road* nextRoad = static_cast<road*>(nextEle);
				m_roadLine->cars.remove(this);
				lastLine = m_roadLine;
				direction = nextRoad->directionFrom(nextNode);
				m_roadLine = &nextRoad->getInputLines(nextNode).at(line);
				m_roadLine->cars.push_back(this);
				onElement = nextEle;
				positionOnRoad = positionOnRoad - eleLength;	
				onNewRoad();
			}
			else if (nextEle->type == infElType::junction)
			{
				junction* nextJunc = static_cast<junction*> (nextEle);
				m_roadLine->cars.remove(this);
				lastLine = m_roadLine;

				int index = nextJunc->nodeIndex(nextNode);

				m_junctionInput = &nextJunc->ends[index].inputs[line];

				/*std::vector<junctionLine*> posLines = m_junctionInput->getDirectionLines(route->turns[routeProgress]);
				line = storageObj.m_randomHelper.randi(0, posLines.size()-1);*/

				m_junctionLine = nextJunctionLine;
				m_junctionLine->cars.push_back(this);
				m_junctionInput->cars.push_back(this);
				nextJunc->ends[m_junctionLine->outNode].outputs[m_junctionLine->outLine].cars.push_back(this);
				onElement = nextEle;
				positionOnRoad = positionOnRoad - eleLength;

				onNewJunction();
				

			}
			else if (nextEle->type == infElType::roundabout)
			{
				Roundabout* nextRb = static_cast<Roundabout*> (nextEle);

				m_roadLine->cars.remove(this);
				lastLine = m_roadLine;

				int index = nextRb->nodeIndex(nextNode);

				//TODO poprawic odpowiednie pasy wjazdowe w przypadku jeden pas-> wiele pasow
				m_rbLine = &(nextRb->ends[index].inputLines[line]);
				m_rbLine->cars.push_back(this);
				onElement = nextRb;
				positionOnRoad = positionOnRoad - eleLength;
				onNewRoundabout();

			}
			else return 1;

		}


	}
	else if (onElement->type == infElType::junction)
	{
		eleLength = m_junctionLine->length;
		junction* onJunc = static_cast<junction*> (onElement);
		if (positionOnRoad > eleLength)
		{
			node* nextNode = onJunc->ends[m_junctionLine->outNode].roads;

			infElement* nextEle = nextNode->getOther(onElement);

			if (nextEle->type == infElType::road)
			{
				road* nextRoad = static_cast<road*>(nextEle);
				m_junctionInput->cars.remove(this);
				m_junctionLine->cars.remove(this);
				lastLine = m_junctionLine;
				onJunc->ends[m_junctionLine->outNode].outputs[m_junctionLine->outLine].cars.remove(this);

				direction = nextRoad->directionFrom(nextNode);
				line = m_junctionLine->outLine;
				m_roadLine = &nextRoad->getInputLines(nextNode).at(line);
				m_roadLine->cars.push_back(this);
				onElement = nextEle;
				positionOnRoad = positionOnRoad - eleLength;

				
				
				onJunctionLeave();
				onNewRoad();
			}

			else return 1;
		}
	}
	else if (onElement->type == infElType::roundabout)
	{
		Roundabout* onRb = static_cast<Roundabout*> (onElement);
		eleLength = m_rbLine->length;

		if (positionOnRoad > eleLength)
		{
			if (m_rbLine->lineType == RbLineType::input)
			{
				RbInputLine* rbInputLine = static_cast<RbInputLine*> (m_rbLine);
				m_rbLine->cars.remove(this);
				lastLine = m_rbLine;
				int index = m_rbLine->inputNode;

				m_rbLine = &(onRb->ends[index].afterLines[rbInputLine->targetLine]);
				m_rbLine->cars.push_back(this);
				positionOnRoad =  positionOnRoad - eleLength;
			}
			else if (m_rbLine->lineType == RbLineType::circle_after)
			{
				if (changingLinesRb)
					throw "Cannot";

				RbCircleLine* rbCircleLine = static_cast<RbCircleLine*> (m_rbLine);
				int nextNodeId = rbCircleLine->inputNode - 1;
				if (nextNodeId < 0)
					nextNodeId = 3;

				m_rbLine->cars.remove(this);
				lastLine = m_rbLine;
				if (route->turns[routeProgress] == nextNodeId) //zjazd z ronda
				{
					if(m_rbLine->parent->ends[nextNodeId].special && rbCircleLine->line == 0)
						m_rbLine = onRb->ends[nextNodeId].outputLine2;
					else
						m_rbLine = onRb->ends[nextNodeId].outputLine;

					
					m_rbLine->cars.push_back(this);
					positionOnRoad = positionOnRoad - eleLength;
				}
				else //jazda po rondzie
				{
					
					if (m_rbLine->parent->ends[nextNodeId].special && rbCircleLine->line == 0)
						m_rbLine = &(onRb->ends[nextNodeId].insideLines[1]);
					else
						m_rbLine = &(onRb->ends[nextNodeId].insideLines[rbCircleLine->line]);

					m_rbLine->cars.push_back(this);
					positionOnRoad = positionOnRoad - eleLength;

					//nastepny zjazd
					nextNodeId = nextNodeId - 1;
					if (nextNodeId < 0)
						nextNodeId = 3;

					if(onRb->roundaboutType != RoundaboutType::turbo)
						if (route->turns[routeProgress] == nextNodeId && rbCircleLine->line+1 < onRb->linesNumber)
							needToChangeLineRb = true;


				}
			}
			else if (m_rbLine->lineType == RbLineType::circle_in)
			{
				if (changingLinesRb)
				{
					old_rbLine->cars.remove(this);
					old_rbLine = old_rbLine->getNextLine(0); //nie ma znaczenia parametr
					old_rbLine->cars.push_back(this);
				}
				
				RbCircleLine* rbCircleLine = static_cast<RbCircleLine*> (m_rbLine);
				m_rbLine->cars.remove(this);
				lastLine = m_rbLine;

				m_rbLine = &(onRb->ends[rbCircleLine->inputNode].afterLines[rbCircleLine->line]);
				m_rbLine->cars.push_back(this);
				positionOnRoad = positionOnRoad - eleLength;
			}
			else if (m_rbLine->lineType == RbLineType::output)
			{
				node* nextNode = onRb->ends[m_rbLine->inputNode].roads;

				infElement* nextEle = nextNode->getOther(onElement);

				RbOutputLine* rbOutputLine = static_cast<RbOutputLine*> (m_rbLine);

				if (nextEle->type == infElType::road)
				{
					road* nextRoad = static_cast<road*>(nextEle);
					m_rbLine->cars.remove(this);
					lastLine = m_rbLine;
					

					direction = nextRoad->directionFrom(nextNode);
					line = rbOutputLine->targetLine;
					m_roadLine = &nextRoad->getInputLines(nextNode).at(line);
					m_roadLine->cars.push_back(this);
					onElement = nextEle;
					positionOnRoad = positionOnRoad - eleLength;



					onRoundaboutLeave();
					onNewRoad();
				}

				else return 1;
			}

		}

	}
	
	else return 1;

	


	return 0;
}

void car::startLineChange()
{
	timeOnChangeLine = 0;
	changingLines = true;
	needToChangeLine = false;
	oldLine = line;
	old_roadLine = m_roadLine;

	if (preferedLine < line)line--;
	else line++;
	m_roadLine = futureRoadLine;
	m_roadLine->cars.push_back(this);

	lineChangeProgres = 0;
	lineChangeSpeed = 1.0f;
}

void car::startLineChangeRb()
{
	timeOnChangeLine = 0;
	changingLinesRb = true;
	needToChangeLineRb = false;
	float old_positionOnRoad = positionOnRoad;
	positionOnRoad = static_cast<RbCircleLine*>(m_rbLine)->getCarPosOnNewLine(positionOnRoad);
	tempCarCenterDist = positionOnRoad - static_cast<RbCircleLine*>(m_rbLine)->getCarPosOnNewLine(old_positionOnRoad - 0.5f*lenght);
	old_rbLine = m_rbLine;

	m_rbLine = &(old_rbLine->parent->ends[old_rbLine->inputNode].insideLines[1]);
	m_rbLine->cars.push_back(this);
	lineChangeProgres = 0;
}

void car::setFutureLine()
{
	if (preferedLine < line)
	{
		if (direction)
			futureRoadLine = &static_cast<road*>(onElement)->linesForward[line - 1];
		else
			futureRoadLine = &static_cast<road*>(onElement)->linesBack[line - 1];
	}
	else
	{
		if (direction)
			futureRoadLine = &static_cast<road*>(onElement)->linesForward[line + 1];
		else
			futureRoadLine = &static_cast<road*>(onElement)->linesBack[line + 1];
	}
}



void car::calculateMotion(float deltaT)
{
	float deltaV;
	float gap;

	float acceleration;

	if (needToChangeLine || needToChangeLineRb)
	{
		float stopDist;
		if (needToChangeLine)
		{
			stopDist = (m_roadLine->length - 30.f) - positionOnRoad; // 30m od skrzyzowania;
			if (stopDist < 0)
				stopDist = 0;
		}
		else
			stopDist = 4.0f + driver.jamDistance - positionOnRoad; // 2m za wjazdem na element ronda
		if (closestCar.p_car == nullptr)
		{
			gap = stopDist;
			deltaV = speed;
			acceleration = calculateIDM(deltaT, gap, deltaV);
		}
		else
		{
			float distance = closestCar.distance - closestCar.p_car->lenght;
			if (distance < 0) //kolizja
			{
				speed = 0;
				return;
			}
			gap = distance;
			deltaV = speed - closestCar.p_car->speed;

			acceleration = fmin(calculateIDM(deltaT, gap, deltaV), calculateIDM(deltaT, stopDist, speed)); // mniejsze z samochod/ miejsce zatrzymania

		}

	}
	
	else
	{
		if (closestCar.p_car == nullptr)
		{
			if (distanceToJunction > 0 &&
				(nextTrafficLightsColor == trafficLightsColors::red ||   //swiat�a sie licza
				(nextTrafficLightsColor == trafficLightsColors::yellow &&  distanceToJunction/speed > driver.timeBufferForYellowIgnore) ||
					junctionBlocked || junctionNoSpace ||
					(junctionOnPriority && !ignorePriority)))
			{
				gap = distanceToJunction;
				deltaV = speed;

			}
			else //nic nie  ma z przodu
			{
				gap = -1;
				deltaV = 0;
			}

		}
		else
		{
			float distance = closestCar.distance - closestCar.p_car->lenght;
			if (distance < 0) //kolizja
			{
				speed = 0;
				return;
			}

			if (distanceToJunction > 0 && distance > distanceToJunction &&
				(nextTrafficLightsColor == trafficLightsColors::red ||
				(nextTrafficLightsColor == trafficLightsColors::yellow &&  distanceToJunction / speed > driver.timeBufferForYellowIgnore) ||
					junctionBlocked || junctionNoSpace ||
					(junctionOnPriority && !ignorePriority))) //swiat�a sie licza
			{
				gap = distanceToJunction;
				deltaV = speed;
			}
			else //samochod jest uwzgledniany
			{
				gap = distance;
				deltaV = speed - closestCar.p_car->speed;
			}

		}

		
		acceleration = calculateIDM(deltaT, gap, deltaV);
	}

	moveCar(deltaT, acceleration);

}

float car::calculateIDM(float deltaT, float gap, float deltaV)
{
	float distPart;
	if (gap < 0)
		distPart = 0;
	else
	{
		float desiredGap = driver.jamDistance + driver.jamDistance1 * sqrtf(speed / curMaxSpeed) + driver.safeTimeHeadway * speed
			+ (speed*deltaV) / (2 * sqrtf(driver.maxAcceleration*driver.maxDeceleration));

		distPart = pow((desiredGap / gap), 2);
	}

	float speedPart = pow((speed / curMaxSpeed), storageObj.accelerationExponent);
	float acceleration = driver.maxAcceleration * ((1 - speedPart) - distPart);


	return acceleration;
}

void car::moveCar(float deltaT, float acceleration)
{
	float oldSpeed = speed;
	speed = speed + acceleration * deltaT;
	if (speed < 0)
		speed = 0;

	//positionOnRoad = positionOnRoad + speed*deltaT;
	else
		positionOnRoad = positionOnRoad + oldSpeed * deltaT + 0.5f*deltaT*deltaT*acceleration;
}

float car::checkLineForSpace(roadLine * l)
{
	float closestDistance = INFINITY;
	if (l->cars.size() < 1) return -1; //puste

	for (car* c : l->cars)
	{
		
		float tempDistance = c->positionOnRoad;
		if (tempDistance < closestDistance)
		{
			if (c->speed < 5.0f)
			{
				closestDistance = tempDistance - c->lenght ;
			}
			else
			{
				closestDistance -= lenght + 2.0f; //+ max jam distance;
			}
		}
		

	}
	if (closestDistance < 0) return 0;
	if (closestDistance == INFINITY) return -1;

	return closestDistance;
}



void car::checkJunctionForPriority(float deltaT)
{ 
	
	if (needToGiveWay)
	{	
		if (junctionOnPriority && getDistanceToJunction() < 5)
			waitingForPriority = true;
			
	/*	else
			timeOnPriority = 0;*/

		if(waitingForPriority == true);
			timeOnPriority += deltaT;


		junctionOnPriority = false;
		junctionBlocked = nextJunctionLine->isBlockedByOther();
		
		car* tempCar = getClosestCarForPriority();

		//bool junctionOnPriority
		if (tempCar == nullptr) return;

		if (tempCar->speed < 1.0)
		{
			if (tempCar->getDistanceToJunction() < 5 && !tempCar->junctionNoSpace) 
				junctionOnPriority = true;			
		}
		else if ((tempCar->getDistanceToJunction() / tempCar->speed) < driver.timeBufferJunction)
				junctionOnPriority = true;

		int timeout;
		if (nextJunctionLine->m_junction->junctionType == JunctionType::mixed)
			timeout = driver.maxWaitForPriority + 30;//30
		else
			timeout = driver.maxWaitForPriority;


		if (timeOnPriority > timeout && !junctionBlocked ) // jak za dlugo to lamie pierwszenstwo
		{
			ignorePriority = true;	
			
		}


	
	}



}

void car::checkJunctionForSpace(junction * nextJ)
{
	if (nextJunctionLine != nullptr)
	{
		junctionOutput* output = &(nextJ->ends[nextJunctionLine->outNode].outputs[nextJunctionLine->outLine]);
		
		
		float freeDistance = checkLineForSpace(output->outLine);
		if (freeDistance < 0)
		{
			junctionNoSpace = false;
			return;
		}

		freeDistance -= (lenght+2.0f) * output->cars.size();
		if (freeDistance < lenght)
			junctionNoSpace = true;
		else
			junctionNoSpace = false;	

	}
}

void car::checkRoundaboutForPriority(float deltaT)
{
	junctionOnPriority = false;
	car* tempCar = getClosestCarForPriorityRb();
	if (tempCar != nullptr )
	{
		junctionOnPriority = true;
	}
}

void car::onNewRoad()
{
	
	

	road* curRoad = static_cast<road*>(onElement);
	if (curRoad->isCarNextToJunction(direction))//skrzyzowanie moze byc na obu koncach
	{


		calculatePrefLine();
		
		if (preferedLine != line)
		{
			needToChangeLine = true;
			setFutureLine();
		}
		else
			onPreferedLine();
		
	}
}

void car::onNewJunction()
{
	junctionBlocked = false;

	junction* junc = static_cast<junction*> (onElement);

	m_junctionLine->wantToCross.remove(this);
	m_junctionLine->blockLines();
	needToGiveWay = false;
	ignorePriority = false;
	waitingForPriority = false;
	timeOnPriority = 0;
	



}

void car::onJunctionLeave()
{
	routeProgress++;
	nextJunctionLine->freeLines();
	nextJunctionLine = nullptr;
}

void car::onPreferedLine() // tylko nextToJunction
{
	road* onRoad = static_cast<road*> (onElement);
	if (onRoad->getNextElement(direction)->type == infElType::junction)
	{
		junction* nextJunc = static_cast<junction*> (onRoad->getNextElement(direction));
		junctionInput* nextJunctionInput = &nextJunc->ends[(nextJunc->elementIndex(onRoad))].inputs[line];

		std::vector<junctionLine*> posLines = nextJunctionInput->getDirectionLines(route->turns[routeProgress]);
		int jl = storageObj.m_randomHelper.randi(0, posLines.size() - 1);
		nextJunctionLine = posLines[jl];
		nextJunctionLine->wantToCross.push_back(this); //TODO potencjalnie zmienic na puzniejsze ustawianie sie moze to poprawic wydajnosc
		if (nextJunc->junctionType > JunctionType::lights)
			needToGiveWay = true;
	}
	else //rondo
	{
		nextRoundabout = static_cast<Roundabout*> (onRoad->getNextElement(direction));
		nextRoundaboutEnd = nextRoundabout->nodeIndex(onRoad->getForntNode(direction));
	}
	
}

void car::onNewRoundabout()
{
}

void car::onRoundaboutLeave()
{
	routeProgress++;
	nextRoundabout = nullptr;
}


