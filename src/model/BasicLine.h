#pragma once

#include "car.h"

struct carPosData;

class BasicLine
{
public:
	float length; //dlugosc calkowita

	virtual carPosData getCarPosData(float pos, float lcp)=0;

};
