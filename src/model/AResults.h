#pragma once
#include <vector>
#include <map>
#include <boost/property_tree/ptree.hpp>


struct analysisInput
{
	float carPerMin; //czas pomiedzy samochodami
	float greenLightTime; // czas zielonego w s
	float maxV; // ograniczenie predkosci
	boost::property_tree::ptree* customBorders;
};

struct analysisOutput
{
	struct routeResult
	{
		float averageTimeLoss; //sredni czas stracony
		float carsPerMinute; // ilosc samochodów na minute
	};
	std::map<std::pair<int, int>, routeResult> routesResults;	
	
};

struct routeData
{
	float baseTravelTime;

	bool noCarFinished;   // true dopuki jakis samochod nie przejedzie calej trasy
	float firstCarTime;	//czas przejazdu pierwszego samochodu
	
	int carNum;
	int carsFinished;
	float sumTimeSpend;

	void reset();

};


class AResults
{
public:
	AResults();
	~AResults();

	//bool noCarFinished;   // true dopuki jakis samochod nie przejedzie calej trasy
	//float firstCarTime;	//czas przejazdu pierwszego samochodu

	//float sumTimeSpend;
	//int carNum;
	//int carsAfterJunction;

	std::map<std::pair<int, int>, routeData> routesData;

	int testNumber;
	float currentTime; //czas od poczatku tego testu


	float testTime;
	std::vector<analysisInput> inputs;
	std::vector<analysisOutput> outputs;

	void newCycle();
	void saveCycle();


	


};

