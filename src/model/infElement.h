#pragma once

#include <list>


enum class infElType
{
	road,
	junction,
	roundabout,
	border

};



class infElement //infrastructure element
{
public:
	infElType type;

	virtual void run(float deltaT);
	virtual ~infElement();
	virtual void clear() = 0;
	

};

