#include "Driver.h"
#include "storage.h"

#define MAX_SPEED_LOW 15.0f
#define MAX_SPEED_HIGH 30.0f
#define SAFE_TIME_LOW 0.8f
#define SAFE_TIME_HIGH 2.0f
#define MAX_ACCELATION_LOW 0.8f  //0.3 DLA WYRAZNICH ZATRZYMAN
#define MAX_ACCELATION_HIGH 2.5f
#define MAX_DECELERATION_LOW 1.5f
#define MAX_DECELERATION_HIGH 2.5f
#define JAM_DIST_LOW 0.5f
#define JAM_DIST_HIGH 2.0f
#define MAX_WAIT_FOR_LINE_LOW 2.0f
#define MAX_WAIT_FOR_LINE_HIGH 5.0f

#define TIME_BUFFER_FOR_LINE_LOW 2.0f
#define TIME_BUFFER_FOR_LINE_HIGH 3.0f
#define TIME_BUFFER_FOR_LINE_RB_LOW 1.0f
#define TIME_BUFFER_FOR_LINE_RB_HIGH 2.0f
#define TIME_BUFFER_JUNCTION_LOW 1.0f
#define TIME_BUFFER_JUNCTION_HIGH 2.0f
#define MAX_WAIT_FOR_PRIORITY_LOW 5.0f
#define MAX_WAIT_FOR_PRIORITY_HIGH 15.0f

#define TIME_BUFFER_YELLOW_IGNORE_LOW 1.0f
#define TIME_BUFFER_YELLOW_IGNORE_HIGH 2.0f




Driver::Driver()
{
	maxSpeed = 15.0f; //wynika z drogi //TODO ustawiac wartosc
	safeTimeHeadway = 1.6f;
	maxAcceleration = 1.8f;
	maxDeceleration = 2.0f; 
	jamDistance = 2.0f;
	jamDistance1 = 0; //raczej ignorowany

	maxWaitForLineChange = 3.0f; //w sek
	timeBufferForLineChange = 2.0f;
	timeBufferForLineChangeRb = 1.0f;

	timeBufferJunction = 1.0f;
	maxWaitForPriority = 10.0f;
}


Driver::~Driver()
{
}

void Driver::pickParameters()
{
	maxSpeed = generateRandom(MAX_SPEED_LOW, MAX_SPEED_HIGH);
	safeTimeHeadway = generateRandom(SAFE_TIME_LOW, SAFE_TIME_HIGH);
	maxAcceleration = generateRandom(MAX_ACCELATION_LOW, MAX_ACCELATION_HIGH);
	maxDeceleration = generateRandom(MAX_DECELERATION_LOW, MAX_DECELERATION_HIGH);
	jamDistance = generateRandom(JAM_DIST_LOW, JAM_DIST_HIGH);	

	maxWaitForLineChange = generateRandom(MAX_WAIT_FOR_LINE_LOW, MAX_WAIT_FOR_LINE_HIGH);
	timeBufferForLineChange = generateRandom(TIME_BUFFER_FOR_LINE_LOW, TIME_BUFFER_FOR_LINE_HIGH);
	timeBufferForLineChangeRb = generateRandom(TIME_BUFFER_FOR_LINE_RB_LOW, TIME_BUFFER_FOR_LINE_RB_HIGH);
	timeBufferJunction = generateRandom(TIME_BUFFER_JUNCTION_LOW, TIME_BUFFER_JUNCTION_HIGH);
	maxWaitForPriority = generateRandom(MAX_WAIT_FOR_PRIORITY_LOW, MAX_WAIT_FOR_PRIORITY_HIGH);
	timeBufferForYellowIgnore = generateRandom(TIME_BUFFER_YELLOW_IGNORE_LOW, TIME_BUFFER_YELLOW_IGNORE_HIGH);
}

float Driver::generateRandom(float min, float max)
{
	float mean = (min + max) * 0.5;

	return generateRandom(min, max, mean);
	
}

float Driver::generateRandom(float min, float max, float mean)
{
	float sig = (max - min) * 0.25; // rozklad ograniczony do 2 sigm ~4% na wyjscie poza zakres

	float result = storageObj.m_randomHelper.randNormal(mean, sig);

	if (result < min) return min;
	if (result > max) return max;

	return result;
}
void Driver::midParameters()
{
	//maxSpeed = midParameter(MAX_SPEED_LOW, MAX_SPEED_HIGH);
	maxSpeed = MAX_SPEED_HIGH;
	safeTimeHeadway = midParameter(SAFE_TIME_LOW, SAFE_TIME_HIGH);
	maxAcceleration = midParameter(MAX_ACCELATION_LOW, MAX_ACCELATION_HIGH);
	maxDeceleration = midParameter(MAX_DECELERATION_LOW, MAX_DECELERATION_HIGH);
	jamDistance = midParameter(JAM_DIST_LOW, JAM_DIST_HIGH);

	maxWaitForLineChange = midParameter(MAX_WAIT_FOR_LINE_LOW, MAX_WAIT_FOR_LINE_HIGH);
	timeBufferForLineChange = midParameter(TIME_BUFFER_FOR_LINE_LOW, TIME_BUFFER_FOR_LINE_HIGH);
	timeBufferForLineChangeRb = midParameter(TIME_BUFFER_FOR_LINE_RB_LOW, TIME_BUFFER_FOR_LINE_RB_HIGH);
	timeBufferJunction = midParameter(TIME_BUFFER_JUNCTION_LOW, TIME_BUFFER_JUNCTION_HIGH);
	maxWaitForPriority = midParameter(MAX_WAIT_FOR_PRIORITY_LOW, MAX_WAIT_FOR_PRIORITY_HIGH);
	timeBufferForYellowIgnore = midParameter(TIME_BUFFER_YELLOW_IGNORE_LOW, TIME_BUFFER_YELLOW_IGNORE_HIGH);

}

float Driver::midParameter(float min, float max)
{
	return (min + max) * 0.5;	
}
