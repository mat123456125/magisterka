#pragma once
#include "junction.h"
#include "../node.h" 

//skrzyzowanie z 4 wjazdami
class junction4 : virtual public junction
{
public:
	junction4(node* r1, node* r2, node* r3, node* r4);//zgodnie z ruchem ws zegara
	~junction4();

	posibleDirections getTurnDirection(int outputIndex, node* input);
	int getTurnSide(posibleDirections turn, int input);
};

