#include "forwardJunctionLights4Smart1.h"

#define WAIT_TIME 5.0f //TODO przeniesc?



forwardJunctionLights4Smart1::forwardJunctionLights4Smart1(node * r1, node * r2, node * r3, node * r4) :
	forwardJunctionLights4(r1, r2, r3, r4)
{
	roadCheckTimer = 0;
	cyclesActived = false;
}


forwardJunctionLights4Smart1::~forwardJunctionLights4Smart1()
{
}

void forwardJunctionLights4Smart1::run(float deltaT)
{
	if (!cyclesActived)
	{
		roadCheckTimer -= deltaT;
		if (roadCheckTimer < 0)
		{
			roadCheckTimer += ROAD_CHECK_TIME;

			if (checkRoads())
			{
				cyclesActived = true;
				colorChangeTimer = WAIT_TIME;
				roadCheckTimer = 0;
			}
		}
	}
	else
	{
		runCycle(deltaT);
	}
}

bool forwardJunctionLights4Smart1::checkRoads()
{
	if (checkRoad(ends[1].roads)) return true;
	if (checkRoad(ends[3].roads)) return true;
	return false;
}

bool forwardJunctionLights4Smart1::checkRoad( node* n)
{
	road* r = static_cast<road*>(n->getOther(this));
	std::vector<roadLine>& lines = r->getOutputLines(n);
	for(auto& l : lines)
	{
		for (auto* c: l.cars)
		{
			if (l.length - c->positionOnRoad < 3.0f)
				return true;
		}

	}
	return false;
}

void forwardJunctionLights4Smart1::runCycle(float deltaT)
{
	colorChangeTimer -= deltaT;
	if (colorChangeTimer < 0)
	{
		if (cycle == 0)
		{
			colorChangeTimer = YELLOW_LIGHT_TIME;
			trafficLights[0] = trafficLightsColors::yellow;
			trafficLights[2] = trafficLightsColors::yellow;
			cycle = 1;
		}
		else if (cycle == 1)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;
			trafficLights[0] = trafficLightsColors::red;
			trafficLights[2] = trafficLightsColors::red;
			cycle = 2;
		}
		else if (cycle == 2)
		{
			colorChangeTimer = GreenLightTime;
			trafficLights[1] = trafficLightsColors::green;
			trafficLights[3] = trafficLightsColors::green;
			cycle = 3;
		}
		else if (cycle == 3)
		{
			colorChangeTimer = YELLOW_LIGHT_TIME;
			trafficLights[1] = trafficLightsColors::yellow;
			trafficLights[3] = trafficLightsColors::yellow;
			cycle = 4;
		}
		else if (cycle == 4)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;
			trafficLights[1] = trafficLightsColors::red;
			trafficLights[3] = trafficLightsColors::red;
			cycle = 5;
		}
		else if (cycle == 5)
		{
			colorChangeTimer = GreenLightTime - WAIT_TIME;
			trafficLights[0] = trafficLightsColors::green;
			trafficLights[2] = trafficLightsColors::green;
			cycle = 6;			
		}
		else if (cycle == 6)
		{
			cycle = 0;
			cyclesActived = false;
		}
	}
}
