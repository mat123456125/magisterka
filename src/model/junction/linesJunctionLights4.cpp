#include "linesJunctionLights4.h"






linesJunctionLights4::linesJunctionLights4(node * r1, node * r2, node * r3, node * r4) : junction4(r1, r2, r3, r4)
{
	junctionType = JunctionType::lights;
	setLights();

}

linesJunctionLights4::~linesJunctionLights4()
{
}

void linesJunctionLights4::setLights()
{
	lightsType = LightsType::lines;

	colorChangeTimer = GreenLightTime;
	cycle = 0;
	side = 0;
	for (int i = 0; i < 4; i++)
	{
		
		for (int j = 0; j < ends[i].inputs.size(); j++)
		{
			if ((i == 0 || i == 2) && j != 0)
			{
				ends[i].inputs[j].lightColor = trafficLightsColors::green;
			}
			else
				ends[i].inputs[j].lightColor = trafficLightsColors::red;
		}

	}
}

void linesJunctionLights4::run(float deltaT)
{
	colorChangeTimer -= deltaT;
	if (colorChangeTimer < 0)
	{
		if (cycle == 0)
		{
			colorChangeTimer = YELLOW_LIGHT_TIME;

			for (int i = 0; i < 4; i += 2)
			{
				for (int j = 0; j < ends[i + side].inputs.size(); j++)
				{
					if (j != 0)
					{
						ends[i + side].inputs[j].lightColor = trafficLightsColors::yellow;
					}
				}
			}

			cycle = 1;
		}
		else if (cycle == 1)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;

			for (int i = 0; i < 4; i += 2)
			{
				for (int j = 0; j < ends[i + side].inputs.size(); j++)
				{				
						ends[i + side].inputs[j].lightColor = trafficLightsColors::red;					
				}
			}

			cycle = 2;
		}
		else if (cycle == 2)
		{
			colorChangeTimer = GreenLightTime;
			
			for (int i = 0; i < 4; i += 2)
			{			
				ends[i + side].inputs[0].lightColor = trafficLightsColors::green;				
			}

			cycle = 3;
		}
		else if (cycle == 3)
		{
			colorChangeTimer = YELLOW_LIGHT_TIME;

			for (int i = 0; i < 4; i += 2)
			{
				ends[i + side].inputs[0].lightColor = trafficLightsColors::yellow;
			}

			cycle = 4;
		}
		else if (cycle == 4)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;

			for (int i = 0; i < 4; i += 2)
			{
				ends[i + side].inputs[0].lightColor = trafficLightsColors::red;
			}

			cycle = 5;
		}
		else if (cycle == 5)
		{
			colorChangeTimer = GreenLightTime*2;

			side++;
			side = side % 2;

			for (int i = 0; i < 4; i += 2)
			{
				for (int j = 0; j < ends[i + side].inputs.size(); j++)
				{
					if (j != 0)
					{
						ends[i + side].inputs[j].lightColor = trafficLightsColors::green;
					}
				}
			}


			cycle = 0;
		}
	}


}

trafficLightsColors linesJunctionLights4::getLightsColorForCar(node * inputNode, int line)
{
	int juncInputIndex = nodeIndex(inputNode);
	return ends[juncInputIndex].inputs[line].lightColor;
}
