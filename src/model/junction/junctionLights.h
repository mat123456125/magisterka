#pragma once

#include "junction.h"
#include <vector>



#define GREEN_LIGHT_TIME 20.0
#define YELLOW_LIGHT_TIME 3.0
#define AFTER_YELLOW_LIGHT_TIME 1.0

enum class LightsType
{
	sides,
	lines
};


//skrzyzowanie ze swiatlami
class junctionLights :virtual public junction
{
public:
	
	junctionLights();
	junctionLights(int endsNum);
	~junctionLights();

	LightsType lightsType;

	std::vector<trafficLightsColors> trafficLights;

	float colorChangeTimer;
	int greenLightNode;
	float GreenLightTime;
	
	

	
	virtual void run(float deltaT);

	virtual trafficLightsColors getLightsColorForCar(node* inputNode, int line);

};

