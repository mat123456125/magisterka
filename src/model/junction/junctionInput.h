#pragma once

#include "junction.h"
#include "junctionLine.h"
#include <vector>
#include "../../util/vector2.h"
#include "../config.h"




class car;
class junctionLine;
class junction;
enum class trafficLightsColors;

//pas wjazdowy na skrzyzowanie
class junctionInput
{
public:
	junctionInput();
	junctionInput(junction* p, node* n, int line);
	~junctionInput();

	junction* parent;
	vector2 position; //bezwzgledna

	
	std::vector<junctionLine> lines;
	std::list<car*> cars;

	std::vector<junctionLine*> getDirectionLines(int outputIndex);

	trafficLightsColors lightColor;

	void clear();
};

