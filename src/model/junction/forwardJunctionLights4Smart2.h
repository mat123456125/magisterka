#pragma once
#include "forwardJunctionLights4.h"
#include "../config.h"

//skrzyzowanie ze zmienym cyklem swiatel
class forwardJunctionLights4Smart2 :
	public forwardJunctionLights4
{
public:
	forwardJunctionLights4Smart2(node * r1, node * r2, node * r3, node * r4);//zgodnie z ruchem ws zegara
	~forwardJunctionLights4Smart2();

	void run(float deltaT);
private:
	float roadCheckTimer;
	
	bool activeAxis; //true = os r1-r3
	bool cycleExtended;

	bool checkRoads();
	void runCycle(float deltaT);
	float getCarNum( node* n); //ilosc samochodow na drodze
};


