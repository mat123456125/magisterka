#include "junctionEnd.h"



junctionEnd::junctionEnd()
{
}


junctionEnd::~junctionEnd()
{
}

void junctionEnd::clear()
{
	for (junctionInput& in : inputs)
	{
		in.clear();
	}
	for (junctionOutput& out : outputs)
	{
		out.clear();
	}
}
