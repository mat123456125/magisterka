#pragma once


#include "junctionLights.h"
#include "junction4.h"


class forwardJunctionLights4 :public junctionLights, public junction4
{
public:
	forwardJunctionLights4(node* r1, node* r2, node* r3, node* r4);//zgodnie z ruchem ws zegara
	~forwardJunctionLights4();

	void run(float deltaT);

protected:
	short cycle;
	void setPriority();
};

