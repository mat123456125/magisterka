#pragma once

#include "junction4.h"
#include "../node.h" 

class simpleJunction4 :public junction4
{
public:
	simpleJunction4(node* r1, node* r2, node* r3, node* r4);
	~simpleJunction4();
private:
	void setPriority();
};

