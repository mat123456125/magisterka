#include "forwardJunctionLights4Smart2.h"

#define WAIT_TIME 5.0f //TODO przeniesc?



forwardJunctionLights4Smart2::forwardJunctionLights4Smart2(node * r1, node * r2, node * r3, node * r4) :
	forwardJunctionLights4(r1, r2, r3, r4)
{
	roadCheckTimer = 0;
	
	activeAxis = true;
	cycleExtended = false;
}


forwardJunctionLights4Smart2::~forwardJunctionLights4Smart2()
{
}

void forwardJunctionLights4Smart2::run(float deltaT)
{
	runCycle(deltaT);
	

	
}

bool forwardJunctionLights4Smart2::checkRoads()
{
	node* activeRoad1;
	node* activeRoad2;
	node* waitingRoad1;
	node* waitingRoad2;	

	if (!cycleExtended)
	{
		if (activeAxis)
		{
			activeRoad1 = ends[0].roads;
			activeRoad2 = ends[2].roads;
			waitingRoad1 = ends[1].roads;
			waitingRoad2 = ends[3].roads;
		}
		else
		{
			activeRoad1 = ends[1].roads;
			activeRoad2 = ends[3].roads;
			waitingRoad1 = ends[0].roads;
			waitingRoad2 = ends[2].roads;
		}

		float movingCars = std::max( getCarNum(activeRoad1) , getCarNum(activeRoad2));
		float waitingCars = std::max(getCarNum(waitingRoad1) , getCarNum(waitingRoad2));

		float difference = movingCars - waitingCars;

		if (difference > 0)
		{
			colorChangeTimer += 10.0f* difference;
			cycleExtended = true;
			return false;
		}
		if (movingCars >= 5)
		{
			colorChangeTimer += 20.0f;
			cycleExtended = true;
			return false;
		}
		if (movingCars >= 2)
		{
			colorChangeTimer += 10.0f;
			cycleExtended = true;
			return false;
		}
	}
	return true;
}



void forwardJunctionLights4Smart2::runCycle(float deltaT)
{
	colorChangeTimer -= deltaT;
	if (colorChangeTimer < 0)
	{
		if (cycle == 0)
		{
			if (checkRoads())
			{
				colorChangeTimer = YELLOW_LIGHT_TIME;
				trafficLights[0] = trafficLightsColors::yellow;
				trafficLights[2] = trafficLightsColors::yellow;
				cycle = 1;
				cycleExtended = false;
			}
		}
		else if (cycle == 1)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;
			trafficLights[0] = trafficLightsColors::red;
			trafficLights[2] = trafficLightsColors::red;
			cycle = 2;
		}
		else if (cycle == 2)
		{
			colorChangeTimer = GreenLightTime;
			trafficLights[1] = trafficLightsColors::green;
			trafficLights[3] = trafficLightsColors::green;
			cycle = 3;
			activeAxis = false;
		}
		else if (cycle == 3)
		{
			if (checkRoads())
			{
				colorChangeTimer = YELLOW_LIGHT_TIME;
				trafficLights[1] = trafficLightsColors::yellow;
				trafficLights[3] = trafficLightsColors::yellow;
				cycle = 4;
				cycleExtended = false;
			}
		}
		else if (cycle == 4)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;
			trafficLights[1] = trafficLightsColors::red;
			trafficLights[3] = trafficLightsColors::red;
			cycle = 5;
		}
		else if (cycle == 5)
		{
			colorChangeTimer = GreenLightTime;
			trafficLights[0] = trafficLightsColors::green;
			trafficLights[2] = trafficLightsColors::green;
			cycle = 0;
			activeAxis = true;
		}
		
	}
}

float forwardJunctionLights4Smart2::getCarNum(node * n)
{
	road* r = static_cast<road*>(n->getOther(this));
	int result = 0;
	std::vector<roadLine>& lines = r->getOutputLines(n);
	for (auto& l : lines)
	{
		for (auto* c : l.cars)
		{
			if (l.length - c->positionOnRoad < 50.0f)
				result += 1;
		}		
	}
	
	if (lines.size() != 0)
		return (float)result / lines.size();
	else
		return 0;
	
}
