#include "junctionLine.h"
#include "simpleJunctionLights4.h"

#define PI 3.14159

junctionLine::junctionLine()
{
}


junctionLine::~junctionLine()
{
}

junctionLine::junctionLine(int in, int on, int il, int ol, posibleDirections dir, junction* j)
{
	inNode = in;
	outNode = on;
	outLine = ol;
	inputLine = il;
	direction = dir;
	blocked = false;
	m_junction = j;

	setLength();

}

carPosData junctionLine::getCarPosData(float pos, float lcp)
{
	carPosData posData;
	if (direction == posibleDirections::forward)
	{
		posData.absPositon = m_junction->ends[inNode].inputs[inputLine].position;
		
		posData.absPositon = posData.absPositon + forwardAxis * pos  ;
		posData.angle = atan2(forwardAxis.y, forwardAxis.x);
	}
	else
	{
		float theta = pos / length * 3.1415 / 2;
		vector2 zero = m_junction->ends[outNode].roads->position + m_junction->ends[inNode].roads->position - m_junction->center;
		

		vector2 local;
		if (direction == posibleDirections::right)
			local =  yAxis *(b*sin(theta))  - xAxis* (a*cos(theta));
		else
		{
			local = yAxis * (b*sin(theta)) + xAxis * (a*cos(theta));
			theta = -theta;		
		}

		posData.absPositon = zero + local;

		posData.angle = theta + atan2(-xAxis.x,xAxis.y );

	}
	return posData;
}

bool junctionLine::isBlockedByOther()
{
	if (blocked == 0)
		return false;
	else
		return true;
}

void junctionLine::blockLines()
{
	for (int i = 0; i < blocking.size(); i++)
	{
		blocking[i]->blocked++;			
	}
}

void junctionLine::freeLines()
{
	for (int i = 0; i < blocking.size(); i++)
	{
		blocking[i]->blocked--;
	}
}

void junctionLine::clear()
{
	cars.clear();
	wantToCross.clear();
	blocked = 0;

}



void junctionLine::setLength()
{
	
	if (direction == posibleDirections::left)
	{
		xAxis = (m_junction->center - m_junction->ends[outNode].roads->position).normalize();
		yAxis = (m_junction->center - m_junction->ends[inNode].roads->position).normalize();
		vector2 distance = m_junction->ends[outNode].outputs[outLine].position - m_junction->ends[inNode].inputs[inputLine].position;
		a = abs( distance.x * xAxis.x + distance.y * xAxis.y);
		b = abs( distance.x * yAxis.x + distance.y * yAxis.y);
		length = (PI*(1.5*(a + b) - sqrt(a*b))) / 4;
	}
	else if(direction == posibleDirections::right)
	{
		xAxis = ( m_junction->ends[outNode].roads->position - m_junction->center  ).normalize();
		yAxis = ( m_junction->center - m_junction->ends[inNode].roads->position ).normalize();
		vector2 distance = m_junction->ends[outNode].outputs[outLine].position - m_junction->ends[inNode].inputs[inputLine].position;
		a = abs(distance.x * xAxis.x + distance.y * xAxis.y);
		b = abs(distance.x * yAxis.x + distance.y * yAxis.y);
		length = (PI*(1.5*(a + b) - sqrt(a*b))) / 4;
	}
	else
	{
		
		yAxis = (m_junction->center - m_junction->ends[inNode].roads->position).normalize();
		xAxis = yAxis.perpendicular2();
		forwardAxis = m_junction->ends[outNode].outputs[outLine].position - m_junction->ends[inNode].inputs[inputLine].position;
		length = forwardAxis.length();
		forwardAxis = forwardAxis.normalize();
	}
}
