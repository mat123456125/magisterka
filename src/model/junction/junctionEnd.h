#pragma once

#include "../node.h"
#include "junctionInput.h"
#include "junctionOutput.h"

class junctionInput;
class junctionOutput;

class junctionEnd
{
public:
	junctionEnd();
	~junctionEnd();

	node* roads;

	std::vector<junctionInput> inputs;
	std::vector<junctionOutput> outputs;

	void clear();



};

