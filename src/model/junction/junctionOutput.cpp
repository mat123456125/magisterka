#include "junctionOutput.h"
#include "simpleJunctionLights4.h"



junctionOutput::junctionOutput()
{
	takenRight = false;
	takenForward = false;
	takenLeft = false;
}


junctionOutput::~junctionOutput()
{
}

junctionOutput::junctionOutput(junction* p, node* n, int line)
{
	
	takenRight = false;
	takenForward = false;
	takenLeft = false;

	lineNumber = line;
	parent = p;
	outLine = &(static_cast<road*>(n->getOther(parent))->getInputLines(n).at(lineNumber));

	position = n->position;
	vector2 direction = (position - p->center).normalize().perpendicular();
	position = position + direction * ((ROAD_WIDTH / 2.0) + ROAD_WIDTH * line);
}

void junctionOutput::clear()
{
	cars.clear();
}
