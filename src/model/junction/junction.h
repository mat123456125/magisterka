#pragma once

#include "../infElement.h"
#include "junctionEnd.h"
#include "../roadLine.h"
#include <vector>

class junctionEnd;
enum class posibleDirections;

enum class JunctionType
{
	lights,
	noLights,
	mixed // kolizyjne ze swiatlami
};

enum class trafficLightsColors
{
	red,
	yellow,
	green
};


class junction :public infElement
{
protected:
	struct inputSidesInfo
	{
		std::vector<roadLine>* thisRoadLines;
		int turnLeftLines;
		int forwardLines;
		int turnRightLines;
	};


public:
	junction();
	~junction();
	
	JunctionType junctionType;

	std::vector<junctionEnd> ends;
	int endsNumber;

	vector2 center;
	vector2 dim[8]; //pozycje bokow wjazdow na skrzyzowanie, wierzcholki wieloboku skrzyzowania

	int nodeIndex(node* n);
	int elementIndex(infElement* n);


	void clear();
	virtual posibleDirections getTurnDirection(int outputIndex, node* input) = 0;
	virtual int getTurnSide(posibleDirections turn, int input) = 0;
	
	virtual trafficLightsColors getLightsColorForCar(node* inputNode, int line);
	virtual bool checkBlocked(node* inputNode, int line);

protected:
	inputSidesInfo getSidesInfo(int side);

	void setJuntionInputs();
	void setJunctionLines(int side, inputSidesInfo info, int line);

	void setBlocking();
	void setDim();


};

