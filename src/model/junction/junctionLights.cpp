#include "junctionLights.h"





junctionLights::junctionLights()
{
	GreenLightTime = 20.0f;
}

junctionLights::junctionLights(int endsNum)
{
	junctionType = JunctionType::lights;
	lightsType = LightsType::sides;

	
	GreenLightTime = 20.0f;

	//swiatla
	trafficLights.resize(endsNum);
	trafficLights[0] = trafficLightsColors::green;
	for (int i = 1; i < endsNum; i++)
	{
		trafficLights[i] = trafficLightsColors::red;
	}
	
	colorChangeTimer = GreenLightTime;
	greenLightNode = 0;


}


junctionLights::~junctionLights()
{
}

void junctionLights::run(float deltaT)
{
	colorChangeTimer -= deltaT;
	if (trafficLights[greenLightNode] == trafficLightsColors::green)
	{
		if (colorChangeTimer < 0)
		{
			colorChangeTimer = YELLOW_LIGHT_TIME;
			trafficLights[greenLightNode] = trafficLightsColors::yellow;
		}
	}
	else if (trafficLights[greenLightNode] == trafficLightsColors::yellow)
	{
		if (colorChangeTimer < 0)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;
			trafficLights[greenLightNode] = trafficLightsColors::red;
		}
	}
	else //wszystkie swiatla czerwone
	{
		if (colorChangeTimer < 0)
		{
			colorChangeTimer = GreenLightTime;
			greenLightNode = (greenLightNode + 1) % endsNumber;
			trafficLights[greenLightNode] = trafficLightsColors::green;
		}
	}
}

trafficLightsColors junctionLights::getLightsColorForCar(node* inputNode, int line)
{
	int juncInputIndex = nodeIndex(inputNode);
	return trafficLights[juncInputIndex];
}
