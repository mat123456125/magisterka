#include "junction3.h"






junction3::junction3(node * r1, node * r2, node * r3)
{
	endsNumber = 3;
	type = infElType::junction;
	ends.resize(3);

	ends[0].roads = r1;
	ends[0].roads->setElement(this);
	ends[1].roads = r2;
	ends[1].roads->setElement(this);
	ends[2].roads = r3;
	ends[2].roads->setElement(this);

	center = getClosestPointOnLine(r1->position, r2->position, r3->position);

	setJuntionInputs();
	setBlocking();
	setDim();
}

junction3::~junction3()
{
}

posibleDirections junction3::getTurnDirection(int outputIndex, node * input)
{
	int inputIndex = nodeIndex(input);
	if (inputIndex == 0)
	{
		if (outputIndex == 1)
			return posibleDirections::forward;
		else //2
			return posibleDirections::right;
	}
	else if (inputIndex == 1)
	{
		if (outputIndex == 0)
			return posibleDirections::forward;
		else //2
			return posibleDirections::left;
	}
	else //2
	{
		if (outputIndex == 0)
			return posibleDirections::left;
		else //1
			return posibleDirections::right;
	}
}

int junction3::getTurnSide(posibleDirections turn, int input)
{
	if (input == 0)
	{
		if (turn == posibleDirections::right)
			return 2;
		else if (turn == posibleDirections::forward)
			return 1;
		else // left
			return -1;
	}
	else if (input == 1)
	{
		if (turn == posibleDirections::right)
			return -1;
		else if (turn == posibleDirections::forward)
			return 0;
		else // left
			return 2;
	}
	else //2
	{
		if (turn == posibleDirections::right)
			return 1;
		else if (turn == posibleDirections::forward)
			return -1;
		else // left
			return 0;
	}
}
