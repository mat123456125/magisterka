#pragma once
#include "forwardJunctionLights4.h"
#include "../config.h"

//skrzyzowanie z osia r1-r3 dominujaca swiatlo zielone dla przecinajacej osie wlaczane 
//tylko w przypadku wykrycia samochodu
class forwardJunctionLights4Smart1 :
	public forwardJunctionLights4
{
public:
	forwardJunctionLights4Smart1(node * r1, node * r2, node * r3, node * r4);//zgodnie z ruchem ws zegara
	~forwardJunctionLights4Smart1();

	void run(float deltaT);
private:
	float roadCheckTimer;
	bool cyclesActived;

	bool checkRoads();
	bool checkRoad(node* n);
	void runCycle(float deltaT);
};

