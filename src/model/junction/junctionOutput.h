#pragma once
#include "../roadLine.h"



class roadLine;
class junction;

class junctionOutput
{
public:
	junctionOutput();
	~junctionOutput();
	junctionOutput(junction* p, node* n, int line);

	std::list<car*> cars;

	junction* parent;//TODO zmieniac na junction

	int lineNumber;
	roadLine* outLine;
	vector2 position;

	bool takenRight;
	bool takenForward;
	bool takenLeft;

	void clear();
};

