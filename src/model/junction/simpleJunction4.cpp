#include "simpleJunction4.h"






simpleJunction4::simpleJunction4(node * r1, node * r2, node * r3, node * r4) : junction4(r1, r2, r3, r4)
{
	junctionType = JunctionType::noLights;
	setPriority();
}

simpleJunction4::~simpleJunction4()
{
}

void simpleJunction4::setPriority()
{
	for (int i = 0; i < endsNumber; i++)
	{
		int leftSide = getTurnSide(posibleDirections::left, i);
		int forwardSide = getTurnSide(posibleDirections::forward, i);
		int rightSide = getTurnSide(posibleDirections::right, i);
		for (junctionInput &input : ends[i].inputs)
		{
			for (junctionLine &line : input.lines)
			{
				if (line.direction == posibleDirections::left)
				{
					//lewa strona = brak
					
						
					//przod = blokuje wszystko bez lewo
					if (forwardSide != -1)
						for (junctionInput &input2 : ends[forwardSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction != posibleDirections::left)
								line.priorityLines.push_back(&line2);
							}
						}
					//prawo = ustepuje wszystko bez prawo
					if (rightSide != -1)
						for (junctionInput &input2 : ends[rightSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction != posibleDirections::right)
									line.priorityLines.push_back(&line2);
							}
						}


				}
				else if (line.direction == posibleDirections::forward)
				{
					//lewa strona = brak
					
					//przod = brak
					
					//prawo = blokuje wszystko 
					if (rightSide != -1)
						for (junctionInput &input2 : ends[rightSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								line.priorityLines.push_back(&line2);
							}
						}
				}
				else if (line.direction == posibleDirections::right)
				{
					//brak

				}
			}
		}
	}
}
