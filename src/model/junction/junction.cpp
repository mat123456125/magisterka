#include "junction.h"



junction::junction()
{
	
}


junction::~junction()
{
}

int junction::nodeIndex(node *n)
{
	for (int i = 0; i < 4; i++)
	{
		if (ends[i].roads == n)
		{
			return i;
		}
	}
	return -1;
}

int junction::elementIndex(infElement * n)
{
	for (int i = 0; i < 4; i++)
	{
		if (ends[i].roads->getOther(this) == n)
		{
			return i;
		}
	}
	return -1;
}

void junction::clear()
{
	for (junctionEnd& e : ends)
	{
		e.clear();
	}
}


trafficLightsColors junction::getLightsColorForCar(node* inputNode, int line)
{
	return trafficLightsColors::green;
}

bool junction::checkBlocked(node * inputNode, int line)
{
	return false;
}

junction::inputSidesInfo junction::getSidesInfo(int side)
{
	inputSidesInfo info;
	info.turnLeftLines = 0;
	info.turnRightLines = 0;
	info.forwardLines = 0;

	road* tempRoad = static_cast<road*>(ends[side].roads->getOther(this)); //TODO sprawdzac klase
	info.thisRoadLines = &(tempRoad->getOutputLines(ends[side].roads));

	for (int i = 0; i < info.thisRoadLines->size(); i++)
	{
		std::vector<posibleDirections> dir = info.thisRoadLines->at(i).directions;
		for (int j = 0; j < dir.size(); j++)
		{
			if (dir[j] == posibleDirections::left)
				info.turnLeftLines++;
			if (dir[j] == posibleDirections::forward)
				info.forwardLines++;
			if (dir[j] == posibleDirections::right)
				info.turnRightLines++;
		}
	}



	return info;
}

void junction::setJuntionInputs()
{
	road* tempRoad;

	for (int i = 0; i < endsNumber; i++)
	{
		tempRoad = static_cast<road*>(ends[i].roads->getOther(this));
		std::vector<roadLine>& tempRoadLines = tempRoad->getInputLines(ends[i].roads);

		for (int j = 0; j < tempRoadLines.size(); j++)
		{
			ends[i].outputs.push_back(junctionOutput(this, ends[i].roads, j));
		}


	}

	for (int i = 0; i < endsNumber; i++)
	{
		road* tempRoad = static_cast<road*>(ends[i].roads->getOther(this)); //TODO sprawdzac klase

		inputSidesInfo sidesInfo = getSidesInfo(i);
		//blokowanie niepotrzebnych pas�w wyjazdowych ze skrzyzowania
		int forwardSide = getTurnSide(posibleDirections::forward, i);
		if (forwardSide != -1)
		{
			int unusedLinesF = ends[forwardSide].outputs.size() - sidesInfo.forwardLines;
			if (unusedLinesF > 0)
			{
				for (int j = 0; j < unusedLinesF; j++)
					ends[forwardSide].outputs[j].takenForward = true;
			}
		}

		int rightSide = getTurnSide(posibleDirections::right, i);
		if (rightSide != -1)
		{
			int unusedLinesR = ends[rightSide].outputs.size() - sidesInfo.turnRightLines;
			if (unusedLinesR > 0)
			{
				for (int j = 0; j < unusedLinesR; j++)
					ends[rightSide].outputs[j].takenRight = true;

			}
		}

		//TODO chyba brakuje left

		//dla kazdego pasa drogi wchodzacej
		for (int j = 0; j < sidesInfo.thisRoadLines->size(); j++)
		{
			ends[i].inputs.push_back(junctionInput(this, ends[i].roads, j));
			setJunctionLines(i, sidesInfo, j);

		}
	}

}

void junction::setJunctionLines(int side, inputSidesInfo info, int line)
{

	//TODO cala funkcja do sprawdzenia potencjalnie zbyt arbitralne decyzje problemy z taken
	std::vector<posibleDirections> dir = info.thisRoadLines->at(line).directions;
	for (int j = 0; j < dir.size(); j++)
	{
		if (dir[j] == posibleDirections::left)
		{
			int leftSide = getTurnSide(dir[j], side);
			if (info.turnLeftLines == 1) //jeden pas do skretu w lewo
			{
				for (junctionOutput &out : ends[leftSide].outputs)
				{
					ends[side].inputs[line].lines.push_back(junctionLine(side, leftSide, line, out.lineNumber, posibleDirections::left, this));
				}
			}
			//TODO do pomyslenia
			else //if (info.turnLeftLines == outputs[leftSide].size()) // tyle samo pas�w do skretu co na drodze wyjsciowej
			{
				for (junctionOutput &out : ends[leftSide].outputs)
				{
					if (out.takenLeft == false)
					{
						ends[side].inputs[line].lines.push_back(junctionLine(side, leftSide, line, out.lineNumber, posibleDirections::left, this));
						out.takenLeft = true;
						break;
					}
				}
			}
		}
		else if (dir[j] == posibleDirections::forward)
		{
			int forwardSide = getTurnSide(dir[j], side);
			if (info.forwardLines == 1) //jeden pas prosto
			{
				junctionOutput &out = ends[forwardSide].outputs.back();
				ends[side].inputs[line].lines.push_back(junctionLine(side, forwardSide, line, out.lineNumber, posibleDirections::forward, this));
			}
			else
			{
				for (junctionOutput &out : ends[forwardSide].outputs)
				{
					if (out.takenForward == false)
					{
						ends[side].inputs[line].lines.push_back(junctionLine(side, forwardSide, line, out.lineNumber, posibleDirections::forward, this));
						out.takenForward = true;
						break;
					}
				}
			}
		}
		else if (dir[j] == posibleDirections::right)
		{
			int rightSide = getTurnSide(dir[j], side);
			if (info.turnRightLines == 1) //jeden pas w prawo
			{
				for (junctionOutput &out : ends[rightSide].outputs)
				{
					ends[side].inputs[line].lines.push_back(junctionLine(side, rightSide, line, out.lineNumber, posibleDirections::right, this));
				}
			}
			else
			{
				for (junctionOutput &out : ends[rightSide].outputs)
				{
					if (out.takenRight == false)
					{
						ends[side].inputs[line].lines.push_back(junctionLine(side, rightSide, line, out.lineNumber, posibleDirections::right, this));
						out.takenRight = true;
						break;
					}
				}
			}
		}
	}

}

void junction::setBlocking()
{
	for (int i = 0; i < endsNumber; i++)
	{
		int leftSide = getTurnSide(posibleDirections::left, i);
		int forwardSide = getTurnSide(posibleDirections::forward, i);
		int rightSide = getTurnSide(posibleDirections::right, i);
		for (junctionInput &input : ends[i].inputs)
		{
			for (junctionLine &line : input.lines)
			{
				if (line.direction == posibleDirections::left)
				{
					//lewa strona = blokuje wszystko bez prawego
					if (leftSide != -1)
						for (junctionInput &input2 : ends[leftSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction != posibleDirections::right)
									line.blocking.push_back(&line2);
							}
						}
					//przod = blokuje wszystko 
					if (forwardSide != -1)
						for (junctionInput &input2 : ends[forwardSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction != posibleDirections::left)
									line.blocking.push_back(&line2);//todo pomyslec
							}
						}
					//prawo = blokuje wszystko 
					if (rightSide != -1)
						for (junctionInput &input2 : ends[rightSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								line.blocking.push_back(&line2);
							}
						}


				}
				else if (line.direction == posibleDirections::forward)
				{
					//lewa strona = blokuje wszystko bez prawego
					if (leftSide != -1)
						for (junctionInput &input2 : ends[leftSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction != posibleDirections::right)
									line.blocking.push_back(&line2);
							}
						}
					//przod = blokuje lewe
					if (forwardSide != -1)
						for (junctionInput &input2 : ends[forwardSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction == posibleDirections::left)
									line.blocking.push_back(&line2);
							}
						}
					//prawo = blokuje wszystko 
					if (rightSide != -1)
						for (junctionInput &input2 : ends[rightSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								line.blocking.push_back(&line2);
							}
						}
				}
				else if (line.direction == posibleDirections::right)
				{
					//lewa strona = blokuje wszystko bez prawego
					if (leftSide != -1)
						for (junctionInput &input2 : ends[leftSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction != posibleDirections::right)
									line.blocking.push_back(&line2);
							}
						}
					//przod = blokuje wszystko 
					if (forwardSide != -1)
						for (junctionInput &input2 : ends[forwardSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction == posibleDirections::right)
									line.blocking.push_back(&line2);
							}
						}
					//prawo = nic

				}
			}
		}
	}
}

void junction::setDim()
{
	vector2 position;
	for (int i = 0; i < endsNumber; i++)
	{
		position = ends[i].roads->position;
		vector2 direction = (position - center).normalize().perpendicular2();

		if (ends[i].inputs.size() > 0)
			dim[2 * i] = ends[i].inputs.back().position + direction * 1.5;
		else
			dim[2 * i] = position;

		if (ends[i].outputs.size() > 0)
			dim[2 * i + 1] = ends[i].outputs.back().position - direction * 1.5;
		else
			dim[2 * i + 1] = position;



	}


}