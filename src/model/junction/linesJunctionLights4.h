#pragma once
#include "junctionLights.h"
#include "junction4.h"



class linesJunctionLights4 :public junctionLights, public junction4
{
public:
	linesJunctionLights4(node* r1, node* r2, node* r3, node* r4);
	~linesJunctionLights4();

	void run(float deltaT);
	trafficLightsColors getLightsColorForCar(node* inputNode, int line);

private:
	void setLights();

	short cycle;
	short side;

};

