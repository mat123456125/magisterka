#include "junction4.h"






junction4::junction4(node * r1, node * r2, node * r3, node * r4)
{
	endsNumber = 4;
	ends.resize(4);

	type = infElType::junction;

	ends[0].roads = r1;
	ends[0].roads->setElement(this);
	ends[1].roads = r2;
	ends[1].roads->setElement(this);
	ends[2].roads = r3;
	ends[2].roads->setElement(this);
	ends[3].roads = r4;
	ends[3].roads->setElement(this);

	center = getIntersectionPoint(r1->position, r3->position, r2->position, r4->position);




	setJuntionInputs();
	setBlocking();
	setDim();
}

junction4::~junction4()
{
}

posibleDirections junction4::getTurnDirection(int outputIndex, node * input)
{
	int inputIndex = nodeIndex(input);
	int delta = inputIndex - outputIndex;
	delta = (delta + 4) % 4;
	if (delta == 1)
		return posibleDirections::right;
	else if (delta == 2)
		return posibleDirections::forward;
	else
		return posibleDirections::left;
}

int junction4::getTurnSide(posibleDirections turn, int input)
{
	if (turn == posibleDirections::right)
		return (input + 3) % 4;
	else if (turn == posibleDirections::forward)
		return (input + 2) % 4;
	else // left
		return (input + 1) % 4;
}
