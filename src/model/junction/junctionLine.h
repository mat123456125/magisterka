#pragma once

#include <list>
#include <vector>
#include "../car.h"
#include "../roadLine.h"
#include "../config.h"
#include "junction.h"
#include "../BasicLine.h"
#include <math.h>

class junction;
enum class posibleDirections;
struct carPosData;

//trasa przejazdu przez skrzyzowanie z pasa na pas
class junctionLine :public BasicLine
{
public:
	junctionLine();
	~junctionLine();
	junctionLine(int in, int on, int il, int ol, posibleDirections dir, junction* j);

	posibleDirections direction;

	int blocked; //uzywane do zapobiegania koliziom
	

	int inNode;	//indeks strony skrzyzowania
	int outNode; //indeks strony skrzyzowania
	int inputLine;
	int outLine;

	

	std::list<car*> cars;

	junction* m_junction;

	std::vector<junctionLine*> blocking;
	std::vector<junctionLine*> priorityLines;
	std::list<car*> wantToCross; //samochody ktore chce przejechac przez to skrzyzowanie (on prefered line)

	carPosData getCarPosData(float pos, float lcp);

	bool isBlockedByOther();
	void blockLines();
	void freeLines();

	void clear();


private:

	void setLength();

	float a, b;
	vector2 xAxis;
	vector2 yAxis;
	vector2 forwardAxis;

};

