#include "junctionInput.h"
#include "simpleJunctionLights4.h"


junctionInput::junctionInput()
{
}

junctionInput::junctionInput(junction* p, node* n, int line)
{
	parent = p;
	position = n->position;
	
	vector2 direction = (position - p->center).normalize().perpendicular2();
	position = position + direction * ((ROAD_WIDTH / 2.0) + ROAD_WIDTH * line);
}


junctionInput::~junctionInput()
{
}

std::vector<junctionLine*> junctionInput::getDirectionLines(int outputIndex)
{
	std::vector<junctionLine*> res;
	for (junctionLine& jl : lines)
	{
		if (jl.outNode == outputIndex)
		{
			res.push_back(&jl);
		}
	}

	return res;
}

void junctionInput::clear()
{
	for (junctionLine& l : lines)
	{
		l.clear();
	}
	cars.clear();
}
