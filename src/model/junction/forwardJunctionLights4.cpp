#include "forwardJunctionLights4.h"



forwardJunctionLights4::forwardJunctionLights4(node * r1, node * r2, node * r3, node * r4) : junction4(r1, r2, r3, r4), junctionLights(4)
{
	junctionType = JunctionType::mixed;
	cycle = 0;
	trafficLights[0] = trafficLightsColors::green;
	trafficLights[2] = trafficLightsColors::green;

	setPriority();
}


forwardJunctionLights4::~forwardJunctionLights4()
{
}

void forwardJunctionLights4::run(float deltaT)
{
	colorChangeTimer -= deltaT;
	if (colorChangeTimer < 0)
	{
		if (cycle == 0)
		{
			colorChangeTimer = YELLOW_LIGHT_TIME;
			trafficLights[0] = trafficLightsColors::yellow;
			trafficLights[2] = trafficLightsColors::yellow;
			cycle = 1;
		}
		else if (cycle == 1)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;
			trafficLights[0] = trafficLightsColors::red;
			trafficLights[2] = trafficLightsColors::red;
			cycle = 2;
		}
		else if (cycle == 2)
		{
			colorChangeTimer = GreenLightTime;
			trafficLights[1] = trafficLightsColors::green;
			trafficLights[3] = trafficLightsColors::green;
			cycle = 3;
		}
		else if (cycle == 3)
		{
			colorChangeTimer = YELLOW_LIGHT_TIME;
			trafficLights[1] = trafficLightsColors::yellow;
			trafficLights[3] = trafficLightsColors::yellow;
			cycle = 4;
		}
		else if (cycle == 4)
		{
			colorChangeTimer = AFTER_YELLOW_LIGHT_TIME;
			trafficLights[1] = trafficLightsColors::red;
			trafficLights[3] = trafficLightsColors::red;
			cycle = 5;
		}
		else if (cycle == 5)
		{
			colorChangeTimer = GreenLightTime;
			trafficLights[0] = trafficLightsColors::green;
			trafficLights[2] = trafficLightsColors::green;
			cycle = 0;
		}
	}


}

void forwardJunctionLights4::setPriority()
{
	for (int i = 0; i < endsNumber; i++)
	{
		
		int forwardSide = getTurnSide(posibleDirections::forward, i);
		
		for (junctionInput &input : ends[i].inputs)
		{
			for (junctionLine &line : input.lines)
			{
				if (line.direction == posibleDirections::left)
				{
					
					//przod = blokuje wszystko przejazdy znaprzeiwka bez skretu lewo
					if (forwardSide != -1)
						for (junctionInput &input2 : ends[forwardSide].inputs)
						{
							for (junctionLine &line2 : input2.lines)
							{
								if (line2.direction != posibleDirections::left)
									line.priorityLines.push_back(&line2);
							}
						}
					
				}
				
			}
		}
	}

}
