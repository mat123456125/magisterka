#pragma once
#include "junction.h"
#include "../node.h" 

class junction3 : virtual public junction
{
public:
	junction3(node* r1, node* r2, node* r3);//1,2 na wprost 3 pod katem prostym zgodnie z rwz
	~junction3();

	posibleDirections getTurnDirection(int outputIndex, node* input);
	int getTurnSide(posibleDirections turn, int input);
};

