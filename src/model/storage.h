#pragma once

#include <list>
#include <vector>
#include "car.h"
#include "road.h"
#include "border.h"
#include "../util/randomHelper.h"
#include "../util/threadSyncro.h"
#include "AResults.h"
#include "../mapInput/MapLoader.h"
#include "map/MapInfo.h"
#include <boost/property_tree/ptree.hpp>

class border;
class MapInfo;
class car;

class storage
{
public:
	storage();
	~storage();

	std::list<car*> cars;
	std::vector<node*> nodes;
	std::vector<infElement*> elements;
	std::vector<border*> borders;
	randomHelper m_randomHelper;

	void init();
	void loadProperties();

	ThreadSyncro threadSyncro;

	boost::property_tree::ptree propertiesTree; //ustawienia testow
	AResults aResults; // wyniki testow
	MapLoader mapLoader;

	MapInfo mapInfo;

	int accelerationExponent;//uzywane przez IDM
	bool hiddenSim;
	

private:
	void fixRoadLines(); // poprawa dlugosci roadLine przy po�aczeniach element�w pod k�tem
	void loadBordersProperties();

	

};

extern storage storageObj;

