#pragma once

#include <vector>
#include <memory>
#include "infElement.h"
#include "node.h"
#include "roadLine.h"
#include "../util/vector2.h"

class roadLine;

class road :public infElement
{
public:
	road();
	~road();
	road(node* s, node* e, int nf, int nb);

	node* start;
	node* end;

	float length; //uzywanie do wyszukiwania sciezki do celu

	bool isOneDirectional;
	bool nextToJunction;
	
	int numLinesForward; // ilosc pasow w jednym kierunku
	std::vector<roadLine> linesForward;

	int numLinesBack; //ilosc lini w drugim kierunku
	std::vector<roadLine> linesBack;

	//luki dane pomocnicze

	float startReduction; //odcinek po ktorym konczy sie luk
	float endReduction; //odcinek od poczatku luku koncowego do node end
	

	vector2 startArcPos; //pozycja poczatku luku poczatkowego
	vector2 endArcPos; //pozycja luku koncowego

	vector2 startArcVec; //wector jednostkowy kierunku luku zgodny z kierunkiem drogi
	vector2 endArcVec; //wector jednostkowy kierunku luku

	//moze do zmiany 
	vector2 startArcVecRel; //wector jednostkowy kierunku luku
	vector2 endArcVecRel; //relatywnie do drogi
	float startAddition;
	float endAddition;

	




	void fixLines(); //poparawia dlugosci linii przy po�aczeniach pod k�tem

	std::vector<roadLine>& getInputLines(node* n);  //wchodzace na droge w tym node
	std::vector<roadLine>& getOutputLines(node* n);

	roadLine* getInputLine(node* n, int index);  //wchodzace na droge w tym node
	roadLine* getOutputLine(node* n, int index);

	bool directionFrom(node* n);
	node* getOther(node* caller);
	infElement* getNextElement(bool dir);
	node* getForntNode(bool dir);

	vector2 getRoadOrientation();
	float getRoadAngle();

	bool isCarNextToJunction(bool carDirection);

	void clear();

private:
	bool checkNeighbourRoad(road* neighbour, bool direction, float& neighbourAngle);
	void changeLinesLength(float relAngle, node* side);

	void setReduction(node* side, float reduction, float alpha);

	
};

