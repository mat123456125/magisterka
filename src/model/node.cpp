#include "node.h"




node::node()
{
	first = NULL;
	second = NULL;
}

node::node(vector2 p)
{
	first = NULL;
	second = NULL;
	position = p;
}


node::~node()
{
}

infElement * node::getOther(infElement * caller)
{
	if (caller == first)
		return second;
	else
		return first;
}

void node::setElement(infElement * caller)
{
	if (first == NULL)
	{
		first = caller;
	}
	else if (second == NULL)
	{
		second = caller;
	}
	else throw std::logic_error("node is full");
}
