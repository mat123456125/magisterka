#pragma once
#include <vector>
#include <map>
#include "MapNode.h"
#include "Route.h"

class BorderRoutes
{
public:
	infElement * start;  //border poczatkowy

	std::map<infElement*, Route> routes; //border koncowy jest kluczem
};


class MapInfo
{
public:
	MapInfo();
	~MapInfo();

	std::vector<MapNode*> nodes;
	void createPaths();
	std::map<infElement*, BorderRoutes> routes; //border poczatkowy jest kluczem

private:
	void createMap();
	void copyMap();


	void transformJunctions();
	void transformJunctionSide(int nodeId, int connId);
	void transformRbSide(int nodeId, int connId);

	void connect(MapNode* mapNode);

	void findPathsForBorder(int borderId);
	void initalizeValues(int borderId);
	void findParents(); //poprzedniki w drzewie rozpinajacym
	void fillPaths(int borderId);
	void fillPathForBorder(Route& route, int borderId);
	int findNearestNode();

	MapNode* findNode(infElement* element); //node odpowiadajacy elementowi

	

	



};

