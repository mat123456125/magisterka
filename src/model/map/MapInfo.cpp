#include "MapInfo.h"
#include "../storage.h"
#include <limits>
#include "../roundabout/Roundabout.h"



MapInfo::MapInfo()
{
}


MapInfo::~MapInfo()
{
	for (int i = 0; i < nodes.size(); i++)
	{
		delete nodes[i];
	}
}

void MapInfo::createPaths()
{
	createMap();

	for (int i = 0; i < nodes.size(); i++)
	{
		if (nodes[i]->nodeType == MapNodeType::border)
		{
			findPathsForBorder(i);
		}
	}



}

void MapInfo::createMap()
{
	copyMap();
	transformJunctions();
}

void MapInfo::copyMap()
{
	//kopiowanie wez��w
	for (int i = 0; i < storageObj.elements.size(); i++)
	{		
		nodes.push_back(new MapNode(storageObj.elements[i]));		
	}
	//po�aczenie wez��w
	for (int i = 0; i < nodes.size(); i++)
	{
		connect(nodes[i]);
	}



}

void MapInfo::transformJunctions()
{
	for (int i = 0; i < nodes.size(); i++)
	{
		if (nodes[i]->nodeType == MapNodeType::junction)
		{
			
			for (int j = 0; j < nodes[i]->connections.size(); j++)
			{
				transformJunctionSide(i, j);

			}
			//usuniecie skrzyzowania
			nodes[i] = nodes.back();
			nodes.pop_back();
		}
		else if (nodes[i]->nodeType == MapNodeType::roundabout)
		{
			for (int j = 0; j < nodes[i]->connections.size(); j++)
			{
				transformRbSide(i, j);

			}
			//usuniecie skrzyzowania
			nodes[i] = nodes.back();
			nodes.pop_back();

		}
	}
}

void MapInfo::transformJunctionSide(int nodeId, int connId)
{
	junction* junc = static_cast<junction*>(nodes[nodeId]->p_element);
	std::vector<bool> connectedSides;
	connectedSides.resize(junc->ends.size(),false);

	//sprawdzanie mozliwych po�aczen
	for (int i = 0; i < junc->ends[connId].inputs.size(); i++)
	{		
		for (junctionLine &line : junc->ends[connId].inputs[i].lines)
		{
			connectedSides[line.outNode] = true;
		}
	}

	MapNode* inputRoad = nodes[nodeId]->connections[connId];
	//tworzenie polaczen
	for (int i = 0; i < connectedSides.size(); i++)
	{
		if (connectedSides[i] == true)
		{
			MapNode* tempNode = new MapNode;
			tempNode->nodeType = MapNodeType::junctionRoute;
			tempNode->junctionSide = i;
			tempNode->connections.push_back(nodes[nodeId]->connections[i]);
			
			inputRoad->connections.push_back(tempNode);
			nodes.push_back(tempNode);
		}
	}

	//usuwanie polaczenia ze starym skrzyzowaniem
	
	for (int i = 0; i < inputRoad->connections.size(); i++)
	{
		if (inputRoad->connections[i] == nodes[nodeId])
		{
			if (inputRoad->connections.back()->nodeType == MapNodeType::junctionRoute)
			{
				inputRoad->connections[i] = inputRoad->connections.back();
				inputRoad->connections.pop_back();
			}
			else
			{
				inputRoad->connections.erase(inputRoad->connections.begin() + i);
			}
			break;
		}
	}		
}

void MapInfo::transformRbSide(int nodeId, int connId)
{
	Roundabout* rb = static_cast<Roundabout*>(nodes[nodeId]->p_element);

	std::vector<bool> connectedSides;
	connectedSides.resize(rb->ends.size(), true);

	//ustawianie mozliwych po�aczen
	connectedSides[connId] = false;
	

	MapNode* inputRoad = nodes[nodeId]->connections[connId];
	//tworzenie polaczen
	for (int i = 0; i < connectedSides.size(); i++)
	{
		if (connectedSides[i] == true)
		{
			MapNode* tempNode = new MapNode;
			tempNode->nodeType = MapNodeType::junctionRoute;
			tempNode->junctionSide = i;
			tempNode->connections.push_back(nodes[nodeId]->connections[i]);

			inputRoad->connections.push_back(tempNode);
			nodes.push_back(tempNode);
		}
	}

	//usuwanie polaczenia ze starym skrzyzowaniem

	for (int i = 0; i < inputRoad->connections.size(); i++)
	{
		if (inputRoad->connections[i] == nodes[nodeId])
		{
			if (inputRoad->connections.back()->nodeType == MapNodeType::junctionRoute)
			{
				inputRoad->connections[i] = inputRoad->connections.back();
				inputRoad->connections.pop_back();
			}
			else
			{
				inputRoad->connections.erase(inputRoad->connections.begin() + i);
			}
			break;
		}
	}

}

void MapInfo::connect(MapNode * mapNode)
{
	if (mapNode->p_element->type == infElType::road)
	{
		infElement* start;
		infElement* end;

		road* r = static_cast<road*>(mapNode->p_element);
		start = r->start->getOther(r);
		end = r->end->getOther(r);

		mapNode->connections.push_back(findNode(start)); //start pod indeksem 0
		mapNode->connections.push_back(findNode(end)); //koniec pod indeksem 1

		if (!r->isOneDirectional)
			mapNode->roadDir = MapRoadDirection::both;
		else if(r->numLinesForward > 0)
			mapNode->roadDir = MapRoadDirection::forward;
		else
			mapNode->roadDir = MapRoadDirection::backward;

	}
	else if (mapNode->p_element->type == infElType::junction)
	{
		
		infElement*  temp;
		junction* j = static_cast<junction*>(mapNode->p_element);
		for (int i = 0; i < j->ends.size(); i++)
		{
			temp = j->ends[i].roads->getOther(j);
			mapNode->connections.push_back(findNode(temp)); 
		}
	}
	else if (mapNode->p_element->type == infElType::border)
	{
		border* b = static_cast<border*>(mapNode->p_element);
		mapNode->connections.push_back(findNode(b->m_node->getOther(b)));

	}
	else if (mapNode->p_element->type == infElType::roundabout)
	{
		Roundabout* r = static_cast<Roundabout*>(mapNode->p_element);
		infElement*  temp;
		for (int i = 0; i < r->ends.size(); i++)
		{
			temp = r->ends[i].roads->getOther(r);
			mapNode->connections.push_back(findNode(temp));
		}

	}


}

void MapInfo::findPathsForBorder(int borderId)
{
	//ustawianie warrtosci poczatkowych
	initalizeValues(borderId);
	findParents();
	fillPaths(borderId);






}

void MapInfo::initalizeValues(int borderId)
{
	for (int i = 0; i < nodes.size(); i++)
	{
		nodes[i]->used = false;
		nodes[i]->parent = nullptr;
		if (i == borderId)
			nodes[i]->distance = 0;
		else
			nodes[i]->distance = std::numeric_limits<float>::infinity();
	}
}

void MapInfo::findParents()
{
	for (int i = 0; i < nodes.size() - 1; i++)
	{
		int id = findNearestNode();
		if (id == -1) break;

		nodes[id]->used = true;

		for (int j = 0; j < nodes[id]->connections.size(); j++)
		{
			MapNode* tempNode = nodes[id]->connections[j];
			if (tempNode->used == false
				&& nodes[id]->distance != std::numeric_limits<float>::infinity()
				&& (nodes[id]->distance + tempNode->length) < tempNode->distance)
			{
				tempNode->distance = (nodes[id]->distance + tempNode->length);
				tempNode->parent = nodes[id];
			}
		}


	}
}

void MapInfo::fillPaths(int borderId)
{
	infElement* startPtr = nodes[borderId]->p_element;
	routes[startPtr].start = startPtr;

	for (int i = 0; i < nodes.size(); i++)
	{
		if (nodes[i]->nodeType == MapNodeType::border && nodes[i]->used == true && i != borderId )
		{
			infElement* endPtr = nodes[i]->p_element;
			Route & tempRoute = routes[startPtr].routes[endPtr];
			fillPathForBorder(tempRoute, i);
			tempRoute.start = startPtr;
			tempRoute.destination = endPtr;
			tempRoute.startId = static_cast<border*>(startPtr)->id;
			tempRoute.destId = static_cast<border*>(endPtr)->id;

		}
	}
}

void MapInfo::fillPathForBorder(Route & route, int borderId)
{
	

	std::vector<int> reverseTurns;
	MapNode* tempNode = nodes[borderId];
	do
	{
		tempNode = tempNode->parent;
		if (tempNode->nodeType == MapNodeType::junctionRoute)
			reverseTurns.push_back(tempNode->junctionSide);	

	} while (tempNode->nodeType != MapNodeType::border);

	//odwrocenie kolejnosci
	for (int i = reverseTurns.size() - 1; i >= 0; i--)
	{
		route.turns.push_back(reverseTurns[i]);
	}
}

int MapInfo::findNearestNode() 
{
	int id = -1;
	float min = std::numeric_limits<float>::infinity();
	for (int i = 0; i < nodes.size(); i++)
	{
		if (nodes[i]->used == false && nodes[i]->distance < min)
		{
			id = i;
			min = nodes[i]->distance;
		}
	}
	return id;
}

MapNode* MapInfo::findNode(infElement * element)
{
	for (int i = 0; i < nodes.size(); i++)
	{
		if (nodes[i]->p_element == element)
			return nodes[i];
	}
	return nullptr;
}
