#pragma once

#include <vector>
#include "../infElement.h"



enum class MapNodeType
{
	road,
	junction,
	junctionRoute,
	roundabout,
	border

};

enum class MapRoadDirection
{
	forward,
	backward,
	both

};



class MapNode
{
public:
	MapNode();
	MapNode(infElement* element);	
	~MapNode();

	


	//todo metody pomocnicze

	MapNodeType nodeType;
	std::vector<MapNode*> connections;

	infElement* p_element;
	
	//road
	float length;
	MapRoadDirection roadDir; //mozliwosci przejazdu droga zgodnie z kolejnoscia connections

	//junctionRoute
	int junctionSide; // strona wyjazdowa ze skrzyzowania

	//do obliczania sciazek
	float distance; //odlaglosc od poczatku
	MapNode* parent;
	bool used;

};

