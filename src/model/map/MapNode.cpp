#include "MapNode.h"

#include "../road.h"



MapNode::MapNode()
{
	length = 0;
}

MapNode::MapNode(infElement* element)
{
	if (element->type == infElType::road)
	{
		nodeType = MapNodeType::road;
		length = static_cast<road*>(element)->length;
	}
	else if (element->type == infElType::junction)
	{
		nodeType = MapNodeType::junction;
		length = 0;
	}
	else if (element->type == infElType::border)
	{
		nodeType = MapNodeType::border;
		length = 0;
	}
	else if(element->type == infElType::roundabout)
	{
		nodeType = MapNodeType::roundabout;
		length = 0;
	}
	p_element = element;
}




MapNode::~MapNode()
{
}
