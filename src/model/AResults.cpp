#include "AResults.h"



AResults::AResults()
{
}


AResults::~AResults()
{
}

void AResults::newCycle()
{
	for (auto& data : routesData)
	{
		data.second.reset();
	}
}

void AResults::saveCycle()
{
	analysisOutput temp;
	for (auto& data : routesData)
	{
		analysisOutput::routeResult& res = temp.routesResults[data.first];
		res.averageTimeLoss = (data.second.sumTimeSpend / data.second.carNum) - data.second.baseTravelTime;
		res.carsPerMinute = (data.second.carsFinished - 1) / (testTime - (data.second.firstCarTime / 60));
			
	}
	
	outputs.push_back(temp);
}

void routeData::reset()
{
	sumTimeSpend = 0;
	carNum = 0;
	carsFinished = 0;
	noCarFinished = true;
}
