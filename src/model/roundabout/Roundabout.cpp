#include "Roundabout.h"



Roundabout::Roundabout(node * r1, node * r2, node * r3, node * r4, RoundaboutType rtype)
{
	//TODO poprawic na oglne narazie 20m jeden pas
	type = infElType::roundabout;
	roundaboutType = rtype; 
	center = getIntersectionPoint(r1->position, r3->position, r2->position, r4->position);

	

	ends.resize(4);
	ends[0].roads = r1;
	ends[0].roads->setElement(this);
	ends[1].roads = r2;
	ends[1].roads->setElement(this);
	ends[2].roads = r3;
	ends[2].roads->setElement(this);
	ends[3].roads = r4;
	ends[3].roads->setElement(this);

	endsNumber = 4;
	
	if (roundaboutType == RoundaboutType::turbo)
	{
		setTurbo();
		
	}

	else if (roundaboutType == RoundaboutType::oneLine)
		linesNumber = 1;
	else
		linesNumber = 2;

	setLines();


}


Roundabout::~Roundabout()
{
}

int Roundabout::nodeIndex(node * n)
{
	for (int i = 0; i < 4; i++)
	{
		if (ends[i].roads == n)
		{
			return i;
		}
	}
	return -1;
}

posibleDirections Roundabout::getTurnDirection(int outputIndex, node * input)
{
	int inputIndex = nodeIndex(input);
	int delta = inputIndex - outputIndex;
	delta = (delta + 4) % 4;
	if (delta == 1)
		return posibleDirections::right;
	else if (delta == 2)
		return posibleDirections::forward;
	else
		return posibleDirections::left;
}

void Roundabout::clear()
{
	for (RbEnd& end : ends)
	{
		end.clear();
	}
}

void Roundabout::setLines()
{
	for (int i = 0; i < ends.size(); i++)
	{
		ends[i].setAxis(center);



		road* tempRoad = static_cast<road*>(ends[i].roads->getOther(this));
		int inLinesNumber = tempRoad->getOutputLines(ends[i].roads).size();

		//pasy wjazdowe 1 do 1
		if (inLinesNumber == linesNumber)
		{
			for (int j = 0; j < linesNumber; j++)
			{
				
				ends[i].inputLines.push_back(RbInputLine(this,i,j,j));

			}

		}
		//jeden pas wjazdowy 
		else if (inLinesNumber == 1)
		{
			for (int j = 0; j < linesNumber; j++)
			{

				ends[i].inputLines.push_back(RbInputLine(this,i, 0, j));

			}
		}
		else
		{
			
			//error
			throw "incorect lines number";
		}

		int outLinesNumber = tempRoad->getInputLines(ends[i].roads).size(); //ilosc pasow na drodze po zjazdzie

		ends[i].outputLine = new RbOutputLine(this, i, linesNumber - 1, outLinesNumber - 1);
		if (ends[i].special)
		{
			ends[i].outputLine2 = new RbOutputLine(this, i, 0, 0);
		}

	}
	for (int i = 0; i < ends.size(); i++)
	{

		for (int j = 0; j < linesNumber; j++)
			{
				ends[i].insideLines.push_back(RbCircleLine(this, i, j, true));

				ends[i].afterLines.push_back(RbCircleLine(this, i, j, false));
			}
	}




}

void Roundabout::setTurbo()
{
	linesNumber = 2;

	//maja jazde prosto z obu pasow
	ends[0].special = true;	
	ends[2].special = true;

	



}
