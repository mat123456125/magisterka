#pragma once

#include "../node.h"
#include <vector>

#include "RbInputLine.h"
#include "RbOutputLine.h"
#include "RbCircleLine.h"

class RbInputLine;
class RbLine;

class RbEnd
{
public:
	RbEnd();
	~RbEnd();

	bool special;

	node* roads;

	std::vector<RbInputLine> inputLines;
	RbOutputLine* outputLine;

	RbOutputLine* outputLine2;

	std::vector<RbCircleLine> insideLines; //pasy dla pojazdow ktore nie zjezdzaja z ronda
	std::vector<RbCircleLine> afterLines; //pasy na rondzie juz za wjazdami



	vector2 xAxis;
	vector2 yAxis;

	void setAxis(vector2 center);

	void clear();

};

