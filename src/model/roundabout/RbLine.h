#pragma once

#include <list>
#include "../car.h"
#include "../BasicLine.h"


class Roundabout;

enum class RbLineType
{
	input,
	output,
	circle_in,
	circle_after

};

class RbLine :public BasicLine
{
public:
	RbLine(Roundabout* p,int in);
	~RbLine();


	Roundabout* parent;
	int inputNode; //indeks strony skrzyzowania
	RbLineType lineType;

	

	std::list<car*> cars;

	

	 virtual carPosData getCarPosData(float pos, float lcp);
	 virtual RbLine* getNextLine(int outputNode) = 0;
	 virtual RbLine* getPreviousLine() = 0;
};

