#pragma once
#include "RbLine.h"

class Roundabout;


class RbOutputLine :public RbLine
{
public:
	
	
	RbOutputLine(Roundabout * r, int in, int st, int target);
	~RbOutputLine();

	int startLine; //numer pasa na rondzie
	int targetLine; //numer pasa na drodze


	//vector2 start;
	vector2 center; //srodek elipsy ruchu
	float angle;
	//kat srodkowy ronda po wjechaniu = 90 - angle
	float t; //kat po przeliczeniu

	float a, b; //promienie elipsy


	carPosData getCarPosData(float pos, float lcp);
	RbLine* getNextLine(int outputNode);
	RbLine* getPreviousLine();

private:
	void setParameters();

};
