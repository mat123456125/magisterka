#pragma once

#include "RbLine.h"

class Roundabout;



class RbCircleLine :public RbLine
{
public:
	
	RbCircleLine(Roundabout * r, int inputN, int line, bool ins); //po input i output lines bo kozysta z danych
	~RbCircleLine();

	//bool inside; // inside czy after
	int line; //numer pasa 0 - srodek
	//katy zgodnie z uklade odniesienia zjazdu
	float startAngle;//kat srodkowy poczatek droga
	float pathAngle; //kat srodkowy przebyta droga

	bool specialLine;


	carPosData getCarPosData(float pos, float lcp);
	RbLine* getNextLine(int outputNode);
	RbLine* getPreviousLine();
	float getCarPosOnNewLine(float position);
private:
	void setParameters();

};

