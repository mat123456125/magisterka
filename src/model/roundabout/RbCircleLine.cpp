#include "RbCircleLine.h"
#include <math.h>
#include "Roundabout.h"





RbCircleLine::RbCircleLine(Roundabout * r, int inputN, int l, bool ins) : RbLine(r, inputN)
{
	if(ins)
		lineType = RbLineType::circle_in;
	else
		lineType = RbLineType::circle_after;
	line = l;

	specialLine = false;

	setParameters();
}

RbCircleLine::~RbCircleLine()
{
}

carPosData RbCircleLine::getCarPosData(float pos, float lcp)
{
	carPosData posData;

	float radius = ROUNDOUBOUT_INSIDE_RADIUS + (0.5f + line)* ROUNDOUBOUT_LINE_WIDTH -  lcp * ROUNDOUBOUT_LINE_WIDTH; // moze zanienic na zmienna?

	float theta = pos / length * pathAngle;
	theta = theta + startAngle;
	vector2 yAxis = parent->ends[inputNode].yAxis;
	vector2 xAxis = parent->ends[inputNode].xAxis;


	vector2 local;
	
	if (specialLine)
	{
		radius -= ROUNDOUBOUT_LINE_WIDTH*(1-(pos / length));
	}

	local = yAxis * (radius*sin(theta)) + xAxis * (radius*cos(theta));


	posData.absPositon = parent->center + local;

	

	posData.angle = -theta + atan2(-xAxis.x, xAxis.y);


	return posData;
}

RbLine * RbCircleLine::getNextLine(int outputNode)
{
	if (lineType == RbLineType::circle_after)
	{
		int nextNodeId = inputNode - 1;
		if (nextNodeId < 0)
			nextNodeId = 3;
		if (parent->roundaboutType == RoundaboutType::turbo)
		{
			if (outputNode == nextNodeId) //zjazd z ronda
			{
				if (line == 0)
					return parent->ends[nextNodeId].outputLine2;
				else
					return parent->ends[nextNodeId].outputLine;
			}
			else //pozostanie
			{
				if(parent->ends[nextNodeId].special)
					return &(parent->ends[nextNodeId].insideLines[1]);
				else
					return &(parent->ends[nextNodeId].insideLines[line]);
			}

		}

		if (outputNode == nextNodeId) //zjazd z ronda
		{
			return parent->ends[nextNodeId].outputLine;
		}
		else //pozostanie
		{
			return &(parent->ends[nextNodeId].insideLines[line]);
		}

	}
	else
	{
		return &(parent->ends[inputNode].afterLines[line]);
	}
}

RbLine * RbCircleLine::getPreviousLine()
{
	if (lineType == RbLineType::circle_in)
	{
		int nextNodeId = inputNode + 1;
		nextNodeId = nextNodeId % 4;
		{
			return &(parent->ends[nextNodeId].afterLines[line]);
		}

	}
	else
	{
		return &(parent->ends[inputNode].insideLines[line]);
	}
}

float RbCircleLine::getCarPosOnNewLine(float position)
{
	if (line + 1 == parent->linesNumber)
		return 0;

	RbCircleLine* newLine;
	if (lineType == RbLineType::circle_in)
		newLine = &(parent->ends[inputNode].insideLines[line + 1]);
	else
		newLine = &(parent->ends[inputNode].afterLines[line + 1]);

	//przeliczanie przy zmiennym kacie poczatkowym
	//theta 1 = theta 2
	float curTheta = position / length * pathAngle;
	curTheta = curTheta + startAngle;

	
	float newPos = curTheta - newLine->startAngle;
	newPos = newPos / newLine->pathAngle;
	newPos = newPos * newLine->length;


	return newPos;
		
}

void RbCircleLine::setParameters()
{
	//pomiedzy zjazdem a wjazdem na rondo
	if (lineType == RbLineType::circle_in)
	{

		if (parent->ends[inputNode].special)
		{
			startAngle = -M_PI + parent->ends[inputNode].outputLine2->angle;
			specialLine = true;
		}
		else
			startAngle = -M_PI + parent->ends[inputNode].outputLine->angle;
		float endAngle = -parent->ends[inputNode].inputLines[line].angle;
		pathAngle = endAngle - startAngle;

		float radius = ROUNDOUBOUT_INSIDE_RADIUS + (0.5f + line)* ROUNDOUBOUT_LINE_WIDTH;
		length = pathAngle * radius;

	}
	//pomiedzy wjazdem a kolejnym zjazdem
	else
	{
		
		startAngle = -parent->ends[inputNode].inputLines[line].angle;
		int tempNodeId = inputNode + 1;
		tempNodeId = tempNodeId % 4;
		float endAngle;
		if(parent->ends[tempNodeId].special && line == 0)
			endAngle = -M_PI * 0.5f + parent->ends[tempNodeId].outputLine2->angle;
		else
			endAngle = -M_PI * 0.5f + parent->ends[tempNodeId].outputLine->angle;
		pathAngle = endAngle - startAngle;

		float radius = ROUNDOUBOUT_INSIDE_RADIUS + (0.5f + line)* ROUNDOUBOUT_LINE_WIDTH;
		length = pathAngle * radius;

	}
}
