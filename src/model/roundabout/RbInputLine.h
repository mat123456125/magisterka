#pragma once

#include "RbLine.h"

class Roundabout;


class RbInputLine :public RbLine
{
public:
	
	RbInputLine(Roundabout * r, int in, int st, int target);
	~RbInputLine();

	int startLine; //numer pasa na drodze
	int targetLine; //numer pasa na rondzie


	//vector2 start;
	vector2 center; //srodek elipsy ruchu
	float angle; //kat bez przeliczenia
	//kat srodkowy ronda po wjechaniu = 90 - angle
	float t; //kat jako parametr wzoru parametrycznego

	float a, b; //promienie elipsy
	

	carPosData getCarPosData(float pos, float lcp);
	RbLine* getNextLine(int outputNode);
	RbLine* getPreviousLine();

private:
	void setParameters();
	
};

