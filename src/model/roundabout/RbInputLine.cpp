#include "RbInputLine.h"
#include <math.h>
#include "Roundabout.h"







RbInputLine::RbInputLine(Roundabout * r,int in, int st, int target): RbLine(r,in)
{
	lineType = RbLineType::input;
	startLine = st;
	targetLine = target;

	setParameters();
}


RbInputLine::~RbInputLine()
{
}

carPosData RbInputLine::getCarPosData(float pos, float lcp)
{
	carPosData posData;

	float theta = pos / length * t;
	vector2 yAxis = parent->ends[inputNode].yAxis;
	vector2 xAxis = parent->ends[inputNode].xAxis;


	vector2 local;
	local = yAxis * (b*sin(theta)) - xAxis * (a*cos(theta));
	

	posData.absPositon = center + local;

	theta = (pos) / length * angle;
	
	posData.angle = theta + atan2(-xAxis.x, xAxis.y);


	return posData;
}

RbLine * RbInputLine::getNextLine(int outputNode)
{
	return &(parent->ends[inputNode].afterLines[targetLine]);
}

RbLine * RbInputLine::getPreviousLine()
{
	return nullptr;
}

void RbInputLine::setParameters()
{
	a = ROUNDOUBOUT_ENTER_RADIUS; 
	float centerDistance = a + 3.0f / 2 + startLine * 3.0f; //odleglosc srodka elipsy od noda
	float r = ROUNDOUBOUT_INSIDE_RADIUS + (0.5f + targetLine)* ROUNDOUBOUT_LINE_WIDTH; //promien pasa ronda srodek
	float h; //odleglosc node od pasa ronda
	h = ROUNDOUBOUT_INSIDE_RADIUS + parent->linesNumber*ROUNDOUBOUT_LINE_WIDTH - r;
	

	center = parent->ends[inputNode].roads->position + parent->ends[inputNode].xAxis*(centerDistance);

	angle = atan2(r + h, centerDistance);

	float y = r + h - (r * sin(angle));
	float x = y / tanf(angle);
	t = acosf(x / a);

	b = y / sin(t);

	

	length = (M_PI*(1.5*(a + b) - sqrt(a*b))); //dlugosc 
	length = length * (t / (2.0f*M_PI)); //kawalek cwiartki przyblizenie


}
