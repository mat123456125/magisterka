#include "RbEnd.h"



RbEnd::RbEnd()
{
	special = false;
	outputLine2 = nullptr;
}


RbEnd::~RbEnd()
{
	delete outputLine;
}

void RbEnd::setAxis(vector2 center)
{
	xAxis = (center - roads->position).normalize().perpendicular();
	yAxis = (center - roads->position).normalize();

}

void RbEnd::clear()
{
	for (RbCircleLine& l : insideLines)
	{
		l.cars.clear();
	}
	for (RbCircleLine& l : afterLines)
	{
		l.cars.clear();
	}
	for (RbInputLine& l : inputLines)
	{
		l.cars.clear();
	}
	 
	if (outputLine != nullptr)
		outputLine->cars.clear();
	if (outputLine2 != nullptr)
		outputLine2->cars.clear();

}
