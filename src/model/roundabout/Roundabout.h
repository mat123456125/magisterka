#pragma once

#include "../infElement.h"
#include <vector>
#include "RbEnd.h"


#define ROUNDOUBOUT_INSIDE_RADIUS 6.0f
#define ROUNDOUBOUT_LINE_WIDTH 4.0f
#define ROUNDOUBOUT_ENTER_RADIUS 5.0f

class RbEnd;

enum class RoundaboutType
{
	oneLine,
	twoLine,
	turbo
};

class Roundabout :public infElement
{
public:
	Roundabout(node * r1, node * r2, node * r3, node * r4, RoundaboutType type);//zgodnie z ruchem ws zegara
	~Roundabout();

	RoundaboutType roundaboutType;

	std::vector<RbEnd> ends;
	int endsNumber;
	int linesNumber;

	vector2 center;

	int nodeIndex(node* n);
	posibleDirections getTurnDirection(int outputIndex, node* input);

	void clear();

private:
	void setLines();
	void setTurbo();
};

