#pragma once

#include "infElement.h"
#include "../util/vector2.h"
#include <exception>

enum class nodeType
{
	roadToRoad,
	junctionLights,
	junctionNoLights,
	border
};

//polaczenie 2 elementow
class node
{
public:
	node();
	node(vector2 p);
	~node();

	infElement* first;
	infElement* second;
	nodeType type;
	vector2 position;

	infElement* getOther(infElement* caller);
	void setElement(infElement* caller);



};

