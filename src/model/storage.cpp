#include "storage.h"

using namespace std;

storage::storage()
{ 
	hiddenSim = false;
	//init();
}


storage::~storage()
{
	list<car*>::iterator it;
	for (it = cars.begin(); it != cars.end();)
	{
		delete *it;
		it = cars.erase(it);
	}
	int size = nodes.size();
	for (int i = 0; i < size; i++)
	{
		delete nodes[i];
	}
	size = elements.size();
	for (int i = 0; i < size; i++)
	{
		delete elements[i];
	}

}

void storage::init()
{
	mapLoader.load();
	fixRoadLines();
	mapInfo.createPaths();
	accelerationExponent = 4;
	if (propertiesTree.empty() == false)
		loadBordersProperties();
}

void storage::loadProperties()
{
	
	/*for (auto &test : propertiesTree.get_child("config"))
	{
		int i = 0;
	}*/
	//auto test = 
	mapLoader.fileName = propertiesTree.get<std::string>("config.map");
	hiddenSim = propertiesTree.get<bool>("config.speed.<xmlattr>.fast");
	
	

}

void storage::fixRoadLines()
{
	for (infElement* temp : elements)
	{
		if (temp->type == infElType::road)
		{
			static_cast<road*>(temp)->fixLines();
		}
	}
}

void storage::loadBordersProperties()
{
	
	for (auto &br : propertiesTree.get_child("config.borders"))
	{
		int id = br.second.get<int>("id");
		for (auto &dest : br.second.get_child("destinations"))
		{
			int destId = dest.second.get<int>("id"); 
			float destProb = dest.second.get<float>("probability");
			borders[id]->destinationsProbability.push_back(border::destProb(destId, destProb));
		}		
	}
}
