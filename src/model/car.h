#pragma once

#include "../util/vector2.h" 
#include "road.h"
#include "border.h"
#include "junction\junction.h"
#include "map/Route.h"
#include "BasicLine.h"
#include "Driver.h"



class roadLine;
enum class posibleDirections;
class border;
class junctionLine;
class junctionInput;
class road;
class junction;
class RbLine;
class Roundabout;

enum class trafficLightsColors;

struct carPosData
{
	vector2 absPositon;
	float angle;
};



class car
{

	struct ClosestCar
	{
		car* p_car;
		float distance;
	};


public:
	car();
	~car();
	car(infElement* element);

	//TODO dodac type
	//TODO przesunac orygin na srpdek pojazdu
	float lenght;

	//pozycja bezwzgledna
	vector2 absPositon;
	float angle;

	//element potrzebny do naprawiania kata pojazdu
	BasicLine* lastLine;
	float tempCarCenterDist;

	//pozycja wzgledna
	infElement* onElement;
	bool direction; //true - zgodny z kierunkiem drogi start->end
	float positionOnRoad;

	float speed;


	//droga
	int line; //0 - pas najblizej srodka
	roadLine* m_roadLine;

	//skrzyzowania
	junctionLine* m_junctionLine;
	junctionInput* m_junctionInput;
	junctionLine* nextJunctionLine;

	//rondo
	RbLine* m_rbLine;
	Roundabout* nextRoundabout;
	int nextRoundaboutEnd;
	bool changingLinesRb;
	RbLine* old_rbLine;


	//zmiana pasa droga
	bool changingLines;
	int oldLine;
	roadLine* old_roadLine;
	roadLine* futureRoadLine;
	float lineChangeProgres; //1 - calkowita zmiana pasa
	float lineChangeSpeed;
		

	//przejechanie z elementu na element
	bool changingElements;
	infElement* oldElement;

	//cechy kierowcy do symulacji
	bool blind; //ignorowanie innych samochodow i sygnalizacji
	Driver driver;

	float curMaxSpeed;
	
	

	//potrzeby
	
	bool needToChangeLine;
	bool needToChangeLineRb;
	bool needToGiveWay;
	int preferedLine;
	border* destination;
	int aliveTime;

	bool junctionNoSpace;

	//trasa 
	Route* route;
	int routeProgress;	

	

	


	int run(float deltaT); //return 1 - delete car
	void init();
	//void calculatePath();

	float getDistanceToJunction(); //tylko nextToJunction
	void calculateAbsPos();
private:
	

	ClosestCar checkRoad();
	ClosestCar checkNextRoad(road* nextR, node* nextN, int l_number, float dist);
	ClosestCar checkLine(roadLine* l);
	ClosestCar checkNextJunction(junction* nextJ, node* nextN, float dist);
	ClosestCar checkJunction();
	ClosestCar checkNextRoundabout(Roundabout* nextR, node* nextN, float dist);
	
	ClosestCar checkRbLine(RbLine* nextL, float dist);

	ClosestCar getClosestCarOnRoad();
	ClosestCar getClosestCarOnJunction();
	ClosestCar getClosestCarOnRoundabout();

	car* getClosestCarForPriority();
	car* getClosestCarForPriorityRb();

	void calculatePrefLine();


	bool checkLineForChange(roadLine* l);
	ClosestCar checkForChangeForward(roadLine* l);
	ClosestCar checkForChangeBack(roadLine* l);

	bool checkLineForChangeRb();
	ClosestCar checkForChangeForwardRb();
	ClosestCar checkForChangeBackRb();


	int checkElementChange();
	void startLineChange(); //poczatek zmiany pasa samochod na obu pasach jednoczesnie
	void startLineChangeRb();

	void setFutureLine();

	
	void calculateMotion(float deltaT);
	float calculateIDM(float deltaT, float gap, float deltaV);
	void moveCar(float deltaT, float acceleration);

	float checkLineForSpace(roadLine* l); // czy jest miejsce na przejechanie przez skrzyzowanie
	

	void checkJunctionForPriority(float deltaT);
	void checkJunctionForSpace(junction * nextJ); //tylko jesli nie ma nic z przodu 
	void checkRoundaboutForPriority(float deltaT);
	

	void onNewRoad();
	void onNewJunction();
	void onJunctionLeave();
	void onPreferedLine();
	void onNewRoundabout();
	void onRoundaboutLeave();
	


	
	//dane do symulacji
	//dane z otoczenia
	
	ClosestCar closestCar;
	float distanceToJunction;
	trafficLightsColors nextTrafficLightsColor;
	
	bool junctionBlocked;
	bool junctionOnPriority;
	
	bool ignorePriority;
	bool waitingForPriority;
	float timeOnPriority;
	float timeOnChangeLine; //czas oczekiwania na zmiane pasa
 

	
	//uwzglednianie nextJunctionLine
	//sprawdzanie blokowania ustawianie blokowania
	//todo zmiana nextJunctionLine w roznych sytuacjach






};



