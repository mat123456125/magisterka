#pragma once
#include <fstream>
#include <vector>

class road;

class MapLoader
{
public:
	MapLoader();
	~MapLoader();

	std::string fileName;

	void load();
private:
	void loadNodes(std::vector<std::string>::iterator &it);
	void loadRoads(std::vector<std::string>::iterator &it);
	void loadBorders(std::vector<std::string>::iterator &it);
	void loadRoadDetails(std::vector<std::string>::iterator &it,road* tempRoad);
	void tokenize();
	void parse();


	std::vector<std::string> lines;
	std::vector<std::string> tokens;
};

