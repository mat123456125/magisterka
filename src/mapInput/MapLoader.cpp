#include "MapLoader.h"
#include "../model/storage.h"
#include <string>
#include <sstream> 
#include <cctype>
#include <cstdlib>
#include "..\model\junction\simpleJunctionLights4.h"
#include "..\model\junction\simpleJunctionLights3.h"
#include "..\model\junction\simpleJunction4.h"
#include "..\model\junction\forwardJunctionLights4.h"
#include "..\model\junction\forwardJunctionLights4Smart1.h"
#include "..\model\junction\forwardJunctionLights4Smart2.h"
#include "..\model\junction\linesJunctionLights4.h"
#include "..\model\roundabout\Roundabout.h"

using namespace std;

MapLoader::MapLoader()
{
	fileName = std::string("map9.txt");
}


MapLoader::~MapLoader()
{
}

void MapLoader::load()
{

	std::ifstream is(std::string("mapy/") + fileName);
	if (!is)
		throw "Cannot open file";
	
	std::string line;

	while (is && !is.eof())
	{
		std::getline(is, line);
		lines.push_back(line);

	}
	is.close();

	tokenize();

	parse();


}

void MapLoader::loadNodes(std::vector<std::string>::iterator &it)
{
	float x, y;
	it++;
	while (it != tokens.end())
	{
		if (*it != "\t")
			break;
		else				//pojedynczy wiersz - node
		{
			it++;
			if (it->front() == '-' || isdigit(it->front()))
			{
				x = atof(it->c_str());
			}
			else throw "blad";

			it++;
			if (it->front() == '-' || isdigit(it->front()))
			{
				y = atof(it->c_str());
			}
			else throw "blad";

			storageObj.nodes.push_back(new node(vector2(x, y)));

			while (it != tokens.end() && *it !="\n")
			{
				it++;
			}
			
		}

		it++;
	}
	it--;	
}

void MapLoader::loadRoads(std::vector<std::string>::iterator &it)
{
	int lf, lb;
	int n1, n2;
	road* tempRoad;

	it++;
	while (it != tokens.end())
	{
		if (*it != "\t")
			break;
		else				//pojedynczy wiersz - road
		{
			it++;
			if (*it == "node")
			{
				it++;
				n1 = atoi(it->c_str());
			}
			else throw "blad";

			it++;
			if (*it == "node")
			{
				it++;
				n2 = atoi(it->c_str());
			}
			else throw "blad";

			it++;
			if (isdigit(it->front()))
			{
				lf = atoi(it->c_str());
			}
			else throw "blad";

			it++;
			if (isdigit(it->front()))
			{
				lb = atoi(it->c_str());
			}
			else throw "blad";

			tempRoad = new road(storageObj.nodes[n1], storageObj.nodes[n2], lf, lb);
			

			//storageObj.nodes.push_back(new node(vector2(x, y)));

			while (it != tokens.end() && *it != "\n")
			{
				it++;
			}
			if (it != tokens.end())
			{
				it++;
				if (it != tokens.end() && *it == "\t")
				{
					it++;
					if (it != tokens.end() && *it == "\t")
					{
						it--;
						it--;
						loadRoadDetails(it, tempRoad);
					}
					else
					{
						it--;
						it--;
					}
				}
				else
				{
					it--;
				}

			}

			

			storageObj.elements.push_back(tempRoad);

		}

		it++;
	}
	it--;


}

void MapLoader::loadBorders(std::vector<std::string>::iterator & it)
{
	float a;
	int n1;

	it++;
	while (it != tokens.end())
	{
		if (*it != "\t")
			break;
		else				//pojedynczy wiersz 
		{
			it++;
			if (*it == "node")
			{
				it++;
				n1 = atoi(it->c_str());
			}
			else throw "blad";

			it++;
			if ( isdigit(it->front()))
			{
				a = atof(it->c_str());
			}
			else throw "blad";

			border* b = new border(storageObj.nodes[n1], a);
			storageObj.elements.push_back(b);
			storageObj.borders.push_back(b);
			b->id = storageObj.borders.size() - 1;

			while (it != tokens.end() && *it != "\n")
			{
				it++;
			}

		}

		it++;
	}
	it--;
}

void MapLoader::loadRoadDetails(std::vector<std::string>::iterator & it, road * tempRoad)
{
	int l;
	it++;
	it++;
	while (it != tokens.end())
	{
		if (*it != "\t")
			break;

		it++;
		if (*it == "nextToJunction")
		{
			tempRoad->nextToJunction = true;
		}
		else if(*it == "linesForward")
		{
			it++;
			l = atoi(it->c_str());
			it++;
			if(*it == "Left")
				tempRoad->linesForward[l].setLeft();
			else if (*it == "Forward")
				tempRoad->linesForward[l].setForward();
			else if (*it == "Right")
				tempRoad->linesForward[l].setRight();
			else if (*it == "ForwardRight")
				tempRoad->linesForward[l].setForwardRight();
			else if (*it == "ForwardLeft")
				tempRoad->linesForward[l].setForwardLeft();
			else if (*it == "ForwardSides")
				tempRoad->linesForward[l].setForwardSides();
			else throw "blad";
		}
		else if (*it == "linesBack")
		{
			it++;
			l = atoi(it->c_str());
			it++;
			if (*it == "Left")
				tempRoad->linesBack[l].setLeft();
			else if (*it == "Forward")
				tempRoad->linesBack[l].setForward();
			else if (*it == "Right")
				tempRoad->linesBack[l].setRight();
			else if (*it == "ForwardRight")
				tempRoad->linesBack[l].setForwardRight();
			else if (*it == "ForwardLeft")
				tempRoad->linesBack[l].setForwardLeft();
			else if (*it == "ForwardSides")
				tempRoad->linesBack[l].setForwardSides();
			else throw "blad";

		}



		while (it != tokens.end() && *it != "\n")
		{
			it++;
		}

		it++;
		it++;

	}

	it--;
	it--;

}

void MapLoader::tokenize()
{
	bool notEmptyLine;
	for (int i = 0; i < lines.size(); i++)
	{
		std::string temp = "";
		notEmptyLine = false;

		if (lines[i] == "")
			continue;

		for (string::iterator j = lines[i].begin(); j != lines[i].end(); j++)
		{
			if(*j == '\t') tokens.push_back("\t");
			else if (isalpha(*j))
			{
				notEmptyLine = true;
				while (j != lines[i].end() && (isalpha(*j)|| *j == ':')) //slowa
				{
					temp.push_back(*j);
					j++;
				}
				tokens.push_back(temp);
				temp = "";
				j--;
			}
			else if (*j == '-' || isdigit(*j)) //liczby
			{
				notEmptyLine = true;
				do 
				{
					temp.push_back(*j);
					j++;
				} while (j != lines[i].end() && isdigit(*j));
				tokens.push_back(temp);
				temp = "";
				j--;
			}


		}



		if (notEmptyLine)
		{
			tokens.push_back("\n");
		}
	}
}

void MapLoader::parse()
{

	for (vector<string>::iterator i = tokens.begin(); i != tokens.end(); i++)
	{
		if (*i == "node:")
		{
			i++;
			if(*i == "\n")
				loadNodes(i);
		}
		else if (*i == "road:")
		{
			i++;
			if (*i == "\n")
				loadRoads(i);
		}
		else if (*i == "border:")
		{
			i++;
			if (*i == "\n")
				loadBorders(i);

		}
		else if (*i == "simpleJunctionLights")
		{
			int n[4];
			i++;
			if (atoi(i->c_str()) == 4)
			{
				for (int j = 0; j < 4; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new simpleJunctionLights4(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]])); //zgodnie z ruchem ws zegara
			}
			else if (atoi(i->c_str()) == 3)
			{
				for (int j = 0; j < 3; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new simpleJunctionLights3(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]])); //1,2 na wprost 3 pod katem prostym zgodnie z rwz
			}
			i++;

		}
		else if (*i == "simpleJunction")
		{
			int n[4];
			i++;
			if (atoi(i->c_str()) == 4)
			{
				for (int j = 0; j < 4; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new simpleJunction4(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]])); //zgodnie z ruchem ws zegara
			}
			else if (atoi(i->c_str()) == 3)
			{
				for (int j = 0; j < 3; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new simpleJunctionLights3(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]])); //1,2 na wprost 3 pod katem prostym zgodnie z rwz
			}
			i++;
			
		}
		else if (*i == "forwardJunctionLights")
		{
			int n[4];
			i++;
			if (atoi(i->c_str()) == 4)
			{
				int type = 0;
				i++;
				if (*i == "Smart")
				{
					i++;
					if (atoi(i->c_str()) == 1)
						type = 1;
					else if (atoi(i->c_str()) == 2)
						type = 2;
					else
						throw "blad";
				}
				else
					i--;
				for (int j = 0; j < 4; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				if(type == 0)
					storageObj.elements.push_back(new forwardJunctionLights4(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]])); //zgodnie z ruchem ws zegara
				else if(type == 1)
					storageObj.elements.push_back(new forwardJunctionLights4Smart1(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]]));
				else
					storageObj.elements.push_back(new forwardJunctionLights4Smart2(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]]));
			}
			else if (atoi(i->c_str()) == 3)
			{
				for (int j = 0; j < 3; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new simpleJunctionLights3(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]])); //1,2 na wprost 3 pod katem prostym zgodnie z rwz
			}
			i++;

		}
		else if (*i == "linesJunctionLights")
		{
			int n[4];
			i++;
			if (atoi(i->c_str()) == 4)
			{
				for (int j = 0; j < 4; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new linesJunctionLights4(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]])); //zgodnie z ruchem ws zegara
			}
			else if (atoi(i->c_str()) == 3)
			{
				for (int j = 0; j < 3; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new simpleJunctionLights3(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]])); //1,2 na wprost 3 pod katem prostym zgodnie z rwz
			}
			i++;

		}
		else if (*i == "roundabout")
		{
			int n[4];
			i++;
			if (atoi(i->c_str()) == 1)
			{
				for (int j = 0; j < 4; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new Roundabout(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]], RoundaboutType::oneLine)); //zgodnie z ruchem ws zegara
			}
			else if (atoi(i->c_str()) == 2)
			{
				for (int j = 0; j < 4; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new Roundabout(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]], RoundaboutType::twoLine)); //zgodnie z ruchem ws zegara
			}
			else if (*i == "T")
			{
				for (int j = 0; j < 4; j++)
				{
					i++;
					if (*i != "node")
						throw "blad";
					i++;
					n[j] = atoi(i->c_str());
				}

				storageObj.elements.push_back(new Roundabout(storageObj.nodes[n[0]], storageObj.nodes[n[1]], storageObj.nodes[n[2]], storageObj.nodes[n[3]], RoundaboutType::turbo)); //zgodnie z ruchem ws zegara

			}
			i++;

		}
	}

		


}
