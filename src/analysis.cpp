#include "analysis.h"
#include <fstream>
#include <ctime>
#include <string>
#include "model\junction\simpleJunctionLights4.h"



analysis::analysis(simulation* sim)
{
	

	ptr_simulation = sim;
	storageObj.threadSyncro.stopThread = false;
	storageObj.threadSyncro.threadRunning = true;

	storageObj.aResults.currentTime = 0;
	storageObj.aResults.testNumber = 0;
	
}


analysis::~analysis()
{
}

void analysis::operator()()
{
	setBaseTravelTime();

	for (int j = 0; j < storageObj.aResults.inputs.size(); j++)
	{
		storageObj.aResults.newCycle();
		prepareCycle(storageObj.aResults.inputs[j]);  //ustawianie szczegolow mapy
		storageObj.aResults.testNumber = j + 1;

		for (int i = 0; i < cyclesNumber; i++)
		{
			storageObj.aResults.currentTime = i * FRAME_TIME ;
			ptr_simulation->run();
			if (storageObj.threadSyncro.stopThread)
				break;
		}
		if (storageObj.threadSyncro.stopThread)
			break;
		

		storageObj.aResults.saveCycle();


	}


	if (!storageObj.threadSyncro.stopThread)
	{
		saveResults();
		showMessagebox();
		
	}



	std::unique_lock<std::mutex> lck(storageObj.threadSyncro.threadStoping_mtx);
	storageObj.threadSyncro.threadRunning = false;
	storageObj.threadSyncro.threadStoping.notify_all();
	
}

void analysis::stopAnalisis()
{
	std::unique_lock<std::mutex> lck(storageObj.threadSyncro.threadStoping_mtx);
	storageObj.threadSyncro.stopThread = true;
	while (storageObj.threadSyncro.threadRunning) storageObj.threadSyncro.threadStoping.wait(lck);
}

void analysis::loadSettings()
{
	//TODO ---- zmienic na ptree
	// pomyslec co z input file
	if (storageObj.propertiesTree.empty())
	{
		std::ifstream is("analiza/input.txt");
		if (!is)
			throw "Cannot open file";

		float v;
		is >> v;

		storageObj.aResults.testTime = v; //min

		while (is && (!is.eof()))
		{
			float cpm; float lightTime;
			is >> cpm >> lightTime >> v;
			analysisInput temp;
			temp.carPerMin = cpm;
			temp.greenLightTime = lightTime;
			temp.maxV = v;
			storageObj.aResults.inputs.push_back(temp);


		}
		is.close();
	}
	else
	{
		storageObj.aResults.testTime = storageObj.propertiesTree.get<float>("config.tests.<xmlattr>.testTime");
		for (auto &tr : storageObj.propertiesTree.get_child("config.tests"))
		{
			if (tr.first == "test")
			{
				analysisInput temp;
				temp.carPerMin = tr.second.get<float>("cpm");
				temp.maxV = 30.0;//TODO poprawic
				
				auto bor = tr.second.get_child_optional("borders");
				if (bor)
					temp.customBorders = &(*bor);
				else
					temp.customBorders = nullptr;
				
				storageObj.aResults.inputs.push_back(temp);

			}			
		}
	}

	testTime = storageObj.aResults.testTime; //min
	float timeSec = testTime * 60;
	cyclesNumber = timeSec / FRAME_TIME;
}

void analysis::saveResults()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, sizeof(buffer), "%Y-%m-%d-%H%M%S", timeinfo);
	std::string filename(buffer);

	filename = "analiza/output " + filename + ".txt";

	std::ofstream os(filename);
	if (!os)
		throw "Cannot open file";

	os << storageObj.aResults.testTime << "\n";
	os << "carsPerMinute\tgreenLightTime\tmaxV\t|\t";
	for (auto& res : storageObj.aResults.outputs[0].routesResults)
	{
		os << res.first.first << "-" << res.first.second << " "; //trasa przejazdu
		os << "atl\t";											// averageTimeLoss
		os << res.first.first << "-" << res.first.second << " "; //trasa przejazdu
		os << "cpm\t";											//carsPerMinute
	}
	os << "\n";

	for (int i = 0; i < storageObj.aResults.inputs.size(); i++)
	{
		os << storageObj.aResults.inputs[i].carPerMin << "\t"
			<< storageObj.aResults.inputs[i].greenLightTime << "\t"
			<< storageObj.aResults.inputs[i].maxV << "\t|\t";

		for (auto& res : storageObj.aResults.outputs[i].routesResults)
		{
			os << res.second.averageTimeLoss << "\t";
			os << res.second.carsPerMinute << "\t";
		}
	
		os << "\n";
	}
	

	//zapis do pliku

}

void analysis::prepareCycle(analysisInput inp)
{
	
	float spc = 60.0f / inp.carPerMin;
	for (border* ele : storageObj.borders)
	{
		ele->secPerCar = spc;
		ele->maxSpeed = inp.maxV;
	}
	if (inp.customBorders != nullptr)
	{
		for (auto &br : inp.customBorders->get_child(""))
		{
			int id = br.second.get<int>("id");
			float carPerMin = br.second.get<float>("cpm");
			storageObj.borders[id]->secPerCar = 60.0f / carPerMin;
		}		
	}
	
	for (infElement* ele : storageObj.elements)
	{
		if (ele->type == infElType::junction)
		{
			//dynamic_cast<simpleJunctionLights4*>(ele)->GreenLightTime = inp.greenLightTime;
			break;
		}
		
	}
	ptr_simulation->removeAllCars();
	


}

void analysis::showMessagebox()
{
	SDL_Window* window = ox::getStage()->getAssociatedWindow();
	SDL_RaiseWindow(window);
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION,
		"koniec",
		"koniec", window);
	
}

void analysis::setBaseTravelTime()
{
	for (auto &start : storageObj.mapInfo.routes)
	{
		border* startBorder = static_cast<border*>(start.first);
		int startId = startBorder->id;
		for (auto &dest : start.second.routes)
		{
			int destId = static_cast<border*>(dest.first)->id;
			car* tempCar = startBorder->generateBlindCar(destId);
			float baseTime = calculateBaseTravelTime(tempCar);
			delete tempCar;
			storageObj.aResults.routesData[std::make_pair(startId, destId)].baseTravelTime = baseTime;

		}
	}
}

float analysis::calculateBaseTravelTime(car * c)
{
	int res;

	do
	{
		res = c->run(FRAME_TIME);
	} while (res == 0);
	c->m_roadLine->cars.remove(c);
	return res * FRAME_TIME;
}
