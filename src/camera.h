#pragma once

#include "oxygine-framework.h"



class camera : public oxygine::Actor
{
public:
	camera();
	~camera();

	oxygine::spActor _cameraTranslate;
	oxygine::spActor _cameraZoom;

	void addChildToCamera(oxygine::spActor actor);

	void translate(oxygine::Vector2 v);
	void zoom(float power);

	void mouseDown(oxygine::Event* event);
	void mouseUp(oxygine::Event* event);
	void mouseMove(oxygine::Event* event);
	void mouseWheel(oxygine::Event* event);

private:
	bool pressed = false;
	oxygine::Vector2 lastPos;


};

typedef oxygine::intrusive_ptr<camera> spCamera;


