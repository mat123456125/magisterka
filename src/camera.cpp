#include "camera.h"


using namespace oxygine;

camera::camera()
{
	setSize(getStage()->getSize());

	
	addEventListener(TouchEvent::TOUCH_DOWN, CLOSURE(this, &camera::mouseDown));
	addEventListener(TouchEvent::MOVE, CLOSURE(this, &camera::mouseMove));
	addEventListener(TouchEvent::TOUCH_UP, CLOSURE(this, &camera::mouseUp));
	addEventListener(TouchEvent::WHEEL_DIR, CLOSURE(this, &camera::mouseWheel));

	_cameraZoom = new Actor();
	_cameraZoom->setPosition(getSize() / 2);

	addChild(_cameraZoom);
	_cameraTranslate = new Actor();
	_cameraZoom->addChild(_cameraTranslate);

	zoom(5);
}


camera::~camera()
{
}

void camera::addChildToCamera(oxygine::spActor actor)
{
	_cameraTranslate->addChild(actor);
}

void camera::translate(oxygine::Vector2 v)
{
	_cameraTranslate->setPosition(_cameraTranslate->getPosition() + v);
}

void camera::zoom(float power)
{
	_cameraZoom->setScale(_cameraZoom->getScale() *(1 + power));// Vector2( power,power));
}

void camera::mouseDown(Event * event)
{
	TouchEvent*  te = static_cast<TouchEvent*>(event);
	lastPos = te->localPosition;
	pressed = true;
}

void camera::mouseUp(oxygine::Event * event)
{
	pressed = false;
}

void camera::mouseMove(oxygine::Event* event)
{
	if (pressed == true)
	{
		TouchEvent*  te = static_cast<TouchEvent*>(event);
		Vector2 move = te->localPosition - lastPos;
		translate(move / _cameraZoom->getScale().x);
		lastPos = te->localPosition;		
	}
	
}

void camera::mouseWheel(oxygine::Event * event)
{
	TouchEvent*  te = static_cast<TouchEvent*>(event);
	Vector2 wheel = te->wheelDirection;
	zoom(wheel.y * 0.2);


}


