#include "ox/oxygine.hpp"
#include "ox/Stage.hpp"
#include "ox/DebugActor.hpp"
#include "example.h"
#include "model\storage.h"
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <sstream>
#include <Windows.h>

namespace po = boost::program_options;


using namespace oxygine;

int checkArgs(int argc, char* argv[])
{
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("map", po::value<std::string>(), "mapa")
		("prop", po::value<std::string>(), "properties")		
		("hidden,h", "szybka symulacja")
		;

	po::positional_options_description p;
	p.add("map", 1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm); //TODO porawic sprawdzanie poprawnosci
	po::notify(vm);

	if (vm.count("help")) {
		std::stringstream str;

		str << desc << "\n";
		OutputDebugStringA(str.str().c_str());

		
		return 1;
	}

	if (vm.count("prop")) {

		std::string fileName = vm["prop"].as<std::string>();
		std::ifstream is( fileName);
		if (!is)
			throw "Cannot open file";
		boost::property_tree::read_xml(is, storageObj.propertiesTree);
		is.close();		

		storageObj.loadProperties();

		return 0;
	}

	if (vm.count("map"))
	{
		storageObj.mapLoader.fileName = vm["map"].as<std::string>();
		
	}
	else
	{
		OutputDebugStringA("Brak mapy\n");
		return 1;
	}
	if (vm.count("hidden"))
	{
		storageObj.hiddenSim = true;

	}
	//TODO -- ladowanie pliku xml property tree
	

	return 0;

}


// This function is called each frame
int mainloop()
{
	// Update engine-internal components
	// If input events are available, they are passed to Stage::instance.handleEvent
	// If the function returns true, it means that the user requested the application to terminate
	bool done = core::update();

	// It gets passed to our example game implementation
	example_update();

	// Update our stage
	// Update all actors. Actor::update will also be called for all its children
	getStage()->update();

	if (core::beginRendering())
	{
		Color clearColor(32, 32, 32, 255);
		Rect viewport(Point(0, 0), core::getDisplaySize());
		// Render all actors inside the stage. Actor::render will also be called for all its children
		storageObj.threadSyncro.rendering_mtx.lock();
		getStage()->render(clearColor, viewport);
		storageObj.threadSyncro.rendering_mtx.unlock();

		core::swapDisplayBuffers();
	}

	return done ? 1 : 0;
}

// Application entry point
void run()
{
	ObjectBase::__startTracingLeaks();

	// Initialize Oxygine's internal stuff
	core::init_desc desc;
	desc.title = "Korki";

	// The initial window size can be set up here on SDL builds, ignored on Mobile devices
	desc.w = 1200;
	desc.h = 720;


	example_preinit();
	core::init(&desc);


	// Create the stage. Stage is a root node for all updateable and drawable objects
	Stage::instance = new Stage();
	Point size = core::getDisplaySize();
	getStage()->setSize(size);
	/*getStage()->setAnchor(0.5, 0.5);
	getStage()->setPosition(size / 2);
	getStage()->setScale(0.8);
	*/

	// DebugActor is a helper actor node. It shows FPS, memory usage and other useful stuff
	//DebugActor::show();

	// Initializes our example game. See example.cpp
	example_init();




	// This is the main game loop.
	while (1)
	{
		int done = mainloop();
		if (done)
			break;
	}
	/*
	If we get here, the user has requested the Application to terminate.
	We dump and log all our created objects that have not been freed yet
	*/
	ObjectBase::dumpCreatedObjects();

	/*
	Let's clean up everything right now and call ObjectBase::dumpObjects() again.
	We need to free all allocated resources and delete all created actors.
	All actors/sprites are smart-pointer objects and don't need to be removed by hand.
	But now we want to delete it by hand.
	*/

	// See example.cpp for the shutdown function implementation
	example_destroy();


	//renderer.cleanup();

	// Releases all internal components and the stage
	core::release();

	// The dump list should be empty by now,
	// we want to make sure that there aren't any memory leaks, so we call it again.
	ObjectBase::dumpCreatedObjects();

	ObjectBase::__stopTracingLeaks();
	//end
}



#include "SDL_main.h"
#include "SDL.h"


extern "C"
{
	void one(void* param) { mainloop(); }
	void oneEmsc() { mainloop(); }

	int main(int argc, char* argv[])
	{
		if(checkArgs(argc, argv))
			return 0;


		run();
		return 0;
	}
};
