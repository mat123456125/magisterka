#include "oxygine-framework.h"
#include <functional>
#include "model\simulation.h"
#include "camera.h"
#include "render\renderer.h"
#include "analysis.h"
#include <thread>


using namespace oxygine;

//it is our resources
//in real project you would have more than one Resources declarations.
//It is important on mobile devices with limited memory and you would load/unload them
Resources gameResources;
simulation* ptr_simulation;
renderer modelRenderer;
analysis* ptr_analysis;



class MainActor : public Actor
{
public:
	spTextField _text;
	spSprite    _button;
	spCamera	_camera;


	MainActor()
	{
		spColorRectSprite background = new ColorRectSprite();
		background->setColor(Color::Green);
		background->setSize(getStage()->getSize());
		
		addChild(background);

		_camera = new camera();
		//_camera->setAnchor(0.5, 0.5);
		//_camera->setPosition(getStage()->getSize() / 2);
		

		addChild(_camera);
		modelRenderer.init(_camera, gameResources);
		
		
		//create button Sprite
		spSprite button = new Sprite();
		
		


		//setup it:
		//set button.png image. Resource 'button' defined in 'res.xml'
		button->setResAnim(gameResources.getResAnim("button"));

		//centered button at screen
		Vector2 pos = /*getStage()->getSize() / 2*/ - button->getSize() / 2;
		button->setPosition(pos);

		//register  click handler to button
		EventCallback cb = CLOSURE(this, &MainActor::buttonClicked);
		button->addEventListener(TouchEvent::CLICK, cb);

		//animate mouse over and mouse out events
		cb = CLOSURE(this, &MainActor::buttonOverOut);
		button->addEventListener(TouchEvent::OVER, cb);
		button->addEventListener(TouchEvent::OUT, cb);


		button->addEventListener(TouchEvent::CLICK, [](Event * e)->void
		{
			logs::messageln("button clicked");
			e->stopPropagation();
		});



		//attach button as child to current actor
		//_camera->addChildToCamera(button);
		_button = button;

	
		

		//create TextField Actor
		spTextField text = new TextField();
		
		if (ptr_simulation->hidden == true)
		{
			addChild(text);
			//centered in button
			
			text->setPosition(10,10);
			text->setSize(500, 200);
			text->setAlign(ox::TextStyle::VerticalAlign::VALIGN_TOP, ox::TextStyle::HorizontalAlign::HALIGN_LEFT);
		}

		//initialize text style
		
		TextStyle style = TextStyle(gameResources.getResFont("arial")).withColor(Color::White);
		text->setStyle(style);
		text->setText(" ");

		_text = text;
	}

	void buttonClicked(Event* event)
	{
		//user clicked to button

		//animate button by chaning color
		_button->setColor(Color::White);
		_button->addTween(Sprite::TweenColor(Color::Green), 500, 1, true);

		//animate text by scaling
		_text->setScale(1.0f);
		_text->addTween(Actor::TweenScale(1.1f), 500, 1, true);

		//and change text
		_text->setText("Clicked!");

		//lets create and run sprite with simple animation
		runSprite();
	}

	void buttonOverOut(Event* e)
	{
		if (e->type == TouchEvent::OVER)
		{
			_button->addTween(Sprite::TweenAddColor(Color(64, 64, 64, 0)), 300);
		}

		if (e->type == TouchEvent::OUT)
		{
			_button->addTween(Sprite::TweenAddColor(Color(0, 0, 0, 0)), 300);
		}
	}

	void runSprite()
	{
		spSprite sprite = new Sprite();
		_camera->addChildToCamera(sprite);

		int duration = 600;//ms
		int loops = -1;//infinity loops

					   //animation has 8 columns - frames, check 'res.xml'
		ResAnim* animation = gameResources.getResAnim("anim");

		//add animation tween to sprite
		//TweenAnim would change animation frames
		sprite->addTween(Sprite::TweenAnim(animation), duration, loops);

		Vector2 destPos = getStage()->getSize()/2 - sprite->getSize();
		Vector2 srcPos = Vector2(0, destPos.y);
		//set sprite initial position
		sprite->setPosition(srcPos);

		//add another tween: TweenQueue
		//TweenQueue is a collection of tweens
		spTweenQueue tweenQueue = new TweenQueue();
		tweenQueue->setDelay(1500);
		//first, move sprite to dest position
		tweenQueue->add(Sprite::TweenPosition(destPos), 2500, 1);
		//then fade it out smoothly
		tweenQueue->add(Sprite::TweenAlpha(0), 500, 1);

		

		sprite->addTween(tweenQueue);

		//and remove sprite from tree when tweenQueue is empty
		//if you don't hold any references to sprite it would be deleted automatically
		tweenQueue->detachWhenDone();
	}

	void update(const ox::UpdateState& parentUS)
	{
		if (ptr_simulation->hidden)
		{
			_text->setText(std::to_string(storageObj.aResults.testNumber) + "/" + std::to_string(storageObj.aResults.inputs.size()) + " - "
				+ std::to_string((int)storageObj.aResults.currentTime) + "/" + std::to_string((int)storageObj.aResults.testTime * 60));
		}
		

		Actor::update(parentUS);
	}
};
//declare spMainActor as intrusive_ptr holder of MainActor
typedef oxygine::intrusive_ptr<MainActor> spMainActor;
//you could use DECLARE_SMART preprocessor definition it does the same:
//DECLARE_SMART(MainActor, spMainActor)


void onEvent(Event* ev)
{
	SDL_Event *event = (SDL_Event*)ev->userData;

	if (event->type != SDL_KEYDOWN)
		return;

	//all key codes could be found in "SDL_keyboard.h" from SDL
	switch (event->key.keysym.sym)
	{
	case SDLK_EQUALS:
		for (border* b : storageObj.borders)
		{
			
			b->secPerCar -= 0.2;
			if (b->secPerCar < 0.1) b->secPerCar = 0.1;
		}

		break;
	case SDLK_MINUS:
		for (border* b : storageObj.borders)
		{
			b->secPerCar += 0.2;
		}
		
		break;
	}
}


void example_preinit() {}


//called from main.cpp
void example_init()
{
	ptr_simulation = new simulation(&modelRenderer);
	//ptr_simulation->setHiddenSim();

	//load xml file with resources definition
	gameResources.loadXML("res.xml");
	
	ox::core::getDispatcher()->addEventListener(ox::core::EVENT_SYSTEM, onEvent);

	//lets create our client code simple actor
	//spMainActor was defined above as smart intrusive pointer (read more: http://www.boost.org/doc/libs/1_60_0/libs/smart_ptr/intrusive_ptr.html)
	spMainActor actor = new MainActor;

	//and add it to Stage as child
	getStage()->addChild(actor);
	
	if (ptr_simulation->hidden == true)
	{
		ptr_analysis = new analysis(ptr_simulation); 
		ptr_analysis->loadSettings();			//ladowanie ustawien testow

		std::thread sim_thread = std::thread((*ptr_analysis)); //oddzielny watek symulacji

		sim_thread.detach();
	}
	
}


//called each frame from main.cpp
void example_update()
{
	if (ptr_simulation->hidden != true)
	{
		ptr_simulation->run();
	}
}



//called each frame from main.cpp
void example_destroy()
{

	if (ptr_simulation->hidden == true)
	{
		ptr_analysis->stopAnalisis();
	}
	
	//free previously loaded resources
	gameResources.free();
	
	
	delete ptr_analysis;
	delete ptr_simulation;
}
