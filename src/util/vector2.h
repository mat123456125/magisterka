#pragma once
#include <math.h>

class vector2
{
public:
	vector2();
	vector2(float nx, float ny);
	~vector2();

	float x;
	float y;

	void set(float nx, float ny);

	vector2 operator+(const vector2& a);
	vector2 operator-(const vector2& a);
	vector2 operator-();
	vector2 operator/(const float& a);
	vector2 operator*(const float& a);
	float dot(const vector2& a);
	float length();
	vector2 normalize();
	vector2 perpendicular();
	vector2 perpendicular2();
	
};

vector2 getIntersectionPoint(vector2 p1, vector2 p2, vector2 p3, vector2 p4);
vector2 getClosestPointOnLine(vector2 l1, vector2 l2, vector2 p1);

