#pragma once

#include <mutex>
#include <condition_variable>

struct ThreadSyncro
{
	std::mutex threadStoping_mtx;
	std::mutex rendering_mtx;
	std::condition_variable threadStoping;
	bool stopThread;
	bool threadRunning;
};
