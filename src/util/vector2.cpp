#include "vector2.h"



vector2::vector2()
{
	x = 0;
	y = 0;
}

vector2::vector2(float nx, float ny)
{
	x = nx;
	y = ny;
}


vector2::~vector2()
{
	set(0, 0);
}

void vector2::set(float nx, float ny)
{
	x = nx;
	y = ny;
}

vector2 vector2::operator+(const vector2 & a)
{
	vector2 res;
	res.x = x + a.x;
	res.y = y + a.y;
	return res;
}

vector2 vector2::operator-(const vector2 & a)
{
	vector2 res;
	res.x = x - a.x;
	res.y = y - a.y;
	return res;
}

vector2 vector2::operator-()
{
	return vector2(-x,-y);
}

vector2 vector2::operator/(const float & a)
{
	if (a == 0) return vector2();
	return vector2(x/a,y/a);
}

vector2 vector2::operator*(const float & a)
{
	return vector2(x*a,y*a);
}

float vector2::dot(const vector2 & a)
{
	return x*a.x + y*a.y;
}

float vector2::length()
{
	return sqrtf(x * x + y * y);
}

vector2 vector2::normalize()
{
	vector2 res;
	float l = length();
	if (l != 0)
	{
		res.x = x / l;
		res.y = y / l;
	}
	return res;
}

vector2 vector2::perpendicular()
{	
	return vector2(-y,x);
}

vector2 vector2::perpendicular2()
{
	return vector2(y, -x);
}

vector2 getIntersectionPoint(vector2 p1, vector2 p2, vector2 p3, vector2 p4)  //p1,p2 prosta 1  p3,p4 prosta 2
{
	float numerator1 = ((p1.x * p2.y - p1.y * p2.x) * (p3.x - p4.x)) - ((p1.x - p2.x)*(p3.x * p4.y - p3.y * p4.x));	
	float numerator2 = ((p1.x * p2.y - p1.y * p2.x) * (p3.y - p4.y)) - ((p1.y - p2.y)*(p3.x * p4.y - p3.y * p4.x));
	float denominator = ((p1.x - p2.x)* (p3.y - p4.y)) - ((p1.y - p2.y)*(p3.x - p4.x));

	vector2 result;
	result.x = numerator1 / denominator;
	result.y = numerator2 / denominator;

	return result;
}

vector2 getClosestPointOnLine(vector2 l1, vector2 l2, vector2 p1)
{
	vector2 line = (l2 - l1).normalize();
	return l1 + (line * (p1 - l1).dot(line));
	
}
