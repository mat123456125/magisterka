#pragma once

#include <random>
#include <math.h>
//#include <SimpleMath.h>

class randomHelper
{
public:
	randomHelper();
	~randomHelper();
	float randf(); //losowy float  od 0 do 1
	float randf(float a, float b); //losowy float z przedzialu
	float randNormal(float mi, float sigma);
	int randi(int a, int b); //losowy float z przedzialu
	//DirectX::SimpleMath::Vector3 randUnitVector3(); //losowy wektor dl 1
private:
	std::mt19937 generator;
};

